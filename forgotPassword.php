<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/forgotPassword.php" />
<meta property="og:title" content="Forgot Password | Pingola" />
<title>Forgot Password | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/forgotPassword.php" />
<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    

    <h1 class="line-header margin-bottom50">Forgot Password</h1>

        <form method="POST" action="utilities/forgotPasswordFunction.php">

            <div class="width100">
                <p class="input-top-p">Email</p>
                <input class="input-name clean" type="email" placeholder="Type Your Email" id="forgot_password" name="forgot_password" required>	
			</div>

            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

        </form>

    </div> 

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update refer's credit !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            // $messageType = "Input data for register already taken by other user !! <br> Please register again";
            $messageType = "Fail get submit data on referral history !!";
        }
        else if($_GET['type'] == 5)
        {
            // $messageType = "password must be more than 5 value !!";
            $messageType = "fail to register !!";
        }
        else if($_GET['type'] == 6) 
        {
            // $messageType = "Password is not match with retype password !!";
            $messageType = "Input data for register already taken by other user !! <br> Please register again";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "password must be more than 5 value !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "password and retype password not the same !!";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "unable to find related data sponsor ID in Referral table !!";
        }

        else if($_GET['type'] == 10)
        {
            $messageType = "unable to find related data sponsor ID !!";
        }
        else if($_GET['type'] == 11)
        {
            $messageType = "invalid sponsor ID !!";
        }


        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>