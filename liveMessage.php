<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $messageDetails = getMessage($conn," ORDER BY date_created ASC LIMIT 100");
$messageDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC LIMIT 50 ",array("uid"),array($uid),"s");

// $messageDetails = getMessage($conn," WHERE id DESC LIMIT 50 ");
if($messageDetails)
{   
    for($cnt = 0;$cnt < count($messageDetails) ;$cnt++)
    {
    ?>
        <div class="chat-bubble-div">
            <?php 
                $productQnA = $messageDetails[$cnt]->getReplyThree();
                if($productQnA != '')
                {
                ?>
                    <p class="username-date text-center"><b>About : <?php echo $messageDetails[$cnt]->getReplyThree();?></b></p>
                <?php
                }
            ?>
			<p class="chat-date"><?php echo $messageDetails[$cnt]->getDateCreated();?></p>
            <p class="chat-bubble user-chat">
                <?php echo $messageDetails[$cnt]->getReceiveSMS();?>
            </p>
			
            <p class="chat-bubble reply-message">
                <?php echo $messageDetails[$cnt]->getReplySMS();?>
            </p>
            
			<!-- <p class="username-date"><b>Username Here</b> 22/08/2020 10:00 AM</p> -->
            <!-- <p class="username-date"><b><?php echo $messageDetails[$cnt]->getUsername();?></b> <?php echo $messageDetails[$cnt]->getDateCreated();?></p> -->
        </div>
    <?php
    }
    ?>
<?php
}
$conn->close();
?>

<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script>
$('.chat-section').scrollTop($('.chat-section')[0].scrollHeight);	
</script>	