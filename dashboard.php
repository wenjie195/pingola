<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/dashboard.php" />
<meta property="og:title" content="Dashboard | Pingola" />
<title>Dashboard | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/dashboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

  
     <div class="width100 overflow">
    	<h1 class="line-header">Dashboard</h1>
        <div class="clear"></div>   
 		<div class="width100 overflow">
        	<a href="topUpRequest.php" class="opacity-hover">
                <div class="three-div-width1 box-css">
                    <img src="img/pingcash.png" alt="Top Up Request" title="Top Up Request" class="box-img">
                    <p class="box-p">Top Up Request</p>
                    <p class="box-value">300</p>
                </div>
            </a>
            <a href="withdrawRequest.php" class="opacity-hover">
                <div class="three-div-width1 box-css mid-three-width1">
                    <img src="img/withdraw.png" alt="Withdrawal Request" title="Withdrawal Request" class="box-img">
                    <p class="box-p">Withdrawal Request</p>
                    <p class="box-value">300</p>
                </div>   
            </a> 
            <a href="gamesOnGoing.php" class="opacity-hover">     
                <div class="three-div-width1 box-css">
                    <img src="img/dota2.png" alt="Games Ongoing" title="Games Ongoing" class="box-img">
                    <p class="box-p">Games Ongoing</p>
                    <p class="box-value">300</p>
                </div>  
           </a>               
        </div>
     </div>
</div>


<?php include 'js.php'; ?>


</body>
</html>