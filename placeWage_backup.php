<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$selectTeam = rewrite($_POST['team']);
$title = rewrite($_POST['title']);
$gameId = rewrite($_POST['game_id']);
$matches = rewrite($_POST['matches']);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/placeWage.php" />
<meta property="og:title" content="Place Wage | Pingola" />
<title>Place Wage | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/placeWage.php" />
<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <?php
    // if(isset($_POST['games_uid']))
    // {
    // $conn = connDB();
    // $gamesRows = getGames($conn,"WHERE uid = ? ", array("uid") ,array($_POST['games_uid']),"s");
    // $gamesDetails = $gamesRows[0];
    ?>

    <div class="modal-content-css register-modal-content">
        <div class="clear"></div>
        <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Place Your Wage</h2>
        <!-- <p class="black-text match-title"><?php //echo $gamesDetails->getTitle();?></p> -->
        <p class="black-text match-title"><?php echo $title;?></p>

        <!-- <form > -->
        <form method="POST" action="utilities/placeWageFunction.php" enctype="multipart/form-data">

          <input type="hidden" name="game_id" value="<?php echo $gameId ?>">
          <input type="hidden" name="matches" value="<?php echo $matches ?>">

            <div class="dual-input">
                <p class="input-top-p big-top-p"><?php //echo $gamesDetails->getTeamOne();?>  <?php //echo $gamesDetails->getValueOne();?></p>
                <!-- <input class="input-name clean" type="number" placeholder="0 PingCash" id="" name="" required> -->
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p big-top-p"><?php //echo $gamesDetails->getTeamTwo();?>  <?php //echo $gamesDetails->getValueTwo();?></p>
                <!-- <input class="input-name clean" type="number" placeholder="0 PingCash" id="" name="" required>	 -->
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p  big-top-p">Team Select</p>
                <select class="clean input-name admin-input" name="team_selected" id="team_selected" required>
                    <!-- <option value="" name=" ">Select a Team</option> -->
                    <option value="<?php echo $selectTeam ?>"><?php echo $selectTeam ?></option>
                    <!-- <option value="<?php //echo $gamesDetails->getTeamOne();?>" name="<?php //echo $gamesDetails->getTeamOne();?>"><?php //echo $gamesDetails->getTeamOne();?></option> -->
                    <!-- <option value="<?php //echo $gamesDetails->getTeamTwo();?>" name="<?php //echo $gamesDetails->getTeamTwo();?>"><?php //echo $gamesDetails->getTeamTwo();?></option> -->
                </select>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p big-top-p">Place Wage</p>
                <input class="input-name clean" type="number" placeholder="0 PingCash" name="wage_value" id="wage_value" required>
            </div>

            <input class="input-name clean" type="hidden" value="<?php //echo $gamesDetails->getValueOne();?>" name="value_one" id="value_one" readonly>
            <input class="input-name clean" type="hidden" value="<?php //echo $gamesDetails->getValueTwo();?>" name="value_two" id="value_two" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $title?>" name="game_title" id="game_title" readonly>
            <input class="input-name clean" type="hidden" value="<?php //echo $gamesDetails->getStatus();?>" name="game_status" id="game_status" readonly>
            <input class="input-name clean" type="hidden" value="<?php //echo $userDetails->getCredit();?>" name="current_cash" id="current_cash" readonly>
            <input class="input-name clean" type="hidden" value="<?php //echo $userDetails->getUsername();?>" name="username" id="username" readonly>

            <button class="blue-button white-text width100 clean register-button"  name="submit" >Confirm</button>

            <!-- <div class="width100 text-center margin-top10 overflow">
            	<img src="img/timer-black.png" class="timer-png"><p class="timer-p"  id="timerP"></p>
            </div> -->

            <?php

                $timestamp;
                // echo "<br>";
                $date2 = strtotime($gamesDetails->getMatchday());
                $hourDiff = $date2 - $timestamp;
                $date = date("h:i:s",strtotime($hourDiff));
                $days_remaining = floor($hourDiff / 86400);
                $hours_remaining = floor(($hourDiff % 86400) / 3600);
                $mins_remaining = floor(($hourDiff % 3600) / 60 );
                $sec_remaining = floor(($hourDiff % 60) );

                // if($sec_remaining  != 0)
                {
                ?>
                    <div class="width100 text-center margin-top10 overflow">
                        <!-- <img src="img/timer-white.png" class="timer-png"> -->
                        <img src="img/timer-black.png" class="timer-png">
                        <p class="timer-p"><?php echo " $days_remaining d and $hours_remaining h $mins_remaining m " ;?></p>
                    </div>
                <?php
                }

            ?>
        </form>
    </div>

    <?php
    // }
    ?>

</div>

<?php include 'js.php'; ?>

<!-- <script type="text/javascript">
$(document).ready(function()
{
    $("#divCounter").load("countdownWage.php");
setInterval(function()
{
    $("#divCounter").load("countdownWage.php");
}, 10000);
});
</script> -->

</body>
</html>
