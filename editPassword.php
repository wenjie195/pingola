<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/editPassword.php" />
<meta property="og:title" content="Edit Password | Pingola" />
<title>Edit Password | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/editPassword.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <h1 class="line-header margin-bottom50">Edit Password</h1>

        <form method="POST" action="utilities/editPasswordFunction.php">

            <div class="dual-input">
                <p class="input-top-p">Current Password</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="Current Password" id="register_password" name="register_password" required>        
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>
            </div>
            
            <div class="clear"></div>

            <div class="dual-input">
            <!-- <div class="dual-input second-dual-input"> -->
            <p class="input-top-p">New Password</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="New Password" id="register_retype_password" name="register_retype_password" required> 
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">           
                </div>
            </div>
            
            <!-- <div class="clear"></div> -->
            
            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Retype New Password</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="Retype Password" id="password" name="password" required>        
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
                </div>
			</div>            
                        
            <div class="clear"></div>

            <button class="blue-button white-text width100 clean register-button"  name="">Submit</button>

            <div class="clear"></div>

        </form>

    </div> 

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "password must be same with re-type password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more than 5 !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "current password is wrong !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>