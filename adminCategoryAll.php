<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$categoryDetails = getCategory($conn," WHERE status = ? ",array("status"),array("Available"),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminCategoryAll.php" />
<meta property="og:title" content="Category | Pingola" />
<title>Category | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminCategoryAll.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height" id="myTable">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">Category</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        <form>
                <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn  mid-search-btn clean">
                    <img src="img/search.png" class="visible-img mid-search-img opacity-hover" alt="Search" title="Search">
                   
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="adminCategoryAdd.php"><div class="green-button white-text puppy-button">Add Category</div></a>
        </div>      
    </div>


    <div class="clear"></div>
	<div class="overflow-scroll-div margin-top30 same-padding-tdh">
    	<table class="table-css">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Category</th>
                    <th>Added On</th>
                    <!--<th>Status</th>-->
                    <th>Details</th>
                    <th>Delete</th>                    
                </tr>
            </thead>
            <tbody>
            <?php
            if($categoryDetails)
                {
                    
                    for($cnt = 0;$cnt < count($categoryDetails) ;$cnt++)
                    {?>
                        
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $categoryDetails[$cnt]->getName();?></td>

                            <td>
                                <?php echo $date = date("d-m-Y",strtotime($categoryDetails[$cnt]->getDateCreated()));?>
                            </td>
                            <!--<td><?php echo $categoryDetails[$cnt]->getStatus();?></td>-->

                            <!-- <td>
                                <a href="editSeller.php" class="hover1">
                                    <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                    <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                </a>                    
                            </td> -->

                            <!-- <td>
                                <a class="hover1 open-confirm pointer">
                                    <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                    <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                </a>                    
                            </td> -->

                            <td>
                                <!-- <form action="editCategory.php" method="POST" class="hover1"> -->
                                <form action="adminCategoryEdit.php" method="POST" class="hover1">
                                    <button class="clean transparent-button pointer" type="submit" name="category_id" value="<?php echo $categoryDetails[$cnt]->getId();?>">
                                        <img src="img/edit.png" class="edit-icon1" alt="Edit" title="Edit">
                                        
                                    </button>
                                </form> 
                            </td>

                            <td>
                                <!-- <form method="POST" action="utilities/deleteCategoryFunction.php" class="hover1"> -->
                                <form method="POST" action="utilities/adminCategoryDeleteFunction.php" class="hover1">
                                    <input type="hidden" value="<?php echo $categoryDetails[$cnt]->getName();?>" name="category_name" id="category_name">
                                    <button class="clean transparent-button pointer" type="submit" name="category_id" value="<?php echo $categoryDetails[$cnt]->getId();?>">
                                        <img src="img/delete1a.png" class="edit-icon1" alt="Delete" title="Delete">
                                        
                                    </button>
                                </form> 
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>                                   
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Category Added!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "ERROR!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Category Deleted !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete category !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Category Edited!!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to edit category!!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "This category already existed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>