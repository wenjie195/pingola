<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$uid = $_SESSION['uid'];

$conn = connDB();

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
// curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/upcoming?token=F7U9j7gsyiDz8BzpTePBShxkAJ2hGrEeZVrP24Dk0OwZlq7s40U");
// curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/upcoming?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ");


// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);

// $gamesDetails = getGames($conn);
// $gamesDetails = getGames($conn," Order By date_created ASC ");
// $gamesDetails = getGames($conn," WHERE matchday < '".$longtime."' Order By date_created ASC ");
$gamesDetails = getGames($conn," WHERE matchday > '".$longtime."' Order By date_created ASC ");

$conn->close();

?>

        <table id="fix" class="table-css">
            <thead>
                <tr>
                    <th class="th" style="text-align: center;">NO.</th>
                    <!-- <th class="th" style="text-align: center;">MATCH TYPE</th> -->
                    <th class="th" style="text-align: center;">TOURNAMENT LOGO</th>
                    <th class="th" style="text-align: center;">TOURNAMENT NAME</th>
                    <!-- <th class="th" style="text-align: center;">STATUS</th> -->
                    <th class="th" style="text-align: center;">MATCH</th>
                    <th class="th" style="text-align: center;">OPPONENT</th>
                    <th class="th" style="text-align: center;">SCHEDULED DATE</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if ($exchangeData)
            {
                for ($i=0; $i <count($exchangeData) ; $i++)
                {
                    if (strpos($exchangeData[$i]['name'],"TBD") == false)
                    {
                    $No++;
                    ?>
                        <tr>
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $No ?></td>
                            <!-- <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                                <?php if ($exchangeData[$i]['match_type'] == 'best_of')
                                {
                                    echo "Best of ".$exchangeData[$i]['number_of_games'];
                                }
                                ?>
                            </td> -->
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                                <?php if ($exchangeData[$i]['league']['image_url'])
                                {
                                ?>
                                    <img style="width: 120px;height: 100px" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt="">
                                <?php
                                }  
                                else
                                {
                                ?>
                                    <img style="width: 120px;height: 100px" src="img/dota_logo.png" alt=""><?php
                                }
                                ?>
                            </td>
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $exchangeData[$i]['league']['name'] ?></td>
                            <!-- <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                            <?php if ($exchangeData[$i]['status'] == 'not_started')
                            {
                                echo "Not Started Yet";
                            }
                            elseif ($exchangeData[$i]['status'] == 'running')
                            {
                                echo "Running";
                            } ?>
                            </td> -->
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo str_replace(":",":<br>",$exchangeData[$i]['name']) ?></td>
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                                <button class="clean transparent-button" type="submit" name="button">
                                <?php if ($exchangeData[$i]['opponents'][0]['opponent']['image_url'])
                                {
                                ?>
                                <img style="width: 30px;height: 30px;" src="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['image_url'] ?>" alt="">
                                <?php
                                }
                                else
                                {
                                    echo $exchangeData[$i]['opponents'][0]['opponent']['name'];
                                }
                                ?>
                                </button>
                                <div style="padding: 5px"></div>
                                <button class="clean transparent-button" type="submit" name="button">
                                <?php if ($exchangeData[$i]['opponents'][1]['opponent']['image_url'])
                                {
                                ?>
                                    <img style="width: 30px;height: 30px;" src="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['image_url'] ?>" alt=""><?php
                                }
                                else
                                {
                                    echo $exchangeData[$i]['opponents'][1]['opponent']['name'];
                                }
                                ?>
                                </button>
                            </td>
                            <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                                <?php 
                                    $officaltime = date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at']));

                                    $timestamp;
                                    // echo "<br>";
                                    $date2 = strtotime($exchangeData[$i]['scheduled_at']);
                                    $hourDiff = $date2 - $timestamp;
                                    $date = date("h:i:s",strtotime($hourDiff));
                                    $days_remaining = floor($hourDiff / 86400);
                                    $hours_remaining = floor(($hourDiff % 86400) / 3600);
                                    $mins_remaining = floor(($hourDiff % 3600) / 60 );
                                    $sec_remaining = floor(($hourDiff % 60) );

                                    echo " $days_remaining d and $hours_remaining h $mins_remaining m $sec_remaining s " ;

                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                }
            }
            ?>
            </tbody>
        </table>