<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/editProfile.php" />
<meta property="og:title" content="Edit Profile | Pingola" />
<title>Edit Profile | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/editProfile.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <h1 class="line-header margin-bottom50">Edit Profile</h1>

        <form method="POST" action="utilities/editProfileFunction.php">

			<div class="dual-input">
                <p class="input-top-p">Username</p>
                <input class="input-name clean" type="text" placeholder="Username" value="<?php echo $userDetails->getUsername();?>" name="edit_username"  id="edit_username" required>
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Fullname</p>
                <input class="input-name clean" type="text" placeholder="Fullname" value="<?php echo $userDetails->getFullname();?>" name="fullname" id="fullname">	
            </div>

            <!-- <div class="dual-input second-dual-input">
                <p class="input-top-p">Nickname</p>
                <input class="input-name clean" type="text" placeholder="Nickname" value="" name="" id="" required>	
            </div> -->
            
            <div class="clear"></div>

			<div class="dual-input">
                <p class="input-top-p">Email</p>
                <input class="input-name clean" type="email" placeholder="Type Your Email" value="<?php echo $userDetails->getEmail();?>" name="edit_email" id="edit_email" required>	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Phone</p>
                <input class="input-name clean" type="text" placeholder="Phone Number" value="<?php echo $userDetails->getPhoneNo();?>" name="edit_phone" id="edit_phone" required>	
            </div>            
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Address</p>
                <input class="input-name clean" type="text" placeholder="Address" value="<?php echo $userDetails->getAddress();?>" name="address" id="address">	
            </div>      

            <div class="dual-input second-dual-input">
                <p class="input-top-p">Area</p>
                <input class="input-name clean" type="text" placeholder="Area" value="<?php echo $userDetails->getArea();?>" name="area" id="area">	
			</div>

			<div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Postcode</p>
                <input class="input-name clean" type="text" placeholder="Postcode" value="<?php echo $userDetails->getPostcode();?>" name="postcode" id="postcode">	
            </div>      

            <div class="dual-input second-dual-input">
                <p class="input-top-p">State</p>
                <input class="input-name clean" type="text" placeholder="State" value="<?php echo $userDetails->getState();?>" name="state" id="state">	
			</div>

            <!-- <div class="dual-input second-dual-input">
                <p class="input-top-p">Country</p>
                <input class="input-name clean" type="text" placeholder="Country" value="" name="country" id="country">	
			</div> -->

			<div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Bank</p>
                <input class="input-name clean" type="text" placeholder="Bank" value="<?php echo $userDetails->getBankName();?>" name="bank_name" id="bank_name">	
            </div>      

            <div class="dual-input second-dual-input">
                <p class="input-top-p">Bank Account No.</p>
                <input class="input-name clean" type="text" placeholder="Bank Account No." value="<?php echo $userDetails->getBankAccountNo();?>" name="bank_account_number" id="bank_account_number">	
			</div>

			<div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Bank Account Holder Name</p>
                <input class="input-name clean" type="text" placeholder="Bank Account Name" value="<?php echo $userDetails->getBankAccountName();?>" name="bank_account_holder" id="bank_account_holder">	
            </div>     
             
            <div class="clear"></div>
            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

            <div class="clear"></div>

        </form>

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>