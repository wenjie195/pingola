<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminAddLiveVideo.php" />
<meta property="og:title" content="Admin Dashboard | Pingola" />
<title>Admin Live Video | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminAddLiveVideo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <h1 class="line-header margin-bottom50">Edit Profile</h1>

        <form method="POST" action="utilities/registerNewSharingFunction.php">

			<div class="dual-input">
                <p class="input-top-p">Embed Link</p>
                <input class="input-name clean" type="text" placeholder="Embed Link" name="register_link"  id="register_link" required>
            </div>
            
            <div class="clear"></div>

            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

            <div class="clear"></div>

        </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>