<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/gameResult.php" />
<meta property="og:title" content="Game Result | Pingola" />
<title>Game Result | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/gameResult.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
	<div class="width100 overflow text-center">

        <?php 
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <div class="left-profile-div margin-auto vip">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
            else
            {
            ?>
                <div class="left-profile-div margin-auto">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php 
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>
		<p class="username-p">My PingCash: <?php echo $userDetails->getCredit();?></p>
        
    </div>
	<div class="clear"></div>
 

     <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">My Game Result</h1>
    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	<!-- StarLadder ImbaTV Dota 2 Minor Season 3 ( 5 July 2020 10:00 pm) -->
            StarLadder ImbaTV Dota 2 Minor Season 3
            (<?php echo $time ;?>)
        </p>
    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
            <p class="bet-team-name">Dota 2</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
        
                     
    </div>
    <!-- Lose and Win got different color and class -->
	<p class="result-p win-color">You Win (PingCash +20).</p>
    <div class="bluered-gradient-bg result-color-div width100 margin-top50">
    	<p class="tournament-p">
        	<!-- UKLC 2020 Summer ( 5 July 2020 10:00 pm) -->
            UKLC 2020 Summer
            (<?php echo $time ;?>)
        </p>

    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/league-of-legends.png" alt="League of Legends" title="League of Legends" class="bet-game-logo team-logo">
            <p class="bet-team-name">League of Legends</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
        
                     
    </div>
	<p class="result-p lose-color">You Lose (PingCash -40).</p>
    <div class="bluered-gradient-bg result-color-div width100 margin-top50">
    	<p class="tournament-p">
        	<!-- Overwatch League ( 5 July 2020 10:00 pm) -->
            Overwatch League
            (<?php echo $time ;?>)
        </p>    
            <div class="div1a">
                <img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
                <p class="bet-team-name">SCABBARD</p>
            </div>
            <div class="div2a">
                <div class="emerald-bg rate-div rate-div1">1.890</div>
            </div>
    
            <div class="div4a">
                <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
            </div>
            <div class="div5a">
                <img src="img/overwatch.png" alt="Overwatch" title="Overwatch" class="bet-game-logo team-logo">
                <p class="bet-team-name">Overwatch</p>
            </div>  
            <div class="div6a">
                <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
            </div>   
               
            <div class="div8a">
                <div class="red-bg rate-div rate-div2">1.920</div>
            </div> 
            <div class="div9a">
                <img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
                <p class="bet-team-name">SILENT NIGHT</p>
            </div>        
            
                         
        </div> 
        <p class="result-p win-color">You Win (PingCash +20).</p>       
     </div>   
     <div class="clear"></div>
     <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">All My Gaming History</h1>
        <div class="overflow-scroll-div">
        <table class="table-css">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Date</th>
                    <th>Match</th>
                    <th>Result</th>
                    <th>My PingCash</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td>1.</td>
                    <td>5/5/2020</td>
                    <td>StarLadder ImbaTV Dota 2 Minor Season 3</td>
                    <td class="win-color">Win</th>
                    <td class="win-color">+ 20</td> 
                </tr>
            	<tr>
                	<td>2.</td>
                    <td>5/5/2020</td>
                    <td>UKLC 2020 Summer</td>
                    <td class="lose-color">Lose</th>
                    <td  class="lose-color">- 40</td> 
                </tr>                
            	<tr>
                	<td>3.</td>
                    <td>5/5/2020</td>
                    <td>Overwatch League</td>
                    <td class="win-color">Win</th>
                    <td class="win-color">+ 20</td> 
                </tr>                 
            </tbody>
        </table>
        </div>
     </div>
</div>


<?php include 'js.php'; ?>


</body>
</html>