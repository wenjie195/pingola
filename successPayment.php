<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
// require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allFavorite = getFavorite($conn, "WHERE uid =? AND status = 'Yes' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/successPayment.php" />
<meta property="og:title" content="Success Payment | Pingola" />
<title>Success Payment | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/successPayment.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
    
    <div class="width100 overflow text-center">
        <?php
            $conn = connDB();
            if(isset($_SESSION['order_uid']) && $_SESSION['order_uid'])
            {
                unset($_SESSION['order_uid']);
            ?>
            	<img src="img/check.png" class="tick-css" alt="Success" title="Success">
                <h1 class="text-center success-text white-text2">The payment is successfully make. Thank you! We will ship out the items to you in a few business days. Kindly check order history for the status.</h1>
                <div class="clear"></div>
                <a href="orderHistory.php"><div class="green-button checkout-btn clean view-order-btn">View Order History</div></a>
            <?php
            }
            else
            {   }
            $conn->close();
        ?>
    </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>