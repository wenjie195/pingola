<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/thankYou.php" />
<meta property="og:title" content="Thank You | Pingola" />
<title>Thank You | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/thankYou.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
	<h1 class="text-center margin-bottom50">Thank You!</h1>
    <div class="clear"></div>
    <div class="width100 overflow text-center">
    	<img src="img/ping2.png" class="ping" alt="Thank You" title="Thank You">
    </div>
    <p class="explain-p">You are currently rewarded with <b>$5 Ping Cash</b>!</p> 
    <a href="index.php"><div class="blue-button goback-btn">Go Back</div></a>      
</div>


<?php include 'js.php'; ?>
<!--
<script>
$(document).ready(function () {
    // Handler for .ready() called.
    window.setTimeout(function () {
        location.href = "https://vidatechft.com/pingola/index.php";
    }, 5000);
});

</script>-->
</body>
</html>