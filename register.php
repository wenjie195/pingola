<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// Program to display URL of current page.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
$link = "https";
else
$link = "http";
$link .= "://";
$link .= $_SERVER['HTTP_HOST'];
$link .= $_SERVER['REQUEST_URI'];
// echo $link;
$str1 = $link;
if(isset($_GET['referrerUID']))
{
    $referrerUidLink = $_GET['referrerUID'];
}
else 
{
    $referrerUidLink = "";
}

$visitor_ip=$_SERVER['REMOTE_ADDR'];
// $visitor_ip="25:25:25:25";

$query=" SELECT * FROM pageview WHERE userip = '$visitor_ip' ";
$result = mysqli_query($conn,$query);

// if(!$result)
// {
//     die("error".$query);
// }

$total_visitors=mysqli_num_rows($result);
if($total_visitors < 1)
// if($total_visitors != "")
// if($total_visitors)
{
    $query=" INSERT INTO pageview(userip,refer_uid) VALUES ('$visitor_ip','$referrerUidLink') ";
    $result = mysqli_query($conn,$query);
}

$query="SELECT * FROM pageview";
$result = mysqli_query($conn,$query);

// if(!$result)
// {
//     die("error".$query);
// }

$total_visitors=mysqli_num_rows($result);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/register.php" />
<meta property="og:title" content="Register | Pingola" />
<title>Register | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/register.php" />
<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    

    <h1 class="line-header margin-bottom50">Register</h1>

        <?php
        // Program to display URL of current page.
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
        else
        $link = "http";

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];

        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];

        // Print the link
        // echo $link;
        ?>

        <?php
        $str1 = $link;
        if(isset($_GET['referrerUID']))
        {
            $referrerUidLink = $_GET['referrerUID'];
        }
        else 
        {
            $referrerUidLink = "";
        }
        ?>

        <!-- <form action="utilities/registerFunction.php" method="POST" > -->
        <form method="POST" action="utilities/registerWithReferFunction.php">
			<div class="dual-input">
                <p class="input-top-p">Username</p>
                <input class="input-name clean" type="text" placeholder="Username" id="register_username" name="register_username" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Nickname</p>
                <input class="input-name clean" type="text" placeholder="Nickname" id="" name="" required>	
			</div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p">Email</p>
                <input class="input-name clean" type="email" placeholder="Type Your Email" id="register_email" name="register_email" required>	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Phone</p>
                <input class="input-name clean" type="text" placeholder="Phone Number" id="register_phone" name="register_phone" required>	
			</div>            
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p">Password</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="Password" id="register_password" name="register_password" required>        
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>
			</div>
            <div class="dual-input second-dual-input">
            <p class="input-top-p">Retype Password</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required> 
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">           
                </div>
			</div>
            <div class="clear"></div>
            <input class="input-name clean" type="hidden" value="<?php echo $referrerUidLink ?>" id="sponsor_id" name="sponsor_id" readonly required>	

            <button class="blue-button white-text width100 clean register-button"  name="register">Register</button>

            <div class="clear"></div>

            <!-- <div class="width100 text-center margin-top10">
                <a class="open-login bottom-a">Login</a>
            </div> -->

        </form>

    </div> 

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update refer's credit !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            // $messageType = "Input data for register already taken by other user !! <br> Please register again";
            $messageType = "Fail get submit data on referral history !!";
        }
        else if($_GET['type'] == 5)
        {
            // $messageType = "password must be more than 5 value !!";
            $messageType = "fail to register !!";
        }
        else if($_GET['type'] == 6) 
        {
            // $messageType = "Password is not match with retype password !!";
            $messageType = "Input data for register already taken by other user !! <br> Please register again";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "password must be more than 5 value !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "password and retype password not the same !!";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "unable to find related data sponsor ID in Referral table !!";
        }

        else if($_GET['type'] == 10)
        {
            $messageType = "unable to find related data sponsor ID !!";
        }
        else if($_GET['type'] == 11)
        {
            $messageType = "invalid sponsor ID !!";
        }


        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>