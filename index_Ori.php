<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('d M Y');

$conn = connDB();

// $countryList = getCountries($conn);
$gamesDetails = getGames($conn," Order By date_created ASC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/" />
<meta property="og:title" content="Pingola" />
<title>Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg">
	<h1 class="line-header margin-bottom50">Grab Now</h1>
    <div class="clear"></div>
    <div class="three-div-width">
    	<img src="img/pakages1.jpg" class="width100" alt="Pingola Promotion" title="Pingola Promotion">
    </div>
    <div class="three-div-width mid-three-width">
    	<img src="img/pakages2a.jpg" class="width100" alt="Pingola Promotion" title="Pingola Promotion">
    </div>
    <div class="three-div-width">
    	<img src="img/pakages3.jpg" class="width100" alt="Pingola Promotion" title="Pingola Promotion">
    </div>        
</div>
<div class="width100 div2 dark-bg padding-top-bottom same-padding">
	<h1 class="line-header margin-bottom50">Sign Up Now</h1>
	<div class="width100 overflow text-center">
    	<img src="img/ping2.png" class="ping-coin" alt="Sign Up Now" title="Sign Up Now">
    </div>
    <p class="explain-p">To get $5 Ping Cash for free!</p>
	<div class="width100 overflow text-center">
    	<div class="blue-button goback-btn open-register">Sign Up</div>
    </div>
</div>
<div class="menu-distance width100 div2 grey-bg padding-top-bottom same-padding">
	
    	<h1 class="line-header margin-bottom50">Live Game</h1>

    <div class="clear"></div>
        <div class="item width100">            
            <div class="clearfix width100 video-big-div">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li> 
                        <iframe width="100%" height="750px" src="https://www.youtube.com/embed/wucMZbj7lj8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="live-iframe" allowfullscreen></iframe>
                    </li>
                    <li> 
                        <iframe width="100%" height="750px" src="https://www.youtube.com/embed/1VjyIsaUGEI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="live-iframe" allowfullscreen></iframe>
                    </li>
                    <li> 
                        <iframe width="100%" height="750px" src="https://www.youtube.com/embed/JfGu3mf65XY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="live-iframe" allowfullscreen></iframe>
                    </li>
                    
                </ul>
            </div>
        </div>

</div>

<div class="menu-distance width100 same-padding betting-div dark-bg padding-top-bottom">
	<h1 class="line-header margin-bottom50">Waging</h1>
    <div class="clear"></div>
	<div class="darkblue-bg games-div">
    	<div class="game-logo-div">
        	<img src="img/dota2.png" class="width100" alt="Dota 2" title="Dota 2">
        </div>
    	<div class="game-logo-div">
        	<img src="img/league-of-legends.png" class="width100" alt="League of Legends" title="League of Legends">
        </div>        
    	<div class="game-logo-div">
        	<img src="img/counter-strike-global-offensive.png" class="width100" alt="Counter Strike: Global Offensive" title="Counter Strike: Global Offensive">
        </div>     
    	<div class="game-logo-div">
        	<img src="img/overwatch.png" class="width100" alt="Overwatch" title="Overwatch">
        </div>          
    	<div class="game-logo-div">
        	<img src="img/starcraft.png" class="width100" alt="StarCraft" title="StarCraft">
        </div> 
    	<div class="game-logo-div last-game-logo">
        	<img src="img/honor-of-kings.png" class="width100" alt="Honor of Kings" title="Honor of Kings">
        </div>        
                   
    </div>

    <div class="clear"></div>

    <?php
    if($uid != "")
    {
        if($gamesDetails)
        {
            for($cnt = 0;$cnt < count($gamesDetails) ;$cnt++)
            {
            ?>
            <form method="POST" action="placeWage.php" class="hover1">
                <div class="bluered-gradient-bg result-color-div width100">

                    <p class="tournament-p">
                        <?php echo $gamesDetails[$cnt]->getTitle();?> (<?php echo $time ;?>)
                    </p>
                    <div class="div1a">
                        <img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
                        <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamOne();?></p>
                    </div>
                    <div class="div2a">
                        <div class="emerald-bg rate-div rate-div1"><?php echo $gamesDetails[$cnt]->getValueOne();?></div>
                    </div>

                    <div class="div4a">
                        <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
                    </div>
                    <div class="div5a">
                        <img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
                        <p class="bet-team-name">Dota 2</p>
                    </div>  
                    <div class="div6a">
                        <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
                    </div>   

                    <div class="div8a">
                        <div class="red-bg rate-div rate-div2"><?php echo $gamesDetails[$cnt]->getValueTwo();?></div>
                    </div> 
                    <div class="div9a">
                        <img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
                        <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamTwo();?></p>
                    </div>  

                </div>

                <div class="width100 text-center margin-bottom50">
                    <button class="blue-button clean login-to-bet place-bet-button" type="submit" name="games_uid" value="<?php echo $gamesDetails[$cnt]->getUid();?>">
                        Place Your Wage
                    </button>
                    <!-- the file is inside js, hope it can be repeated, but if cannot then have to hide it -->
                    <div class="width100 text-center margin-top10 overflow">
                        <img src="img/timer-white.png" class="timer-png"><p class="timer-p white-timer"  id="timerP"></p>
                    </div>                
                </div>
            </form>
            <?php
            }
        }
    }
    else
    {
        if($gamesDetails)
        {
            for($cnt = 0;$cnt < count($gamesDetails) ;$cnt++)
            {
            ?>
            <form method="POST" action="placeWage.php" class="hover1">
                <div class="bluered-gradient-bg result-color-div width100">

                    <p class="tournament-p">
                        <?php echo $gamesDetails[$cnt]->getTitle();?> (<?php echo $time ;?>)
                    </p>
                    <div class="div1a">
                        <img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
                        <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamOne();?></p>
                    </div>
                    <div class="div2a">
                        <div class="emerald-bg rate-div rate-div1"><?php echo $gamesDetails[$cnt]->getValueOne();?></div>
                    </div>

                    <div class="div4a">
                        <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
                    </div>
                    <div class="div5a">
                        <img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
                        <p class="bet-team-name">Dota 2</p>
                    </div>  
                    <div class="div6a">
                        <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
                    </div>   

                    <div class="div8a">
                        <div class="red-bg rate-div rate-div2"><?php echo $gamesDetails[$cnt]->getValueTwo();?></div>
                    </div> 
                    <div class="div9a">
                        <img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
                        <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamTwo();?></p>
                    </div>  

                </div>

            </form>
            <?php
            }
        }
        ?>
            <div class="blue-button clean login-to-bet open-login">Login to Continue</div> 
        <?php
    }
    ?>

    <!-- <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	StarLadder ImbaTV Dota 2 Minor Season 3 ( 5 May 2020 10:00 pm)
        </p>
    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
            <p class="bet-team-name">Dota 2</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>                    
    </div>

    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	UKLC 2020 Summer ( 5 May 2020 10:00 pm)
        </p>

    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/league-of-legends.png" alt="League of Legends" title="League of Legends" class="bet-game-logo team-logo">
            <p class="bet-team-name">League of Legends</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>         
    </div>

    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	Overwatch League ( 5 May 2020 10:00 pm)
        </p>    
    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/overwatch.png" alt="Overwatch" title="Overwatch" class="bet-game-logo team-logo">
            <p class="bet-team-name">Overwatch</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
    </div>
   <div class="blue-button clean login-to-bet open-login">Login to Continue</div>  -->
    
</div>

<div class="width100 same-padding betting-div grey-bg padding-top-bottom">
	<h1 class="line-header margin-bottom50">Result of Games</h1>
    <div class="clear"></div>

    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	StarLadder ImbaTV Dota 2 Minor Season 3 ( 5 May 2020 10:00 pm)
        </p>
    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="win-trophy"><img src="img/empty.png" class="win-png empty"></div>
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
            <p class="bet-team-name">Dota 2</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="win-trophy"><img src="img/win.png" class="win-png" alt="Winner" title="Winner"></div>
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
        
                     
    </div>

    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	UKLC 2020 Summer ( 5 May 2020 10:00 pm)
        </p>

    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="win-trophy"><img src="img/win.png" class="win-png" alt="Winner" title="Winner"></div>
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
            
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/league-of-legends.png" alt="League of Legends" title="League of Legends" class="bet-game-logo team-logo">
            <p class="bet-team-name">League of Legends</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="win-trophy"><img src="img/empty.png" class="win-png empty"></div>
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
        
                     
    </div>

    <div class="bluered-gradient-bg result-color-div width100">
    	<p class="tournament-p">
        	Overwatch League ( 5 May 2020 10:00 pm)
        </p>    
    	<div class="div1a">
        	<img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
            <p class="bet-team-name">SCABBARD</p>
        </div>
        <div class="div2a">
        	<div class="win-trophy"><img src="img/win.png" class="win-png" alt="Winner" title="Winner"></div>
        	<div class="emerald-bg rate-div rate-div1">1.890</div>
            
        </div>

        <div class="div4a">
        	<img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
        </div>
        <div class="div5a">
        	<img src="img/overwatch.png" alt="Overwatch" title="Overwatch" class="bet-game-logo team-logo">
            <p class="bet-team-name">Overwatch</p>
        </div>  
        <div class="div6a">
        	<img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
        </div>   
           
        <div class="div8a">
        	<div class="win-trophy"><img src="img/empty.png" class="win-png empty"></div>
        	<div class="red-bg rate-div rate-div2">1.920</div>
        </div> 
        <div class="div9a">
        	<img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
            <p class="bet-team-name">SILENT NIGHT</p>
        </div>        
        
                     
    </div>
   
    
    
    
</div>
<div class="width100 same-padding betting-div dark-bg padding-top-bottom">
	<h1 class="line-header margin-bottom50">Blog and News</h1>
    <div class="clear"></div>
    <a href="https://www.dailyesports.gg/australias-new-couch-warriors-league-lets-players-compete-from-home/" class="opacity-hover" target="_blank">
    	<div class="news-big-div overflow">
            <div class="news-image-container">
                <img src="img/australia-couch-warriors-league.jpg" class="news-pic width100" alt="Australia’s new Couch Warriors League lets players compete from home" title="Australia’s new Couch Warriors League lets players compete from home">
            </div>
        	<div class="news-content">
            	<p class="news-title text-overflow">Australia’s new Couch Warriors League lets players compete from home</p>
                <p class="news-date">05/04/2020</p>
                <p class="news-content-p">
                	Due to the current pandemic, offline events around the world have been canceled, including the Pokémon Championships, Overwatch League, DreamHack Masters, and more. Amidst these cancellations, a new online League has risen in Australia: the National Couch Warriors League. Couch Warriors are the same team behind Battle Arena Melbourne, Australia’s international fighting game major.
                </p>
            </div>
      	</div>
    </a>
   <a href="https://www.formula1.com/en/latest/article.formula-1-launches-virtual-grand-prix-series-to-replace-postponed-races.1znLAbPzBbCQPj1IDMeiOi.html" class="opacity-hover" target="_blank">
    	<div class="news-big-div overflow">
            <div class="news-image-container">
                <img src="img/f1.jpg" class="news-pic width100" alt="Formula 1 Launches Virtual Grand Prix Series to Replace Postponed Races" title="Formula 1 Launches Virtual Grand Prix Series to Replace Postponed Races">
            </div>
        	<div class="news-content">
            	<p class="news-title text-overflow">Formula 1 Launches Virtual Grand Prix Series to Replace Postponed Races</p>
                <p class="news-date">20/03/2020</p>
                <p class="news-content-p">
                	Formula 1 has today announced the launch of a new F1 Esports Virtual Grand Prix series, featuring a number of current F1 drivers. The series has been created to enable fans to continue watching Formula 1 races virtually, despite the ongoing COVID-19 situation that has affected this season’s opening race calendar.
                </p>
            </div>
    	</div>
    </a>
    <a href="https://www.insidethegames.biz/articles/1091356/esports-world-championship-games-2020" class="opacity-hover" target="_blank">
    	<div class="news-big-div overflow">
            <div class="news-image-container">
                <img src="img/eilat2020.png" class="news-pic width100" alt="International Esports Federation Announce Games and Dates for 2020 World Championship" title="International Esports Federation Announce Games and Dates for 2020 World Championship">
            </div>
        	<div class="news-content">
            	<p class="news-title text-overflow">International Esports Federation Announce Games and Dates for 2020 World Championship</p>
                <p class="news-date">02/03/2020</p>
                <p class="news-content-p">
                	"It would be an honour to have the defending world champions for eFootball PES series, Tekken 7 and DOTA 2 attend IeSF's 12th World Championships," said IeSF President, Colin Webster, according to Animation Xpress.
                </p>
            </div>
    	</div>
    </a>
    <a href="https://escharts.com/blog/most-anticipated-esports-events-2020" target="_blank" class="opacity-hover">
    	<div class="news-big-div overflow">
            <div class="news-image-container">
                <img src="img/esport-events.jpg" class="news-pic width100" alt="The Most Anticipated Esports Events of 2020" title="The Most Anticipated Esports Events of 2020">
            </div>
        	<div class="news-content">
            	<p class="news-title text-overflow">The Most Anticipated Esports Events of 2020</p>
                <p class="news-date">27/01/2020</p>
                <p class="news-content-p">
                	It makes sense to start with the tournament that caused an uproar in 2019 – 2019 LoL World Championship with its incredible 4 million peak viewers mark. Everyone expects this result to be further improved on in 2020. Another point which makes this tournament worth the wait is the amazing level of rivalry between the Western and Eastern teams.
                </p>
            </div>
    	</div>
    </a>


    
</div>
<div class="width100 same-padding betting-div padding-top-bottom bg1">
	<h1 class="line-header margin-bottom50">About Us</h1>
    <div class="clear"></div>
	<p class="about-us-p">
    	Pingola is a gamer platform created for gamers by gamers. We all know the frustrations that each gamer has through different time horizons and here at Pingola, we are offering all kinds of features to ensure your horizon of the gaming world is well covered. If you are an avid gamer and would like to see more from us, please do drop us your suggestions at <b>forgamers@pingola.games</b>
    </p>
</div>
<div class="width100 same-padding betting-div padding-top-bottom bg2">
	<h1 class="line-header margin-bottom50">Contact Us</h1>
    <div class="clear"></div>
	<div class="left-contact-div">
    	<img src="img/logo.png" class="contact-logo" alt="Pingola" title="Pingola">
    	<!--<p class="contact-p">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rhoncus lorem ut diam fermentum suscipit. Etiam ut varius velit, nec cursus nunc. Praesent varius maximus rutrum. Mauris id vehicula neque. Morbi efficitur porta volutpat. Duis nec magna a tellus tempus tincidunt a sed ex. Vivamus felis est, tristique in purus eu, suscipit fringilla urna. Aliquam pulvinar metus in lacus ultrices, id dapibus ante iaculis. Aliquam accumsan ipsum sit amet justo ultricies, non luctus risus suscipit. Suspendisse potenti. Pellentesque viverra sapien quis dolor posuere, sit amet cursus libero finibus. Fusce aliquam condimentum pulvinar. Donec non malesuada ligula. Etiam pulvinar metus eu porta iaculis. Ut imperdiet dignissim pretium.
        </p>
        <p class="contact-info-p"><img src="img/contact-phone.png" class="contact-icon" alt="Phone No."> XX - XXX XXXX</p>-->
    	<p class="contact-info-p"><img src="img/address.png" class="contact-icon" alt="Address"> 360 Orchard Road, No 09 - 09 International Building, Singapore 238869</p>
        <p class="contact-info-p"><img src="img/email2.png" class="contact-icon" alt="Email"> helpgamers@pingola.games</p>
        <!--<p class="contact-info-p"><img src="img/terms.png" class="contact-icon" alt="Terms"> Terms and Policies</p>-->
    </div>
	<div class="right-contact-div">
    	<form id="contactform" method="post" action="index.php" class="form-class extra-margin">
        <input type="text" name="name" placeholder="Name" class="input-name clean" required >
                  
                  <input type="email" name="email" placeholder="Email" class="input-name clean" required >                  
                  
                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" required >


                                  
                  <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                  <div class="clear"></div>
                  <table class="form-table">
                  	<tbody>
                        <tr>
                            <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required></td>
                            <td><p class="opt-msg">I want to be updated with more information about your company's news and future promotions</p></td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required></td>
                            <td><p class="opt-msg"> I just want to be contacted based on my request/inquiry</p></td>
                        </tr>   
                    </tbody>                
                  </table>
                   
                  <div class="res-div"><input type="submit" name="send_email_button" value="SEND US A MESSAGE" class="input-submit blue-button white-text clean pointer"></div>
                </form> 
        
        </form>
    </div>
</div>



<?php include 'js.php'; ?>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com, kevinyam.vidatech@gmail.com, forgamers@pingola.games, helpgamers@pingola.games";
    $email_subject = "Contact Form via Pingola website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Register unsuccessfully !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Input data for register already taken by other user !! <br> Please register again";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "password must be more than 5 value !!";
        }
        else if($_GET['type'] == 6) 
        {
            $messageType = "Password is not match with retype password !!";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Unknown user type !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Incorrect Password";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "This user name does not exist !! <br> Please register !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>