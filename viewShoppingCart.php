<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getPreOrderList($conn, "WHERE user_uid = ? ",array("user_uid"),array($uid),"s");
$products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
// $products = getPreOrderList($conn, "WHERE user_uid = ? AND status != 'Delete' ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/viewShoppingCart.php" />
<meta property="og:title" content="View Cart | Pingola" />
<title>View Cart | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/viewShoppingCart.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<h1 class="line-header margin-bottom50">Cart</h1>
    <div class="clear"></div>

    <!-- <form method="POST" action="utilities/orderListFunction.php"> -->

        <div class="overflow-scroll-div margin-top30 same-padding-tdh">
            <table class="table-css">
                <thead>
                    <tr>
                        <!-- <th><input type="checkbox" id="checkAll" name="" value=""> </th> -->
                        <th>No.</th>
                        <th>Product Name</th>
                        <th>Unit Price (RM)</th>
                        <th>Quantity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Subtotal (RM)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <!-- <td>
                                        <input class="checkBox" name="arr[]" type="checkbox" value="<?php //echo $products[$cnt]->getId();?>">
                                    </td> -->
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $products[$cnt]->getProductName();?></td>
                                    <td><?php echo $products[$cnt]->getOriginalPrice();?></td>

                                    <!-- <td><?php echo $products[$cnt]->getQuantity();?></td> -->

                                    <td class="quantity-td">
                                    
                                        <form method="POST" action="utilities/preOrderSubtractFunction.php" class="oz-form">
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                            <button class="clean left-minus oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                -
                                            </button>
                                        </form>

                                       
                                        <p class="oz-p"><?php echo $products[$cnt]->getQuantity();?></p>
                                       

                                        <form method="POST" action="utilities/preOrderAddFunction.php"  class="oz-form">
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getOriginalPrice();?>" id="product_price" name="product_price" readonly>
                                            <input class="clean" type="hidden" value="<?php echo $products[$cnt]->getQuantity();?>" id="product_quantity" name="product_quantity" readonly>
                                            <button class="clean right-add oz-btn" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                +
                                            </button>
                                        </form>
                                        
                                    </td>

                                    <td><?php echo $products[$cnt]->getTotalPrice();?></td>

                                    <td>
                                        <form method="POST" action="utilities/deletePreOrderFunction.php" class="hover1">
                                            <button class="clean img-btn transparent-button pointer" type="submit" name="preorder_id" value="<?php echo $products[$cnt]->getId();?>">
                                                <img src="img/delete1a.png" class="edit-icon1" alt="Delete" title="Delete">
                                               
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>

            <?php
                if($products)
                {
                ?>
                    <form method="POST" action="utilities/orderListFunction.php" class="middle-div" >
                        <button class="cart-button mid-btn-pls white-text clean2 width100" style="margin-top:50px;" type="submit" name="user_uid" value="<?php echo $products[0]->getUserUid();?>">Checkout</button>
                    </form>
                <?php
                }
            ?> 

            
                <p class="text-center" style="margin-top:20px;"><a style="font-size:18px;" href="shopping.php" class="white-a opacity-hover">Continue Shopping</a></p>
           

        </div>

        <!-- <button class="clean tele-btn" type="submit" id="submit" name="order_list" value="<?php //echo $products[$cnt]->getId(); ?>">Proceed To Checkout</button> -->

    <!-- </form> -->

</div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<script>
  $("#checkAll").on("click",function(){
    var check = $(this).is(':checked')?true:false;
      $(".checkBox").prop('checked',check);
  });
</script>

</body>
</html>