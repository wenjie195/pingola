<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$depositVip = getDeposit($conn," WHERE upgrade = 'Yes' AND status = 'Pending' ");
$deposit = getDeposit($conn," WHERE upgrade = 'No' AND status = 'Pending' ");

// $sharingDetails = getSharing($conn);
$sharingDetails = getSharing($conn,"WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminDashboard.php" />
<meta property="og:title" content="Admin Dashboard | Pingola" />
<title>Admin Dashboard | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminDashboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="menu-distance width100 same-padding div1 grey-bg min-height">

	<div class="width100 overflow text-center">

    <h3 class="green-text h1-title">Deposit With Upgrade VIP</h3>        
        <table class="green-table width100">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Amount</th>
                        <th>Upgrade Status</th>
                        <th>Date</th>
                        <th>Review</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($depositVip)
                    {
                        for($cnt = 0;$cnt < count($depositVip) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $depositVip[$cnt]->getUsername();?></td>
                                <td><?php echo $depositVip[$cnt]->getAmount();?></td>
                                <td><?php echo $depositVip[$cnt]->getUpgrade();?></td>
                                <td><?php echo $depositVip[$cnt]->getDateCreated();?></td>
                                <!-- <td>View More</td> -->
                                <td>
                                    <form method="POST" action="utilities/adminApprovedDepositVipFunction.php" class="hover1">
                                        <button class=" " type="submit" name="deposit_uid" value="<?php echo $depositVip[$cnt]->getUid();?>">
                                            Approved
                                        </button>
                                    </form>

                                    <form method="POST" action="utilities/adminRejectDepositFunction.php" class="hover1">
                                        <button class=" " type="submit" name="deposit_uid" value="<?php echo $depositVip[$cnt]->getUid();?>">
                                            Rejected
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
        </table>

        <h3 class="green-text h1-title">Deposit</h3>        
        <table class="green-table width100">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Review</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($deposit)
                    {
                        for($cntAA = 0;$cntAA < count($deposit) ;$cntAA++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cntAA+1)?></td>
                                <td><?php echo $deposit[$cntAA]->getUsername();?></td>
                                <td><?php echo $deposit[$cntAA]->getAmount();?></td>
                                <td><?php echo $deposit[$cntAA]->getDateCreated();?></td>
                                <!-- <td>View More</td> -->
                                <td>
                                    <form method="POST" action="utilities/adminApprovedDepositFunction.php" class="hover1">
                                        <button class=" " type="submit" name="deposit_uid" value="<?php echo $deposit[$cntAA]->getUid();?>">
                                            Approved
                                        </button>
                                    </form>

                                    <form method="POST" action="utilities/adminRejectDepositFunction.php" class="hover1">
                                        <button class=" " type="submit" name="deposit_uid" value="<?php echo $deposit[$cntAA]->getUid();?>">
                                            Rejected
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
        </table>

        <h3 class="green-text h1-title">Live Video</h3>      

        <a href="adminAddLiveVideo.php">
            <div class="margin-auto clean-button clean register-button pink-button" >
                Add New Video
            </div>
        </a>

        <table class="green-table width100">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Link</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if($sharingDetails)
                    {
                        for($cntBB = 0;$cntBB < count($sharingDetails) ;$cntBB++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cntBB+1)?></td>
                                <td><?php echo $sharingDetails[$cntBB]->getLink();?></td>
                                <td><?php echo $sharingDetails[$cntBB]->getStatus();?></td>
                                <td><?php echo $sharingDetails[$cntBB]->getDateCreated();?></td>
                                <td>
                                    <form method="POST" action="utilities/stopSharingFunction.php" class="hover1">
                                        <button class="clean action-button" type="submit" name="video_uid" value="<?php echo $sharingDetails[$cntBB]->getUid();?>">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
        </table>

    </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>