<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$timestamp = time();
// $timestamp = date_default_timezone_set("Asia/Kuala_Lumpur");

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$todayDate = date('Y-m-d');
$nextDate = date('Y-m-d',strtotime($todayDate. " + 1 Day"));
$betDetailsToday = getBetStatus($conn, "WHERE result != 'PENDING' AND date_created >=? AND date_created <?",array("date_created,date_created"),array($todayDate,$nextDate), "ss");
$betDetailsWeek = getBetStatus($conn, "WHERE WEEKOFYEAR(`date_created`)=WEEKOFYEAR(NOW())");
$betDetailsMonth = getBetStatus($conn, "WHERE YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW());");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/leaderboard.php" />
<meta property="og:title" content="Leaderboard | Pingola" />
<title>Leaderboard | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/leaderboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
	<div class="width100 overflow text-center">



     <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">Leaderboard</h1>
        <div class="w3-container">


          <div class="w3-row">
            <a href="javascript:void(0)" onclick="openCity(event, 'London');">
              <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Today</div>
            </a>
            <a href="javascript:void(0)" onclick="openCity(event, 'Paris');">
              <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">This Week</div>
            </a>
            <a href="javascript:void(0)" onclick="openCity(event, 'Tokyo');">
              <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">This Month</div>
            </a>
          </div>

          <div id="London" class="w3-container city" style="display:block;">
        <div class="overflow-scroll-div margin-top30">
        <p class="leaderboard-p">Today</p>
        <table class="glory-board">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Nickname</th>
                    <th>Total Win (PingCash)</th>
                </tr>
            </thead>
            <tbody>
            	<?php
              if ($betDetailsToday) {
                for ($i=0; $i <count($betDetailsToday) ; $i++) {
                  if ($betDetailsToday[$i]->getResult() == 'WIN') {
                    $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($betDetailsToday[$i]->getUid()), "s");
                    ?>
                    <tr>
                      <td><?php echo $i+1 ?></td>
                      <td><?php echo $userDetails[0]->getUsername() ?></td>
                      <td class="win-color"><?php echo "+ ".$betDetailsToday[$i]->getAmount() ?></td>
                    </tr>
                    <?php
                  }
                }
              }
               ?>
            </tbody>
        </table>
        </div>
          </div>

          <div id="Paris" class="w3-container city" style="display:none">

        <div class="overflow-scroll-div margin-top30">
        <p class="leaderboard-p">This Week</p>
        <table class="glory-board">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Nickname</th>
                    <th>Total Win (PingCash)</th>
                </tr>
            </thead>
            <tbody>
              <?php
              if ($betDetailsWeek) {
                for ($i=0; $i <count($betDetailsWeek) ; $i++) {
                  if ($betDetailsWeek[$i]->getResult() == 'WIN') {
                    $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($betDetailsWeek[$i]->getUid()), "s");
                    ?>
                    <tr>
                      <td><?php echo $i+1 ?></td>
                      <td><?php echo $userDetails[0]->getUsername() ?></td>
                      <td class="win-color"><?php echo "+ ".$betDetailsWeek[$i]->getAmount() ?></td>
                    </tr>
                    <?php
                  }
                }
              }
               ?>
            </tbody>
        </table>
        </div>
          </div>

          <div id="Tokyo" class="w3-container city" style="display:none">

        <div class="overflow-scroll-div margin-top30">
        <p class="leaderboard-p">This Month</p>
        <table class="glory-board">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Nickname</th>
                    <th>Total Win (PingCash)</th>
                </tr>
            </thead>
            <tbody>
              <?php
              if ($betDetailsMonth) {
                for ($i=0; $i <count($betDetailsMonth) ; $i++) {
                  if ($betDetailsMonth[$i]->getResult() == 'WIN') {
                    $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($betDetailsMonth[$i]->getUid()), "s");
                    ?>
                    <tr>
                      <td><?php echo $i+1 ?></td>
                      <td><?php echo $userDetails[0]->getUsername() ?></td>
                      <td class="win-color"><?php echo "+ ".$betDetailsMonth[$i]->getAmount() ?></td>
                    </tr>
                    <?php
                  }
                }
              }
               ?>
            </tbody>
        </table>
        </div>
        </div>
  	</div>
    </div>

</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>
