<?php

include 'css.php';

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches?token=F7U9j7gsyiDz8BzpTePBShxkAJ2hGrEeZVrP24Dk0OwZlq7s40U");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);


$exchangeData = json_decode($output, true);
 ?>
 <div class="width100 shipping-div2">
     <?php //$conn = connDB();?>
         <table id="singleAdvancedTable" class="shipping-table">
             <thead>
                 <tr>
                     <th class="th">NO.</th>
                     <th class="th">TOURNAMENT LOGO</th>
                     <th class="th">MATCH TYPE</th>
                     <th class="th">TOURNAMENT NAME</th>
                     <th class="th">STATUS</th>
                     <th class="th">MATCH</th>
                     <th class="th">SCHEDULED DATE</th>

                     <!-- <th>INVOICE</th> -->
                 </tr>
             </thead>
             <tbody>
                 <?php
                 // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                 // {

                 if ($exchangeData) {
                   for ($i=0; $i <count($exchangeData) ; $i++) {
                     $No++;
                     ?>
                     <tr>
                      <td class="td"><?php echo $No ?></td>
                     <td class="td"><?php echo $exchangeData[$i]['match_type'] ?></td>
                     <td class="td"><img style="width: 100px;height: 100px" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""></td>
                     <td class="td"><?php echo $exchangeData[$i]['league']['name'] ?></td>
                     <td class="td"><?php echo $exchangeData[$i]['status'] ?></td>
                     <td class="td"><?php echo $exchangeData[$i]['name'] ?></td>
                     <td class="td"><?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?></td>
                   </tr>
                     <?php
                   }
                 }

                 //}
                 ?>
             </tbody>
         </table>
