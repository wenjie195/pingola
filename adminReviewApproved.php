<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Reviews.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

if (isset($_GET['search'])) {
  $reviews = getReviews($conn, " WHERE display = 'Yes' AND title=? ",array("title"),array($_GET['search']), "s");
}else {
  $reviews = getReviews($conn, " WHERE display = 'Yes' ");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminReviewApproved.php" />
<meta property="og:title" content="Approved Review | Pingola" />
<title>Approved Review | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminReviewApproved.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="green-text h1-title"><a href="adminReviewPending.php" class="green-a opacity-hover2">Pending Reviews</a> | Approved | <a href="adminReviewRejected.php" class="green-a opacity-hover2">Rejected</a><!-- | <a href="reportedReview.php" class="green-a">Reported</a>--></h1>
           
        </div>
        <div class="width100 ship-bottom-div">
          <?php //echo $_SERVER["PHP_SELF"] ?>
            <form action="search/searchFunction.php" method="post">
                    <?php
                    if (isset($_GET['search'])) {
                      ?>
                      <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                      <?php
                    }else {
                      ?>
                      <input class="line-input clean" type="text" name="search" placeholder="Search">
                      <?php
                    }
                     ?>
                     <!-- SKU and pet name-->
                     <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                     <button class="search-btn" type="submit">
                           <img src="img/search.png" class="visible-img mid-search-img opacity-hover" alt="Search" title="Search">
                          
                     </button>
              </form>

        </div>
    </div>


    <div class="clear"></div>
	<div class="overflow-scroll-div margin-top30 same-padding-tdh">
    	<table class="table-css">
        	<thead>
            	<tr>
                	<th class="first-column table-green-a">No.</th>
                    <th>Link</th>
                    <th>Review</th>
                    <th>Status</th>
                    <th>Remove</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($reviews)
                    {
                        for($cnt = 0;$cnt < count($reviews) ;$cnt++)
                        {
                        ?>

                            <tr>
                                <td class="first-column"><?php echo ($cnt+1)?>.</td>

                                <td><a href="<?php echo "shopping-details.php?id=".$reviews[$cnt]->getCompanyUid() ?>" class="green-a table-green-a">Product Link</a></td>

                                <td>
                                    <div class="table-left-review-data">
                                            <p class="table-review-username-p"><?php echo $reviews[$cnt]->getAuthorName();?></p>
                                            <div class="review-star-div">
                                                <p class="table-review-username-p">
                                                    Rating : <?php $display = $reviews[$cnt]->getRating();?>
                                                        <?php
                                                            if ($display == 1)
                                                            {
                                                                echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                            }
                                                            else if ($display == 2)
                                                            {
                                                                echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                            }
                                                            else if ($display == 3)
                                                            {
                                                                echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                                    '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                            }
                                                            else if ($display == 4)
                                                            {
                                                                echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                                    '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                            }
                                                            else if ($display == 5)
                                                            {
                                                                echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                                '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                                                '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                                            }
                                                        ?>
                                                </p>
                                            </div>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="table-review-comment">
                                        <a href="reviewImages/<?php echo $reviews[$cnt]->getImage();?>" data-fancybox="images-preview"  >
                                            <img src="reviewImages/<?php echo $reviews[$cnt]->getImage();?>" class="table-review-img opacity-hover"  onerror="this.style.display='none'">
                                        </a>
                                        <p class="table-review-p"><?php echo $reviews[$cnt]->getParagraphOne();?></p>
                                        <p class="table-date-p"><?php echo $date = date("d/m/Y",strtotime($reviews[$cnt]->getDateCreated()));?></p>
                                    </div>
                                </td>

                                <td class="table-green-a green-text light-green-text">
                                    <?php
                                        $display = $reviews[$cnt]->getDisplay();
                                        if($display == 'Yes')
                                        {
                                            echo "Approved";
                                        }
                                        else
                                        {   }
                                    ?>
                                </td>

                                <td>

                                    <form method="POST" action="utilities/adminReviewDeleteFunction.php">
                                        <input class="input-name clean input-textarea" type="hidden" value="<?php echo $reviews[$cnt]->getRating();?>" name="rate" id="rate" readonly>
                                        <input class="input-name clean input-textarea" type="hidden" value="<?php echo $reviews[$cnt]->getCompanyUid();?>" name="issue_uid" id="issue_uid" readonly>
                                        <button class="approve-button opacity-hover clean transparent-button" name="review_uid" value="<?php echo $reviews[$cnt]->getUid();?>">
                                            <img src="img/reject.png" class="approve-png" alt="Remove" title="Remove">
                                        </button>
                                    </form>

                                </td>
                            </tr>

                        <?php
                        }
                    }
                ?>
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td class="table-green-a green-text light-green-text">Approved</td>
                    <td>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove" title="Remove"></button>

                    </td>

                </tr>
            	<tr>
                	<td class="first-column table-green-a">2.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td class="table-green-a green-text light-green-text">Approved</td>
                    <td>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove" title="Remove"></button>

                    </td>

                </tr>
            	<tr>
                	<td class="first-column table-green-a">3.</td>
                    <td><a href="malaysia-pets-products-details.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>
                    </td>
                    <td class="table-green-a green-text light-green-text">Approved</td>
                    <td>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove" title="Remove"></button>

                    </td>

                </tr>
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review Published !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "ERROR 1";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "ERROR 2";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR 3";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR 4";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
