<div class="footer-div">
	<p class="footer-p">@ <?php echo $time;?> Pingola, All Rights Reserved.</p>
</div>
<!-- Social Media Share Modal -->
<div id="social-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content share-modal-size">
    <span class="close-css close-social">&times;</span>
    <h2 class="black-text share-title">Share</h2>
    
    <div class="clear"></div>
    <div class="width100 mini-height30"></div>
    <div class="clear"></div>
		<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        					<a class="a2a_button_whatsapp"></a>
                            <a class="a2a_button_facebook social-a"></a>
                            <a class="a2a_button_twitter social-a"></a>
                            <a class="a2a_button_facebook_messenger social-a"></a>
                            <a class="a2a_button_wechat social-a"></a>
                            <a class="a2a_button_copy_link social-a"></a>
                            
                            
		</div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
  </div>

</div>
<!-- Register Modal -->
<div id="register-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css register-modal-content">
    <span class="close-css close-register">&times;</span>
    <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title">Register</h2>
    <form action="utilities/registerFunction.php" method="POST" >
		<p class="input-top-p">Username</p>
        <input class="input-name clean" type="text" placeholder="Username" id="register_username" name="register_username" required>
		<p class="input-top-p">Email</p>
        <input class="input-name clean" type="email" placeholder="Type Your Email" id="register_email" name="register_email" required>	
        <p class="input-top-p">Phone</p>
        <input class="input-name clean" type="text" placeholder="Phone Number" id="register_phone" name="register_phone" required>	
        <p class="input-top-p">Password</p>
        <div class="password-input-div">
        	<input class="input-name clean password-input" type="password" placeholder="Password" id="register_password" name="register_password" required>        
			<img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
        </div>
        <p class="input-top-p">Retype Password</p>
        <div class="password-input-div">
        	<input class="input-name clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required> 
        	<img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">           
		</div>
        <button class="blue-button white-text width100 clean register-button"  name="register">Register</button>
        <div class="clear"></div>
        <div class="width100 text-center margin-top10">
        	<a class="open-login bottom-a">Login</a>
        </div>
    </form>
  </div>

</div>





<!-- Login Modal -->
<div id="login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css register-modal-content">
  <span class="close-css close-login">&times;</span>
  <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title">Login</h2>
    <form action="utilities/loginFunction.php" method="POST" >
      <p class="input-top-p">Username</p>
        <input class="input-name clean" type="text" placeholder="Username" name="username" id="username" required>
      <p class="input-top-p">Password</p>
      <div class="password-input-div">
        <input class="input-name clean password-input" type="password" placeholder="Password" name="password" id="password" required> 
        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">                 
      </div>
      <button class="blue-button white-text width100 clean register-button"  name="login">Login</button>
      <div class="clear"></div>
      <div class="width100 text-center margin-top10">
        <a class="open-register bottom-a">Register</a>
      </div>
      <div class="clear"></div>
      <div class="width100 text-center margin-top10">
        <a class="open-forgot bottom-a">Forgot Password</a>
      </div>        
    </form>
  </div>

</div>
<!-- Forgot Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css register-modal-content">
  <span class="close-css close-forgot">&times;</span>
  <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title">Forgot Password</h2>
   <form method="POST" action="utilities/forgotPasswordFunction.php">
      <p class="input-top-p">Email</p>
        <input class="input-name clean" type="email" placeholder="Type Your Email" id="forgot_password" name="forgot_password" required>

      <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>
     
    </form>
  </div>

</div>
<!-- Wage Modal -->
<div id="wage-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css register-modal-content">
    <span class="close-css close-wage">&times;</span>
    <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Place Your Wage</h2>
    <!-- <p class="black-text match-title">StarLadder ImbaTV Dota 2 Minor Season 3 ( 5 May 2020 10:00 pm)</p> -->
    <p class="black-text match-title">StarLadder ImbaTV Dota 2 Minor Season 3 (<?php echo $time ;?>)</p>
    <form >
			<div class="dual-input">
                <p class="input-top-p big-top-p">Team Scabbard</p>
                <input class="input-name clean" type="number" placeholder="0 PingCash" id="" name="" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p big-top-p">Team Silent Night</p>
                <input class="input-name clean" type="number" placeholder="0 PingCash" id="" name="" required>	
			</div>    

       
        <button class="blue-button white-text width100 clean register-button"  name="">Confirm</button>

       
    </form>
  </div>

</div>

<!-- Top Up Modal -->
<!-- <div id="topup-modal" class="modal-css">

  <div class="modal-content-css register-modal-content">
    <span class="close-css close-topup">&times;</span>
    <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Top Up</h2>
    
    <form >
			<div class="dual-input">
                <p class="input-top-p big-top-p long-p">How Many PingCash You Would Like to Top Up?</p>
                <input class="input-name clean" type="number" placeholder="0 PingCash" id="" name="" required>
			</div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash?</p>
                <select  class="input-name clean">
                	<option>No</option>
                    <option>Yes</option>
                </select>
			</div>    

       
        <button class="blue-button white-text width100 clean register-button"  name="">Confirm</button>

       
    </form>
  </div>

</div> -->




<!-- Thank You Modal -->
<div id="thankyou-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css ping-modal-content">
    <span class="close-css close-thankyou">&times;</span>
    <div class="clear"></div>
    <h2 class="black-text h2-title text-center reg-title">Thank You</h2>
    <div class="width100 overflow text-center">
    	<img src="img/ping.png" class="ping" alt="Thank You" title="Thank You">
    </div>
    <p class="explain-p">You are currently rewarded with <b class="black-text">$5 Ping Cash</b>!</p>
  </div>

</div>


<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>


<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<!--<script type="text/javascript" src="js/lightslider.min.js"></script>
    <script>
    	 $(document).ready(function() {

            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:0,
                slideMargin: 0,
                speed:500,
                auto:false,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
    </script>-->
<script>

var registermodal = document.getElementById("register-modal");
var loginmodal = document.getElementById("login-modal");
var forgotmodal = document.getElementById("forgot-modal");
var wagemodal = document.getElementById("wage-modal");
var topupmodal = document.getElementById("topup-modal");
var upgrademodal = document.getElementById("upgrade-modal");
var filtermodal = document.getElementById("filter-modal");
var filtermodal2 = document.getElementById("filter-modal2");
var socialmodal = document.getElementById("social-modal");
var reviewmodal = document.getElementById("review-modal");


var openregister = document.getElementsByClassName("open-register")[0];
var openregister1 = document.getElementsByClassName("open-register")[1];
var openregister2 = document.getElementsByClassName("open-register")[2];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openlogin5 = document.getElementsByClassName("open-login")[5];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openwage = document.getElementsByClassName("open-wage")[0];
var openwage1 = document.getElementsByClassName("open-wage")[1];
var openwage2 = document.getElementsByClassName("open-wage")[2];
var opentopup = document.getElementsByClassName("open-topup")[0];
var opentopup1 = document.getElementsByClassName("open-topup")[1];
var openupgrade = document.getElementsByClassName("open-upgrade")[0];
var openupgrade1 = document.getElementsByClassName("open-upgrade")[1];
var openfilter = document.getElementsByClassName("open-filter")[0];
var openfilter2 = document.getElementsByClassName("open-filter2")[0];
var opensocial = document.getElementsByClassName("open-social")[0];
var openreview = document.getElementsByClassName("open-review")[0];
var openreview1 = document.getElementsByClassName("open-review")[1];
var openreview2 = document.getElementsByClassName("open-review")[2];

var closeregister = document.getElementsByClassName("close-register")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closewage = document.getElementsByClassName("close-wage")[0];
var closetopup = document.getElementsByClassName("close-topup")[0];
var closeupgrade = document.getElementsByClassName("close-upgrade")[0];
var closefilter = document.getElementsByClassName("close-filter")[0];
var closefilter2 = document.getElementsByClassName("close-filter2")[0];
var closefilter2a = document.getElementsByClassName("close-filter2")[1];
var closesocial = document.getElementsByClassName("close-social")[0];
var closereview = document.getElementsByClassName("close-review")[0];

if(openregister){
openregister.onclick = function() {
  registermodal.style.display = "block";
}
}
if(openregister1){
openregister1.onclick = function() {
  registermodal.style.display = "block";
}
}
if(openregister2){
openregister2.onclick = function() {
  registermodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  registermodal.style.display = "none";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";

}
}
if(openlogin4){
openlogin4.onclick = function() {
  loginmodal.style.display = "block";

}
}
if(openlogin5){
openlogin5.onclick = function() {
  loginmodal.style.display = "block";

}
}
if(openforgot){
openforgot.onclick = function() {
	loginmodal.style.display = "none";
  forgotmodal.style.display = "block";
}
}
if(openwage){
openwage.onclick = function() {
  wagemodal.style.display = "block";
}
}
if(openwage1){
openwage1.onclick = function() {
  wagemodal.style.display = "block";
}
}
if(openwage2){
openwage2.onclick = function() {
  wagemodal.style.display = "block";
}
}
if(opentopup){
opentopup.onclick = function() {
  topupmodal.style.display = "block";
}
}
if(opentopup1){
opentopup1.onclick = function() {
  topupmodal.style.display = "block";
}
}
if(openupgrade){
  openupgrade.onclick = function() {
  upgrademodal.style.display = "block";
}
}
if(openupgrade1){
  openupgrade1.onclick = function() {
  upgrademodal1.style.display = "block";
}
}
if(openfilter){
openfilter.onclick = function() {

	filtermodal.style.display = "block";
}
}
if(openfilter2){
openfilter2.onclick = function() {

	filtermodal2.style.display = "block";
}
}
if(opensocial){
opensocial.onclick = function() {
  socialmodal.style.display = "none";
	socialmodal.style.display = "block";
}
}
if(openreview){
openreview.onclick = function() {
  reviewmodal.style.display = "block";
}
}
if(openreview1){
openreview1.onclick = function() {
  reviewmodal.style.display = "block";
  
}
}
if(openreview2){
openreview2.onclick = function() {
  reviewmodal.style.display = "block";
}
}


if(closeregister){
closeregister.onclick = function() {
 registermodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
 loginmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
 forgotmodal.style.display = "none";
}
}
if(closewage){
closewage.onclick = function() {
 wagemodal.style.display = "none";
}
}
if(closetopup){
closetopup.onclick = function() {
 topupmodal.style.display = "none";
}
}
if(closeupgrade){
  closeupgrade.onclick = function() {
    upgrademodal.style.display = "none";
}
}
if(closefilter){
closefilter.onclick = function() {
  filtermodal.style.display = "none";
}
}
if(closefilter2){
closefilter2.onclick = function() {
  filtermodal2.style.display = "none";
}
}
if(closefilter2a){
closefilter2a.onclick = function() {
  filtermodal2.style.display = "none";
}
}
if(closesocial){
closesocial.onclick = function() {
  socialmodal.style.display = "none";
}
}
if(closereview){
closereview.onclick = function() {
  reviewmodal.style.display = "none";
}
}


window.onclick = function(event) {
  if (event.target == registermodal) {
    registermodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  } 
  if (event.target == wagemodal) {
    wagemodal.style.display = "none";
  } 
  if (event.target == topupmodal) {
    upgrademodal.style.display = "none";
  } 
  if (event.target == filtermodal) {
    filtermodal.style.display = "none";
  }
  if (event.target == filtermodal2) {
    filtermodal2.style.display = "none";
  }  
  if (event.target == socialmodal) {
    socialmodal.style.display = "none";
  } 
  if (event.target == reviewmodal) {
    reviewmodal.style.display = "none";
  }       
}
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}
</script>
<!--
<script>
// Set the date we're counting down to
var countDownDate = new Date("Jun 9, 2020 23:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("timerP").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timerP").innerHTML = "ENDED";
  }
}, 1000);
</script>
-->
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group4").colorbox({rel:'group4', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>  
<script>
function showVariation() {
  var x = document.getElementById("variationDiv");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function myShow() {
  var x = document.getElementById("show-hide-p-div");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>
<script type="text/javascript" src="js/lightslider.js"></script>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:false,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:false,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });
		});
    </script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>