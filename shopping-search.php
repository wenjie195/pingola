<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $productName = rewrite($_POST['product_name']);
  $productDetails = getProduct($conn," WHERE name = ? ",array("name"),array($productName),"s");
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/shopping-search.php" />
<meta property="og:title" content="Search Result | Pingola" />
<title>Search Result | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/shopping-search.php" />
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="black-bg min-height-foot">
<div class="width100 same-padding overflow ping-menu-distance">
  <h1 class="green-text back-title left-align-title opacity-hover" onclick="close_window();return false;">
    <img src="img/back2.png" class="back-png2">Search Result
  </h1>
</div>

<div class="clear"></div>

<div class="width100 same-padding">
  <form action="shopping-search.php" method="post"  target="_blank">
    <input class="line-input filter-search-ow clean ow-left-search" type="text" name="product_name" id="product_name" placeholder="Search"/>
    <button class="search-btn hover1 clean ow-margin-top0 ow-right-search" type="submit">
      <img src="img/search.png" class="visible-img opacity-hover" alt="Search" title="Search">
      
    </button>
  </form>
</div>

<div class="width103 same-padding margin-top30" id="app">   
  <?php
  $conn = connDB();
  if($productDetails)
  {
    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
    {
    ?>
      <a href="shopping-details.php?id=<?php echo $productDetails[$cnt]->getUid();?>">
        <div class="shadow-white-box four-box-size opacity-hover pointer pointer-div">
          <div class="square">
            <div class="width100 white-bg content progressive">
                <img src="img/pet-load300.jpg" data-src="productImage/<?php echo $productDetails[$cnt]->getImageOne();?>" alt="<?php echo $productDetails[$cnt]->getName();?>" title="<?php echo $productDetails[$cnt]->getName();?>" class="preview width100 two-border-radius opacity-hover pointer lazy">
            </div>
          </div>
          <p align="center" class="width100 text-overflow slider-product-name">
            <?php echo $productDetails[$cnt]->getName();?></p>
          <p align="center" class="width100 text-overflow slider-product-name">
            RM<?php echo $productDetails[$cnt]->getMinPrice();?> - RM<?php echo $productDetails[$cnt]->getMaxPrice();?>
          </p>
          <p align="center" class="width100 text-overflow slider-product-name">
            <?php $ratingValue = $productDetails[$cnt]->getRating();?>
            <?php 
              if($ratingValue== 1 )
				      {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  <img src="img/yellow-star.png" class="star-img2">
                </p>
              <?php
              }
              elseif($ratingValue == 2 )
              {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                </p>
              <?php
              }
              elseif($ratingValue == 3 )
              {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                </p>
              <?php
              }
              elseif($ratingValue == 4 )
              {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                </p>
              <?php
              }
              elseif($ratingValue == 5 )
              {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                  <img src="img/yellow-star.png" class="star-img2">
                </p>
              <?php
              }
              else
              {
              ?>
                <p class="slider-product-name star2-p text-overflow">
                  No Rating Yet
                </p>
              <?php
              }
            ?>
          </p>
        </div>
      </a>
    <?php
    }
    ?>
  <?php
  }
  else
  {
    echo "Product no found. Please try again with another name.";
  }
  ?>
</div>

<div class="clear"></div>
</div>
<?php include 'js.php'; ?>

<script type="text/javascript">
var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++)
{
$("#"+i+"").click(function()
{
  var x = $(this).attr('value');
  location.href = "./shopping-details.php?id="+x+"";
});
}
</script>

<script src="js/index2.js"></script>

<script>
  function close_window()
  {
    close();
  } 
</script>

<script>
(function(){
	new Progressive({
	el: '#app',
	lazyClass: 'lazy',
	removePreview: true,
	scale: true
	}).fire()
})()
</script>
</body>
</html>