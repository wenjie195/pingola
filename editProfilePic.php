<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/editProfile.php" />
<meta property="og:title" content="Edit Profile | Pingola" />
<title>Edit Profile | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/editProfile.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <h1 class="line-header margin-bottom50">Edit Profile Picture</h1>

        <form method="POST" action="utilities/editProfilePictureFunction.php" enctype="multipart/form-data">

            <div class="dual-input">
                <p class="input-top-p">Profile Picture</p>
                <input type="file" name="image_one" id="image_one" accept="image/*" required>
            </div>
              
            <div class="clear"></div>
            
            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

            <div class="clear"></div>

        </form>

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>