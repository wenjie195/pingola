<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// $newProductUid = $_SESSION['product_uid'];

$conn = connDB();

$newProductUid = $_SESSION['product_uid'];
// $categoryDetails = getCategory($conn);
// $brandDetails = getBrand($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/addProductVariation.php" />
<meta property="og:title" content="Add Product Variation | Pingola" />
<title>Add Product Variation | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/addProductVariation.php" />
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
<h1 class="line-header margin-bottom50">Add Product Variation</h1>


  <div class="clear"></div>

  <div class="border-separation">

    <form action="utilities/adminRegisterVariationFunction.php" method="POST" enctype="multipart/form-data">

      <div class="width100">
        <p class="input-top-p admin-top-p">Variation 1 Name*</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Name" name="register_name" id="register_name" required>
      </div>

      <div class="clear"></div>

      <div class="width100">
        <p class="input-top-p admin-top-p">Product Price (RM)*</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Price" name="register_price" id="register_price" required>
      </div>

      <div class="clear"></div>  

      <div class="width100">
        <p class="input-top-p admin-top-p">Upload Variation Product Image*</p>
        <!-- <input type="file" name="fileToUpload" > -->
        <input type="file" name="image_one" id="image_one" accept="image/*" required/>
      </div>

      <div class="clear"></div>  

      <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $newProductUid ;?>" name="product_uid" id="product_uid" readonly>

      <!-- <div class="dual-input  second-dual-input">
        <p class="input-top-p admin-top-p">Product Quantity</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Quantity" name="register_quantity" id="register_quantity" required>
      </div> -->

      <!-- <input class="input-name clean input-textarea admin-input" type="hidden" value="1000" name="register_quantity" id="register_quantity" readonly> -->

      <div class="width100 overflow text-center margin-top30">
        <button class="green-button white-text clean2 edit-1-btn margin-auto" id="submit" name="submit">Save</button>
      </div>

    </form>

  </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>