<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

//$userRows = getSeller($conn,"WHERE id = ?",array("id"),array($id),"s");
// $userDetails = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"s");
// $userDetails = getUser($conn," WHERE user_type = ? OR user_type = ? ",array("user_type","user_type"),array(1,5),"ss");

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $orderRequest = getOrders($conn);
$orderRequest = getOrders($conn, "WHERE uid =? ORDER BY date_created DESC ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/orderHistory.php" />
<meta property="og:title" content="Order History | Pingola" />
<title>Order History | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/orderHistory.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<div class="width100">
    	<h1 class="line-header margin-bottom50">Order History</h1>

    </div>

    <div class="clear"></div>

	<div class="width100 margin-top30 same-padding-tdh">
    	<table class="table-css">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Order<br>Date</th>
                    <th>Order<br>ID</th>
                    <th>Total (RM)</th>
                    <th>Tracking No.</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($orderRequest)
                    {
                        
                        for($cnt = 0;$cnt < count($orderRequest) ;$cnt++)
                        {?>
                            
                            <tr class="link-to-details">
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td>1/11/2020</td> -->
                                <td><?php echo $orderRequest[$cnt]->getDateCreated();?></td>
                                <td>#<?php echo $orderRequest[$cnt]->getId();?></td>
                                <td><?php echo $orderRequest[$cnt]->getSubtotal();?></td>
                                <td><?php echo $orderRequest[$cnt]->getTrackingNumber();?></td>
                                <td>
                                    <?php 
                                        $paymentStatus = $orderRequest[$cnt]->getPaymentStatus();
                                        if($paymentStatus == 'ACCEPTED')
                                        {
                                            echo  "Shipped";
                                        }
                                        elseif($paymentStatus == 'PENDING')
                                        {
                                            echo  "Order Received";
                                        }
                                        elseif($paymentStatus == 'WAITING')
                                        {
                                            echo  "Transaction Failed";
                                        }
                                        elseif($paymentStatus == 'REJECTED')
                                        {
                                            echo  "Order Rejected";
                                        }
                                        else
                                        {
                                            // echo  "Transaction Failed";
                                        }
                                    ?>
                                </td>

                                <td>
                                    <form method="POST" action="orderDetails.php" class="hover1">
                                        <button class="clean transparent-btn img-btn light-green-a2" type="submit" name="order_id" value="<?php echo $orderRequest[$cnt]->getOrderId();?>">View
                                            
                                        </button>
                                    </form>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete user !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Account Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update user account !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</body>
</html>