<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$newProductUid = $_SESSION['product_uid'];

// $newPetsType = $_SESSION['newpets_type'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $uid = md5(uniqid());
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/addMultipleImageProduct.php" />
<meta property="og:title" content="Add Product Image | Pingola" />
<title>Add Product Image | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/addMultipleImageProduct.php" />
<link rel="stylesheet" href="css/dropzone.min.css">
<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'header.php'; ?>

<!-- <div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
    <h1 class="green-text h1-title">Upload Product Photo Maximum 6*</h1>
    <div class="green-border"></div>
  </div>
  <div class="border-separation">
    <form action="utilities/addMultiImageProductFunction.php" >
	  <div class="dropzone" id="my-awesome-dropzone">
      <div id="displayTable"></div>
    </div>
		<div class="clear"></div>
    <div class="width100 overflow text-center">
      <button class="green-button white-text clean2 edit-1-btn margin-auto" id="submit" value="submitBtn">Submit</button>
    </div>    
    </form>
  </div>
</div> -->

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<div class="width100">
    <h1 class="green-text h1-title">Upload Product Description Photo Maximum 6 (Can Skip)</h1>
    
  </div>
  <div class="border-separation">
    <form action="utilities/addMultiImageProductFunction.php" >
	<div class="dropzone" id="my-awesome-dropzone">
    
    <div id="displayTable"></div>
    </div>
		<div class="clear"></div>
      <div class="width100 overflow text-center margin-top30">
      	<button class="green-button white-text clean2 edit-1-btn margin-auto" id="submit" value="submitBtn">Next</button>
      </div>    
    
    </form>
  </div>
</div>

<?php include 'js.php'; ?>
<script src="js/dropzone.min.js"></script>
<script>

$(function(){
    var myDropzone = new Dropzone(".dropzone", {
    url: "utilities/addMultiImageProductFunction.php",
    paramName: "file",
    maxFilesize: 5,
	  maxFiles: 6,
    addRemoveLinks: true,
    dictResponseError: 'Server not Configured',
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    // autoProcessQueue: false,
    init:function(){
      var self = this;

      // config
      self.options.addRemoveLinks = true;
      self.options.dictRemoveFile = "Delete";
      
      //New file added
      self.on("addedfile", function (file) {
        console.log('new file added ', file);
      });

      // Send file starts
      self.on("sending", function (file) {
        console.log('upload started', file);
        $('.meter').show();
      });
      
      // File upload Progress
      self.on("totaluploadprogress", function (progress) {
        console.log("progress ", progress);
        $('.roller').width(progress + '%');
      });

      self.on("queuecomplete", function (progress) {
        $('.meter').delay(999).slideUp(999);
      });
      
      // On removing file
      self.on("removedfile", function (file) {
        console.log(file);
      });

	    self.on("maxfilesexceeded", function(file) { 
        this.removeFile(file); 
      });
    }
  });
          });
</script>
</body>
</html>
