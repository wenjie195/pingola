<?php
if(isset($_SESSION['uid']))
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="Pingola" title="Pingola"></a>
            </div>
			<div class="right-menu-div float-right before-header-div login-header">
            <div class="dropdown">
                            <a  class="white-text menu-margin-right menu-item opacity-hover">
                                        Admin <img src="img/dropdown.png" class="dropdown-png">

                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="dashboard.php"  class="menu-padding dropdown-a">Dashboard</a></p>
                                        <p class="dropdown-p"><a href="topUpRequest.php"  class="menu-padding dropdown-a">Top Up Request</a></p>
                                        <p class="dropdown-p"><a href="withdrawRequest.php"  class="menu-padding dropdown-a">Withdraw Request</a></p>
                                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a">Edit Password</a></p>

                            </div>
            </div> 
            <div class="dropdown">
                            <a  class="white-text menu-margin-right menu-item opacity-hover">
                                        Details <img src="img/dropdown.png" class="dropdown-png">

                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="users.php"  class="menu-padding dropdown-a">Users</a></p>
                                        <p class="dropdown-p"><a href="leaderboard.php"  class="menu-padding dropdown-a">Leaderboard</a></p>
                                        
                            </div>
            </div>            
            	<a href="onlineShop.php" class="white-text menu-item opacity-hover menu-margin-right">Shop</a>
                <a href="logout.php" class="white-text menu-item opacity-hover">Logout</a>
                
                           	<div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="dashboard.php">Dashboard</a></li>
                                  <li><a href="topUpRequest.php">Top Up Request</a></li>
                                  <li><a href="withdrawRequest.php">Withdraw Request</a></li>                                
                                  <li><a href="editPassword.php">Edit Password</a></li>
                                  <li><a href="users.php">Users</a></li>                                
                                  <li><a href="leaderboard.php">Leaderboard</a></li>                                  
                                  <li><a href="onlineShop.php">Shop</a></li>
								  <li><a href="logout.php">Logout</a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->                   
                                        
            </div>
        </div>
</header>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="Pingola" title="Pingola"></a>
            </div>
			<div class="right-menu-div float-right before-header-div">
                <a class="white-text menu-margin-right menu-item opacity-hover open-register">
                   Register
                </a>     
                 <a class="white-text menu-item opacity-hover open-login">
                   Login
                </a>                        
            </div>
        </div>
</header>

<?php
}
?>