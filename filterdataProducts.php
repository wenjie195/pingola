<div class="width103" id="app">
<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

if(isset($_POST["action"]))
{
	// $query = " 
	// SELECT * FROM variation WHERE status = 'Available' 
	// ";

	$query = " SELECT * FROM product WHERE status = 'Available' ";

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row_count = $statement->rowCount();

	if(isset($_POST["minimum_price"], $_POST["maximum_price"]) && !empty($_POST["minimum_price"]) && !empty($_POST["maximum_price"]))
	{
		// $query .= "
		//  AND amount BETWEEN '".$_POST["minimum_price"]."' AND '".$_POST["maximum_price"]."'
		// ";
		$query .= "
		 AND midPrice BETWEEN '".$_POST["minimum_price"]."' AND '".$_POST["maximum_price"]."'
		";
	}

	if(isset($_POST["category"]))
	{
		$category_filter = implode("','", $_POST["category"]);
		$query .= "
		 AND category IN('".$category_filter."')
		";
	}

	if(isset($_POST["brand"]))
	{
		$brand_filter = implode("','", $_POST["brand"]);
		$query .= "
		 AND brand IN('".$brand_filter."')
		";
	}

	if(isset($_POST["rating"]))
	{
		$rating_filter = implode("','", $_POST["rating"]);
		$query .= "
		 AND rating IN('".$rating_filter."')
		";
	}

	{
		$query .= "
		 ORDER BY date_created DESC 
		";
	}

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row = $statement->rowCount();
	$output = '';

	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$uid= $row['uid'];

			$imageURL = './productImage/'.$row["image_one"];
			// elseif ($row['status'] == 'Available')
			if ($row['status'] == 'Available')
			{
				if($row['rating'] == 1 )
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								<img src="img/yellow-star.png" class="star-img2">
							</p>
						</div>
					</a>
				';
				}
				elseif($row['rating'] == 2)
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
							</p>
						</div>
					</a>
				';
				}
				elseif($row['rating'] == 3)
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
							</p>
						</div>
					</a>
				';
				}
				elseif($row['rating'] == 4)
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
							</p>
						</div>
					</a>
				';
				}
				elseif($row['rating'] == 5)
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
								<img src="img/yellow-star.png" class="star-img2">
							</p>
						</div>
					</a>
				';
				}
				else
				{
					$output .= '	
					<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
						 <div class="square">
							<div class="width100 white-bg content progressive">
							
									<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
							</div>    
						  </div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
							<p class="slider-product-name text-overflow price-p2z">
								RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
							</p>
							<p class="slider-product-name star2-p text-overflow">
								No Rating Yet
							</p>
						</div>
					</a>
				';
				}
				// $output .= '	
				// 	<a href="shopping-details.php?id='.$row['uid'].'" class="opacity-hover pointer">
				// 		<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['uid'].'">
				// 		 <div class="square">
				// 			<div class="width100 white-bg content progressive">
							
				// 					<img data-src="productImage/'. $row['image_one'] .'"  src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="'. $row['name'] .' | " title="'. $row['name'] .' | " />
							                 
				// 			</div>    
				// 		  </div>
				// 			<p align="center" class="width100 text-overflow slider-product-name">'. $row['name'] .' </p>
				// 			<p class="slider-product-name text-overflow price-p2z">
				// 				RM'. $row['minPrice'] .' - RM'. $row['maxPrice'] .'
				// 			</p>
				// 			<p class="slider-product-name star2-p">
				// 				<img src="img/yellow-star.png" class="star-img2">
				// 				<img src="img/yellow-star.png" class="star-img2">
				// 				<img src="img/yellow-star.png" class="star-img2">
				// 				<img src="img/yellow-star.png" class="star-img2">
				// 				<img src="img/yellow-star.png" class="star-img2">
				// 			</p>
				// 		</div>
				// 	</a>
				// ';
			}
			
		}
	}
	else
	{
		$output = '<h3>No Data Found</h3>';
	}
	echo $output;
}

?>
</div>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./shopping-details.php?id="+x+"";
				});

}
</script>
<script src="js/index2.js"></script>

<script>
(function(){
	new Progressive({
	el: '#app',
	lazyClass: 'lazy',
	removePreview: true,
	scale: true
	}).fire()
})()
</script>