<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$timestamp = time();
// $timestamp = date_default_timezone_set("Asia/Kuala_Lumpur");

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/transactionHistory.php" />
<meta property="og:title" content="Transaction History | Pingola" />
<title>Transaction History | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/transactionHistory.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
	<div class="width100 overflow text-center">

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <div class="left-profile-div margin-auto vip">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
            else
            {
            ?>
                <div class="left-profile-div margin-auto">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>
		<p class="username-p">My PingCash: <?php echo $userDetails->getCredit();?></p>
       
        <div class="blue-button goback-btn  blue-btn-hover open-topup">Top Up</div>
    </div>

 

     <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">Transaction History</h1>
        <div class="overflow-scroll-div">
        <table class="table-css">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Date</th>
                    <th>Action</th>
                    <th>PingCash</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td>1.</td>
                    <td>5/5/2020</td>
                    <td>Top Up</td>
                    <td class="win-color">+ 200</td> 
                </tr>
            	<tr>
                	<td>2.</td>
                    <td>5/5/2020</td>
                    <td>Withdraw</td>
                    <td class="lose-color">- 100</th>
                </tr>                
            	<tr>
                	<td>3.</td>
                    <td>5/5/2020</td>
                    <td>Win Game</td>
                    <td class="win-color">+ 100</th>
                </tr>                 
            </tbody>
        </table>
        </div>
     </div>
</div>

<div class="clear"></div>

    <!-- Top Up Modal -->
    <div id="topup-modal" class="modal-css">
        <div class="modal-content-css register-modal-content">
            <span class="close-css close-topup">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Top Up</h2>
            <form method="POST" action="utilities/topUpFunction.php">
                <div class="dual-input">
                    <p class="input-top-p big-top-p long-p">How Many PingCash You Would Like to Top Up?</p>
                    <!-- <input class="input-name clean" type="number" placeholder="0 PingCash" id="cash_value" name="cash_value" required> -->
                    <input class="input-name clean" type="number" placeholder="0 PingCash" id="cash_value" name="cash_value">
                </div>

                    <?php
                    $vip = $userDetails->getVipStatus();
                    if($vip == 'Yes')
                    {
                    ?>

                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="dual-input second-dual-input">
                        <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash?</p>
                            <!-- <select  class="input-name clean" type="text" name="update_vip" id="update_vip" requried> -->
                            <select class="clean input-name admin-input" type="text" name="update_vip" id="update_vip" required>
                                <option value="No" name="No" selected>No</option>
                                <option value="Yes" name="Yes">Yes</option>

                            </select>
                        </div>
                    <?php
                    }
                    ?>

                <button class="blue-button white-text width100 clean register-button"  name="submit">Confirm</button>
            </form>
        </div>
    </div>

</div>

<?php include 'js.php'; ?>


</body>
</html>