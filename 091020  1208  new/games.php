<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
//$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$gamesDetails = getGames($conn," Order By date_created ASC ");

// $gamesDetailsAA = getGames($conn," WHERE matchday < '".$longtime."' Order By date_created ASC ");

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/upcoming?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/games.php" />
<meta property="og:title" content="Games | Pingola" />
<title>Games | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/games.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height">
	<div class="width100 overflow text-center">

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto vip">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>

                </a>
            <?php
            }
            else
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>
                </a>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php 
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>
		<p class="username-p">My PingCash: <?php echo $userDetails->getCredit();?></p>
        
    </div>
	<div class="clear"></div>
 
    <div class="width100 overflow margin-top30">
        <h1 class="line-header margin-bottom50">Games Available</h1>
	</div>

    <?php
      if ($exchangeData)
      {
          for ($i=0; $i <count($exchangeData) ; $i++)
          {
              // if (strpos($exchangeData[$i]['name'],"TBD") == false && date('Y-m-d H:i',strtotime($exchangeData[$i]['begin_at'])) > date('Y-m-d H:i'))
              if (strpos($exchangeData[$i]['name'],"TBD") == false && date('Y-m-d',strtotime($exchangeData[$i]['begin_at'])) > date('Y-m-d'))
              {
                  $No++;
                  ?>
                  <form class="" action="placeWage.php" method="post">
                      <input type="hidden" name="game_id" value="<?php echo $exchangeData[$i]['id'] ?>">
                      <input type="hidden" name="title" value="<?php echo $exchangeData[$i]['league']['name'] ?>">
                      <input type="hidden" name="matches" value="<?php echo $exchangeData[$i]['slug'] ?>">
                      <div class="bluered-gradient-bg result-color-div width100 ow-bluered">
                          <div class="div1a">
                              <p class="logo-p1">
                                  <?php
                                  if ($exchangeData[$i]['opponents'][0]['opponent']['image_url'])
                                  {
                                  ?>
                                      <img class="team-logo" src="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['image_url'] ?>" alt="">
                                  <?php
                                  }
                                  else
                                  {
                                      echo $exchangeData[$i]['opponents'][0]['opponent']['name'];
                                  }
                                  ?>
                              </p>
                              <p class="bet-team-name"><?php echo $exchangeData[$i]['opponents'][0]['opponent']['name'] ?></p>
                              <button class="clean blue-button bet-button" value="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team0".$i ?>">
                                  Place Wage
                              </button>
                          </div>
                          <div class="div4a">
                              <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
                          </div>
                          <div class="div5a">
                              <?php
                              if ($exchangeData[$i]['league']['image_url'])
                              {
                              ?>
                                  <img  class="bet-game-logo team-logo" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""><?php
                              }
                              else
                              {
                              ?>
                                  <img  class="bet-game-logo team-logo" src="img/dota_logo.png" ><?php
                              }
                              ?>
                              <p class="bet-team-name">
                                  <b><?php echo $exchangeData[$i]['league']['name'] ?> (<?php echo str_replace(":",":<br>",$exchangeData[$i]['name']) ?>)</b><br>
                                  <?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?>
                              </p>
                          </div>
                          <div class="div6a">
                              <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
                          </div>
                          <div class="div9a">
                              <p class="logo-p1">
                                  <?php
                                  if ($exchangeData[$i]['opponents'][1]['opponent']['image_url'])
                                  {
                                  ?>
                                      <img class="team-logo" src="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['image_url'] ?>" alt=""><?php
                                  }
                                  else
                                  {
                                      echo $exchangeData[$i]['opponents'][1]['opponent']['name'];
                                  }
                                  ?>
                              </p>
                              <p class="bet-team-name"><?php echo $exchangeData[$i]['opponents'][1]['opponent']['name'] ?></p>
                              <button class="clean transparent-button blue-button bet-button" value="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team1".$i ?>" title="Place Your Wage">
                                  Place Wage
                              </button>
                          </div>
                      </div>
                  </form>
              <?php
              }
          }
      }
      ?>

</div>


<?php include 'js.php'; ?>

</body>
</html>