<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$uid = $_SESSION['uid'];

$conn = connDB();

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/upcoming?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);

$conn->close();

?>

        <table id="fix" class="table-css">
            <thead>
                <tr>
                    <th class="th" style="text-align: center;">NO.</th>
                    <!-- <th class="th" style="text-align: center;">MATCH TYPE</th> -->
                    <th class="th" style="text-align: center;">TOURNAMENT LOGO</th>
                    <th class="th" style="text-align: center;">TOURNAMENT NAME</th>
                    <!-- <th class="th" style="text-align: center;">STATUS</th> -->
                    <th class="th" style="text-align: center;">MATCH</th>
                    <th class="th" style="text-align: center;">OPPONENT</th>
                    <th class="th" style="text-align: center;">SCHEDULED DATE</th>
                </tr>
            </thead>

            <tbody>
            <?php
            if ($exchangeData)
            {
                for ($i=0; $i <count($exchangeData) ; $i++)
                {
                    if (strpos($exchangeData[$i]['name'],"TBD") == false)
                    {
                    $No++;
                    ?>
                        <tr>
                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $No ?></td>

                        <!-- <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['match_type'] == 'best_of') {
                        echo "Best of ".$exchangeData[$i]['number_of_games'];
                        } ?></td> -->

                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                        <?php
                        if ($exchangeData[$i]['league']['image_url'])
                        {
                        ?>
                        <img style="width: 120px;height: 100px" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""><?php
                        }
                        else
                        {
                        ?>
                            <img style="width: 120px;height: 100px" src="img/dota_logo.png" alt=""><?php
                        }
                        ?>
                        </td>
                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $exchangeData[$i]['league']['name'] ?></td>

                        <!-- <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['status'] == 'not_started') {
                        echo "Not Started Yet";
                        }elseif ($exchangeData[$i]['status'] == 'running') {
                        echo "Running";
                        } ?></td> -->

                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo str_replace(":",":<br>",$exchangeData[$i]['name']) ?></td>
                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                            <form class="" action="placeWage.php" method="post">
                                <input type="hidden" name="title" value="<?php echo $exchangeData[$i]['league']['name'] ?>">

                                <button class="clean transparent-button" value="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team0".$i ?>">
                                    <?php 
                                    if ($exchangeData[$i]['opponents'][0]['opponent']['image_url'])
                                    {
                                    ?>
                                    <img style="width: 30px;height: 30px;" src="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['image_url'] ?>" alt="">
                                    <?php
                                    }
                                    else
                                    {
                                        echo $exchangeData[$i]['opponents'][0]['opponent']['name'];
                                    }
                                    ?>
                                </button>
                                
                                <div style="padding: 5px"></div>

                                <button class="clean transparent-button" value="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team1".$i ?>">
                                <?php
                                if ($exchangeData[$i]['opponents'][1]['opponent']['image_url'])
                                {
                                ?>
                                    <img style="width: 30px;height: 30px;" src="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['image_url'] ?>" alt=""><?php
                                }
                                else
                                {
                                    echo $exchangeData[$i]['opponents'][1]['opponent']['name'];
                                } ?>
                            </button>
                            </form>
                        </td>
                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?></td>
                        </tr>
                    <?php
                    }
                }
            }
            ?>
            </tbody>

        </table>