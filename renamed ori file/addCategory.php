<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/addCategory.php" />
<meta property="og:title" content="Add Category | Pingola" />
<title>Add Category | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/addCategory.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
   <h1 class="line-header margin-bottom50">Add Category</h1>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/registerCategoryFunction.php" method="POST"> 
        <div class="width100">
        	<p class="input-top-p admin-top-p">Category Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Category Name" name="register_name" id="register_name">      
        </div>

        <input class="input-name clean input-textarea admin-input" type="hidden" value="Available" name="register_status" id="register_status">   

        <!-- <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
        	<select class="input-name clean admin-input" name="register_status" id="register_status">
            	<option>Available</option>
                <option>Not Available</option>
            </select>     
        </div>     -->

        <div class="clear"></div>
      
        <div class="clear"></div>  
        <div class="width100 overflow text-center margin-top30">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Category Added!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add New Category!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "This category already existed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>