<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$timestamp = time();
// $timestamp = date_default_timezone_set("Asia/Kuala_Lumpur");

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/onlineShop.php" />
<meta property="og:title" content="PingCash Shop | Pingola" />
<title>PingCash Shop | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/onlineShop.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
     <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">PingCash Shop</h1>
 		<div class="width103">
        	<div class="repeat-four-width-div">
            	<img src="img/p16.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Razer Hammerhead True Wireless Earbuds</p>
                    <p class="price-p">350.00</p>
                </div>
            </div>

        	<div class="repeat-four-width-div">
            	<img src="img/p14.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Razer Junglecat</p>
                    <p class="price-p">500.00</p>
                </div>             
            </div>     
        	<div class="repeat-four-width-div">
            	<img src="img/p13.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Razer Sphex V2 Gaming Mousepad</p>
                    <p class="price-p">50.00</p>
                </div>            
            </div>                   
        	<div class="repeat-four-width-div">
            	<img src="img/p15.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Razer Blade Stealth 13</p>
                    <p class="price-p">3500.00</p>
                </div>               
            </div>          
        
        
        
        	<div class="repeat-four-width-div">
            	<img src="img/p9.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">PS4</p>
                    <p class="price-p">2000.00</p>
                </div>
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p2.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">A50 Wireless Headset + Base Station</p>
                    <p class="price-p">500.00</p>
                </div>            
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p8.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Nintendo Switch with Neon Blue and Neon Red Joy Con</p>
                    <p class="price-p">500.00</p>
                </div>             
            </div>            
        	<div class="repeat-four-width-div">
            	<img src="img/p10.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">PlayStation DualShock 4 Wireless Controller for PlayStation 4 - Rose Gold</p>
                    <p class="price-p">120.00</p>
                </div>               
            </div>  
        	<div class="repeat-four-width-div">
            	<img src="img/p11.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Womier K66 66Key Tyce-C Wired RGB Backlit Gateron Switch Mechanical Gaming Keyboard with Crystalline Base for PC Laptop - Blue Switch</p>
                    <p class="price-p">250.00</p>
                </div>
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p7.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">WARWOLF Rechargeable T1 Wireless Silent LED Backlit USB Optical Ergonomic Gaming Mouse 18May30 Drop Ship F</p>
                    <p class="price-p">30.00</p>
                </div>            
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p12.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Street Fighter V: Champion Edition (PS4)</p>
                    <p class="price-p">120.00</p>
                </div>             
            </div>            
        	<div class="repeat-four-width-div">
            	<img src="img/p6.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Jumanji: The Video Game (PS4)</p>
                    <p class="price-p">120.00</p>
                </div>               
            </div>             
        	<div class="repeat-four-width-div">
            	<img src="img/p3.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Bloodborne (PS4)</p>
                    <p class="price-p">350.00</p>
                </div>
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p1.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">SONY PS4 Game Playerunknown's Battleground PUBG</p>
                    <p class="price-p">30.00</p>
                </div>            
            </div>
        	<div class="repeat-four-width-div">
            	<img src="img/p4.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">The Sims 4 - PlayStation 4</p>
                    <p class="price-p">120.00</p>
                </div>             
            </div>            
        	<div class="repeat-four-width-div">
            	<img src="img/p5.jpg" class="width100" >
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                	<p class="product-name-p text-overflow">Pokémon: Let's Go, Eevee!</p>
                    <p class="price-p">150.00</p>
                </div>               
            </div>             
            
            
            
            
                              
        </div>
    </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>