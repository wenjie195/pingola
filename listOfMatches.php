<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/listOfMatches.php" />
<meta property="og:title" content="List Of Matches | Pingola" />
<title>List Of Matches | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/listOfMatches.php" />
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
  <h1 class="line-header margin-bottom50">List Of Matches</h1>

<?php

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
 ?>
 <div class="overflow-scroll-div">
     <?php //$conn = connDB();?>
         <table id="singleAdvancedTable" class="table-css">
             <thead>
                 <tr>
                     <th class="th" style="text-align: center;">NO.</th>
                     <th class="th" style="text-align: center;">MATCH TYPE</th>
                     <th class="th" style="text-align: center;">TOURNAMENT LOGO</th>
                     <th class="th" style="text-align: center;">TOURNAMENT NAME</th>
                     <th class="th" style="text-align: center;">STATUS</th>
                     <th class="th" style="text-align: center;">MATCH</th>
                     <th class="th" style="text-align: center;">SCHEDULED DATE</th>
                     <!-- <th>INVOICE</th> -->
                 </tr>
             </thead>
             <tbody>
                 <?php
                 // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                 // {

                 if ($exchangeData) {
                   for ($i=0; $i <count($exchangeData) ; $i++) {
                     // if (strpos($exchangeData[$i]['name'],"TBD") == false) {
                     $No++;
                     ?>
                     <tr>
                      <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $No ?></td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['match_type'] == 'best_of') {
                       echo "Best of ".$exchangeData[$i]['number_of_games'];
                     } ?></td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                       <?php if ($exchangeData[$i]['league']['image_url']) {
                         ?><img style="width: 120px;height: 100px" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""><?php
                       }else {
                         echo "No Image";
                       } ?>
                       </td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $exchangeData[$i]['league']['name'] ?></td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['status'] == 'not_started') {
                       echo "Not Started Yet";
                     }elseif ($exchangeData[$i]['status'] == 'running') {
                       echo "Running";
                     } ?></td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo str_replace(":","<br>",$exchangeData[$i]['name']) ?></td>
                     <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?></td>
                   </tr>
                     <?php
                    // }
                   }
                 }

                 //}
                 ?>
             </tbody>
         </table>
</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>
