<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/withdrawFund.php" />
<meta property="og:title" content="Withdraw Fund | Pingola" />
<title>Withdraw Fund | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/withdrawFund.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    <h1 class="line-header margin-bottom50">Withdraw Fund</h1>

        <form method="POST" action="utilities/editProfileFunction.php">

			<div class="dual-input">
                <p class="input-top-p">Current PingCash</p>
                <input class="input-name clean no-input" type="text" placeholder="1000" value="" name=""  id="" >
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Bank</p>
                <input class="input-name clean no-input" type="text" placeholder="Maybank" value="" name="" id="">	
            </div>
            
            <div class="clear"></div>
			<div class="dual-input">
                <p class="input-top-p">Bank Account No.</p>
                <input class="input-name clean no-input" type="text" placeholder="153423423432" value="" name="" id="">	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Bank Account Name</p>
                <input class="input-name clean no-input" type="text" placeholder="Tan Xiao Ming" value="" name="" id="">	
            </div>            
            
            <div class="clear"></div>
			<div class="dual-input">
                <p class="input-top-p">Withdraw Amount</p>
                <input class="input-name clean" type="number" placeholder="0" value="" name="" id="">	
			</div>
            
            <div class="clear"></div>
            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

            <div class="clear"></div>

            <!-- <div class="width100 text-center margin-top10">
                <a class="open-login bottom-a">Login</a>
            </div> -->

        </form>

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>