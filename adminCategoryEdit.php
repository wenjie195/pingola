<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$categoryDetails = getCategory($conn," WHERE status = ? ",array("status"),array("Available"),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/editCategory.php" />
<meta property="og:title" content="Edit Category | Pingola" />
<title>Edit Category | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/editCategory.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<h1 class="line-header margin-bottom50">Edit Category</h1>
    <div class="border-separation">

    <div class="clear"></div>

        <!-- <form method="POST" action="utilities/editCategoryFunction.php"> -->
        <form method="POST" action="utilities/adminCategoryEditFunction.php">

            <?php
            if(isset($_POST['category_id']))
            {
                $conn = connDB();
                $categoryDetails = getCategory($conn,"WHERE id = ? ", array("id") ,array($_POST['category_id']),"i");
            ?>

                <div class="width100">
                    <p class="input-top-p admin-top-p">Category Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Category Name" value="<?php echo $categoryDetails[0]->getName();?>" name="update_category_name" id="update_category_name" required>       
                </div>

                <div class="clear"></div>

                <input type="hidden" value="<?php echo $categoryDetails[0]->getId();?>" name="category_id" id="category_id" required>   
                <input type="hidden" value="<?php echo $categoryDetails[0]->getStatus();?>" name="category_status" id="category_status">

            <?php
            }
            ?>

            <div class="clear"></div>  

            <div class="width100 overflow text-center margin-top30">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

 		<!-- <form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Category Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Category Name" required name="" value>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Available</option>
                <option>Not Available</option>
            </select>     
        </div>        
        <div class="clear"></div>
      
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form> -->

    </div>
    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>