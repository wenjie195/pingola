<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$visitorDetails = getPageview($conn, "WHERE refer_uid =?",array("refer_uid"),array($uid),"s");
// $visitorDetails = $visitorRows[0];

// $gamesDetails = getGames($conn," Order By date_created ASC ");
$gamesDetails = getGames($conn," WHERE matchday > '".$longtime."' Order By date_created ASC ");

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/upcoming?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/profile.php" />
<meta property="og:title" content="Profile | Pingola" />
<title>Profile | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/profile.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

	<div class="width100 overflow text-center">

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto vip">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>

                </a>
            <?php
            }
            else
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>
                </a>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>
		<p class="username-p">My PingCash: <?php echo $userDetails->getCredit();?></p>
        <a href="editProfile.php"><div class="blue-button goback-btn dual-button">Edit Profile</div></a>
        <!-- <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-topup">Top Up</div> -->
        <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-topup">Recharge</div>
		<div class="clear"></div>

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {}
            else
            {
            ?>
            	<div class="width100 overflow text-center">
                	<div class="blue-button vip-upgrade open-upgrade">Upgrade to <img src="img/vip.png" class="vip-img" alt="Upgrade to VIP" title="Upgrade to VIP"></div>
                </div>
            <?php
            }
        ?>

        <!-- <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-upgrade">VIP Upgrade</div> -->
    </div>

    <div class="clear"></div>

    <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">Invitation Link</h1>
        <div class="link-container">

            <form method="POST" action="utilities/linkClickFunction.php">
                <?php $actual_link = "https://$_SERVER[HTTP_HOST]"; ?>
                <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                        <?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div>

                <!-- <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/testing/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/testing/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                    <?php echo "https://".$_SERVER['HTTP_HOST']."/testing/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div> -->

                <!-- <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/pingola/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/pingola/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                    <?php echo "https://".$_SERVER['HTTP_HOST']."/pingola/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div> -->

                <button class="clean blue-btn-hover copy-btn" id="copy-referral-link">COPY</button>
            </form>

        </div>
    	<div class="clear"></div>
    </div>

    <div class="width100 overflow margin-top30">
    	<div class="three-div-width1 box-css">
        	<img src="img/link-clicked.png" alt="Referral Link Clicked" title="Referral Link Clicked" class="box-img">
			<p class="box-p">Referral Link Clicked</p>
            <p class="box-value">
                <?php
                   if($visitorDetails)
                   {
                       echo $totalVisitor = count($visitorDetails);
                   }
                   else
                   {   echo $totalVisitor = 0;   }
                ?>
            </p>
        </div>
    	<div class="three-div-width1 box-css mid-three-width1">
        	<img src="img/referral.png" alt="Successful Referrals" title="Successful Referrals" class="box-img">
			<p class="box-p">Successful Referrals</p>
            <p class="box-value">
                <?php
                    $referNo = $userDetails->getReferNo();
                    if($referNo == '')
                    {
                        echo "0";
                    }
                    else
                    {
                        echo $referNo;
                    }
                ?>
            </p>
        </div>
    	<div class="three-div-width1 box-css">
        	<img src="img/pingcash.png" alt="PingCash" title="PingCash" class="box-img">
			<p class="box-p">PingCash</p>
            <p class="box-value"><?php echo $userDetails->getCredit();?></p>
        </div>
    </div>

    <div class="clear"></div>

    <div class="width100 overflow margin-top30" id="gametab">
        <h1 class="line-header margin-bottom50">Games Available</h1>
    </div>

    <!-- <h4 class="tab-h2"> Dota 2 | <a href="profile_csgo.php" class="red-link">CS GO</a> | <a href="profile_lol.php" class="red-link">LOL</a> | <a href="profile_ow.php" class="red-link">Overwatch</a> </h4> -->

	<div class="darkblue-bg games-div ow-game-logo">
    	<div class="game-logo-div">
            <!-- <a href="profile.php" class="red-link"> -->
                <img id="dota2Tab" src="img/dota2.png" class="width100  opacity-hover" alt="Dota 2" title="Dota 2">
            <!-- </a> -->
        </div>
        <div class="game-logo-div">
            <!-- <a href="profile_csgo.php#gametab" class="red-link"> -->
                <img id="csgoTab" src="img/counter-strike-global-offensive.png" class="width100 opacity-hover" alt="Counter Strike: Global Offensive" title="Counter Strike: Global Offensive">
            </a>
        </div>
    	<div class="game-logo-div">
            <!-- <a href="profile_lol.php#gametab" class="red-link"> -->
                <img id="lolTab" src="img/league-of-legends.png" class="width100 opacity-hover" alt="League of Legends" title="League of Legends">
            </a>
        </div>
    	<div class="game-logo-div last-game-logo">
            <!-- <a href="profile_ow.php#gametab" class="red-link"> -->
                <img id="owTab" src="img/overwatch.png" class="width100 opacity-hover" alt="Overwatch" title="Overwatch">
            </a>
        </div>
    </div>

    <div class="clear"></div>

    <div id="gameTab" class="text-center">
      <?php
      if ($exchangeData)
      {
          for ($i=0; $i <count($exchangeData) ; $i++)
          {
              // if (strpos($exchangeData[$i]['name'],"TBD") == false && date('Y-m-d H:i',strtotime($exchangeData[$i]['begin_at'])) > date('Y-m-d H:i'))
              if (strpos($exchangeData[$i]['name'],"TBD") == false && date('Y-m-d',strtotime($exchangeData[$i]['begin_at'])) > date('Y-m-d'))
              {
                  $No++;
                  ?>
                  <form class="" action="placeWage.php" method="post">
                      <input type="hidden" name="game_id" value="<?php echo $exchangeData[$i]['id'] ?>">
                      <input type="hidden" name="title" value="<?php echo $exchangeData[$i]['league']['name'] ?>">
                      <input type="hidden" name="matches" value="<?php echo $exchangeData[$i]['slug'] ?>">
                      <div class="bluered-gradient-bg result-color-div width100 ow-bluered">
                          <div class="div1a">
                              <p class="logo-p1">
                                  <?php
                                  if ($exchangeData[$i]['opponents'][0]['opponent']['image_url'])
                                  {
                                  ?>
                                      <img class="team-logo" src="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['image_url'] ?>" alt="">
                                  <?php
                                  }
                                  else
                                  {
                                      echo $exchangeData[$i]['opponents'][0]['opponent']['name'];
                                  }
                                  ?>
                              </p>
                              <p class="bet-team-name"><?php echo $exchangeData[$i]['opponents'][0]['opponent']['name'] ?></p>
                              <button class="clean blue-button bet-button" value="<?php echo $exchangeData[$i]['opponents'][0]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team0".$i ?>">
                                  Place Wage
                              </button>
                          </div>
                          <div class="div4a">
                              <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
                          </div>
                          <div class="div5a">
                              <?php
                              if ($exchangeData[$i]['league']['image_url'])
                              {
                              ?>
                                  <img  class="bet-game-logo team-logo" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""><?php
                              }
                              else
                              {
                              ?>
                                  <img  class="bet-game-logo team-logo" src="img/dota_logo.png" ><?php
                              }
                              ?>
                              <p class="bet-team-name">
                                  <b><?php echo $exchangeData[$i]['league']['name'] ?> (<?php echo str_replace(":",":<br>",$exchangeData[$i]['name']) ?>)</b><br>
                                  <?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?>
                              </p>
                          </div>
                          <div class="div6a">
                              <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
                          </div>
                          <div class="div9a">
                              <p class="logo-p1">
                                  <?php
                                  if ($exchangeData[$i]['opponents'][1]['opponent']['image_url'])
                                  {
                                  ?>
                                      <img class="team-logo" src="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['image_url'] ?>" alt=""><?php
                                  }
                                  else
                                  {
                                      echo $exchangeData[$i]['opponents'][1]['opponent']['name'];
                                  }
                                  ?>
                              </p>
                              <p class="bet-team-name"><?php echo $exchangeData[$i]['opponents'][1]['opponent']['name'] ?></p>
                              <button class="clean transparent-button blue-button bet-button" value="<?php echo $exchangeData[$i]['opponents'][1]['opponent']['name'] ?>" type="submit" name='team' id="<?php echo "team1".$i ?>" title="Place Your Wage">
                                  Place Wage
                              </button>
                          </div>
                      </div>
                  </form>
              <?php
              }
          }
      }
      ?>
    </div>

    <!-- Upgrade VIP Modal Old-->
    <div id="upgrade-modal" class="modal-css">
        <div class="modal-content-css register-modal-content">
            <span class="close-css close-upgrade">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Upgrade To VIP</h2>
            <!-- <form method="POST" action="utilities/topUpFunction.php"> -->
            <form method="POST" action="utilities/upgradeVipFunction.php">
                <div class="width100">
                <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash?</p>
                    <select class="clean input-name admin-input" type="text" name="update_vip" id="update_vip" required>
                        <option value="No" name="No" selected>No</option>
                        <option value="Yes" name="Yes">Yes</option>
                    </select>
                </div>

                <button class="blue-button white-text width100 clean register-button"  name="submit">Confirm</button>
            </form>
        </div>
    </div>

    <!-- Top Up Modal New-->
    <div id="topup-modal" class="modal-css">
        <div class="modal-content-css register-modal-content topup-modal-content">
            <span class="close-css close-topup">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Recharge</h2>

            <!-- <form method="POST" action="utilities/requestTopUpFunction.php" > -->
            <!-- <form method="POST" action="utilities/requestTopUpFunction.php" enctype="multipart/form-data"> -->
            <form method="POST" action="#" enctype="multipart/form-data">

                <div class="qr-div">
                    <img src="img/blockchain-qr.png" class="qr-png" alt="Recharge Details" title="Recharge Details">
                    <p class="qr-p">Bitcoin Wallet Address (Scan to Pay)</p>
                    <div class="invitation-link-container shadow-white-div black-text copy-btc">
                        <input type="hidden" id="copyBitcoin" value="1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM">
                        <a id="btcaddress" href="1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM" class="btcaddress">
                            1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM
                        </a>
                    </div>
                    <div class="clean blue-btn-hover copy-btn" id="copy-bitcoin-link">COPY</div>
                    <!-- <div class="clean blue-btn-hover copy-btn" id="copy-address">COPY</div> -->
                </div>

                <div class="clear"></div>

                <!-- <div class="clear"></div>

                <div class="copy-btc2">
                    <p class="input-top-p big-top-p long-p">How Many PingCash You Would Like to Recharge ?</p>
                    <input class="input-name clean" type="number" placeholder="0 PingCash" id="cash_value" name="cash_value" required>
                </div>
                <div class="clear"></div>
                    <?php
                    $vip = $userDetails->getVipStatus();
                    if($vip == 'Yes')
                    {   }
                    else
                    {
                    ?>
                        <div class="copy-btc2">
                        <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash ?</p>
                            <select class="clean input-name admin-input" type="text" name="update_vip" id="update_vip" required>
                                <option value="No" name="No" selected>No</option>
                                <option value="Yes" name="Yes">Yes</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                    <?php
                    }
                    ?>
                <div class="copy-btc2 overflow">
                    <p class="input-top-p admin-top-p">Upload Receipt</p>
                    <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" required /></p>
                </div>
                <div class="clear"></div>
                <div class="copy-btc2">
                	<button class="blue-button white-text width100 clean register-button"  name="submit">Confirm</button>
                </div> -->
            </form>
        </div>
    </div>

</div>


<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Refer Link Copied !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Not enough PingCash. <br> Please top up.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Unable to place wage!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "The wages is placed !!";
            // $messageType = "Unfortunately, you lose.<br>Better luck next time.";
            // $messageType = "Thank You for betting.";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Unable to place wage !!";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Unable to record wage !!";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Congratulations, you win!";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Unknown Result.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Top Up Successfully! <br> Congrtulation, You are a VIP now!";
            $messageType = "Congrtulation, You are a VIP now!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Top Up Request Sumbitted !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Unable to top up !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Not enough PingCash to upgrade as VIP. <br> Please Top Up.";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "ERROR!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile picture uploaded !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to upload profile picture !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
    //   $("#copy-address").click(function(){
    //       var textArea = document.createElement("textarea");
    //       textArea.value = $('#btcaddress').val();
    //       document.body.appendChild(textArea);
    //       textArea.select();
    //       document.execCommand("Copy");
    //       textArea.remove();
    //       putNoticeJavascript("Copied!! ","");
    //   });
</script>

<script>
    $("#copy-bitcoin-link").click(function()
    {
        var textArea = document.createElement("textarea");
        textArea.value = $('#copyBitcoin').val();
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        putNoticeJavascript("Link Copied!! ","");
    });
</script>
<script>
$(document).ready(function(){
  $("#dota2Tab").on('click',function(){
    $.ajax({
      url: 'profile_dota.php',
      success:function(data){
        $("#gameTab").html(data);
      }
    });
  });
    $("#lolTab").on('click',function(){
      $.ajax({
        url: 'profile_lol.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $("#csgoTab").on('click',function(){
      $.ajax({
        url: 'profile_csgo.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $("#owTab").on('click',function(){
      $.ajax({
        url: 'profile_ow.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $(document).ajaxStart(function(){
      $("#gameTab").html("Loading...");
    });
});
</script>

<!-- <script type="text/javascript">
$(document).ready(function()
{
    $("#divCounter").load("countdown.php");
setInterval(function()
{
    $("#divCounter").load("countdown.php");
}, 1000);
});
</script> -->

</body>
</html>
