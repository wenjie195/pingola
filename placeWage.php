<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$longtime = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$selectTeam = rewrite($_POST['team']);
$title = rewrite($_POST['title']);
$gameId = rewrite($_POST['game_id']);
$matches = rewrite($_POST['matches']);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/placeWage.php" />
<meta property="og:title" content="Place Wage | Pingola" />
<title>Place Wage | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/placeWage.php" />
<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">
<!--
    <h1 class="h1-title" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	    Return
        </a>
    </h1>
-->
    <div class="modal-content-css register-modal-content">
        <div class="clear"></div>
        <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Place Your Wage</h2>
        <p class="black-text match-title"><?php echo $title;?></p>

        <form method="POST" action="utilities/placeWageFunction.php" enctype="multipart/form-data">

          <input type="hidden" name="game_id" value="<?php echo $gameId ?>">
          <input type="hidden" name="matches" value="<?php echo $matches ?>">

            <div class="clear"></div>
            
            <div class="dual-input">
                <p class="input-top-p  big-top-p">Team Select</p>
                <select class="clean input-name admin-input" name="team_selected" id="team_selected" required>
                    <option value="<?php echo $selectTeam ?>"><?php echo $selectTeam ?></option>
                </select>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p big-top-p">Place Wage</p>
                <input class="input-name clean" type="number" placeholder="0 PingCash" name="wage_value" id="wage_value" required>
            </div>

            <input class="input-name clean" type="hidden" value="<?php echo $title?>" name="game_title" id="game_title" readonly>

            <button class="blue-button white-text width100 clean register-button"  name="submit" >Confirm</button>
			<div class="width100 text-center margin-top20">
            	<a  onclick="goBack()" class="blue-a1">Cancel</a>
            </div>
        </form>
    </div>

</div>

<?php include 'js.php'; ?>

<script>
    function goBack()
    {
    window.history.back();
    }
</script>

</body>
</html>