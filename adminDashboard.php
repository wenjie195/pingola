<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $depositVip = getBetstatus($conn," WHERE result = 'PENDING' ");

// $undefinedMatch = getBetstatus($conn," WHERE result = 'PENDING' ");
$undefinedMatch = getBetstatus($conn," WHERE result = 'PENDING' AND matches = '' ");

// $depositVip = getDeposit($conn," WHERE upgrade = 'Yes' AND status = 'Pending' ");
// $deposit = getDeposit($conn," WHERE upgrade = 'No' AND status = 'Pending' ");

// $sharingDetails = getSharing($conn);
// $sharingDetails = getSharing($conn,"WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminDashboard.php" />
<meta property="og:title" content="Admin Dashboard | Pingola" />
<title>Admin Dashboard | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminDashboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

	<div class="width100 overflow text-center">

    <h1 class="line-header margin-bottom50">Pending Match (undefined)</h1>
    <div class="overflow-scroll-div">       
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Match</th>
                        <th>Team</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($undefinedMatch)
                    {
                        for($cnt = 0;$cnt < count($undefinedMatch) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $undefinedMatch[$cnt]->getUsername();?></td>
                                <td><?php echo $undefinedMatch[$cnt]->getMatches();?></td>
                                <td><?php echo $undefinedMatch[$cnt]->getTeam();?></td>
                                <td><?php echo $undefinedMatch[$cnt]->getAmount();?></td>
                                <td>
                                    <form method="POST" action="utilities/adminRefundBetFunction.php" class="hover1">
                                        <button class="refund-button clean" type="submit" name="trade_uid" value="<?php echo $undefinedMatch[$cnt]->getTradeUid();?>">
                                            Refund
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
        </table>
      </div>
    </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>