<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","credit"),
          array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit),"sssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          echo "unable to register";
          // return false;
     }
     else
     {}
     return true;
}

function registerUpline($conn,$referralId,$referralName,$referrerId)
{
     if(insertDynamicData($conn,"referral_history",array("referral_id","referral_name","referrer_id"),
          array($referralId,$referralName,$referrerId),"sss") === null)
     {
          echo "unable to register";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $phoneNo = rewrite($_POST['register_phone']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $credit = "5";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $phoneNo ."<br>";
     // echo $register_password ."<br>";
     // echo $register_retype_password ."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
               $userEmailDetails = $userEmailRows[0];

               $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($phoneNo),"s");
               $userPhoneDetails = $userPhoneRows[0];

               if (!$userEmailDetails && !$userPhoneDetails && !$usernameDetails)
               {
                    if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit))
                    {
                         // echo "<script>alert('Register Success !');window.location='../index.php'</script>";    

                         $referrerUid = $uid;
                         $referralName = $username;
                         $currentLevel = "0";
                         $topReferrerUid = $uid;

                         if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                         {
                              header('Location: ../thankYou.php');
                         }
                         else
                         { 
                              $_SESSION['messageType'] = 1;
                              header('Location: ../index.php?type=2');                              
                         }

                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../index.php?type=3');
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=4');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../index.php?type=5');
               //echo "<script>alert('password must be more than 6');window.location='../index.php'</script>";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=6');
     }      
}
else 
{
     header('Location: ../index.php');
}

?>