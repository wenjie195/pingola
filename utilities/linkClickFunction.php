<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $previousClick = $userDetails[0]->getLinkClick();
     $newClick = "1";
     $totalClick = $previousClick + $newClick;

     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $previousClick."<br>";
     // echo $newClick."<br>";
     // echo $totalClick."<br>";

     if(!$user)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($totalClick)
          {
               array_push($tableName,"link_click");
               array_push($tableValue,$totalClick);
               $stringType .=  "s";
          }

          array_push($tableValue,$uid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               // echo "success";
               // header('Location: ' . $_SERVER['HTTP_REFERER']);
               // exit;
               $_SESSION['messageType'] = 1;
               header('Location: ../profile.php?type=1');         
          }
          else
          {
          //    $_SESSION['messageType'] = 1;
          //    header('Location: ../editProfile.php?type=2');
               echo "fail";
          }
     }
     else
     {
          echo "gg";
     //     $_SESSION['messageType'] = 1;
     //     header('Location: ../editProfile.php?type=3');
     }
 
 
}
else 
{
     header('Location: ../index.php');
}
?>