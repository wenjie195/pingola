<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $username = rewrite($_POST["edit_username"]);
    $email = rewrite($_POST["edit_email"]);
    $phone = rewrite($_POST["edit_phone"]);

    $fullname = rewrite($_POST["fullname"]);
    $address = rewrite($_POST["address"]);
    $area = rewrite($_POST["area"]);
    $postcode = rewrite($_POST["postcode"]);
    $state = rewrite($_POST["state"]);
    $bank = rewrite($_POST["bank_name"]);
    $bankAccNumber = rewrite($_POST["bank_account_number"]);
    $bankAccHolder = rewrite($_POST["bank_account_holder"]);

    //   FOR DEBUGGING 
    // echo "<br>";
    // echo $fullname."<br>";
    // echo $register_email."<br>";
    // echo $register_contact."<br>";

    $user = getUser($conn," uid = ?  ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }

        if($fullname)
        {
            array_push($tableName,"full_name");
            array_push($tableValue,$fullname);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($area)
        {
            array_push($tableName,"area");
            array_push($tableValue,$area);
            $stringType .=  "s";
        }
        if($postcode)
        {
            array_push($tableName,"postcode");
            array_push($tableValue,$postcode);
            $stringType .=  "s";
        }
        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }
        if($bank)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bank);
            $stringType .=  "s";
        }
        if($bankAccNumber)
        {
            array_push($tableName,"bank_account_no");
            array_push($tableValue,$bankAccNumber);
            $stringType .=  "s";
        }
        if($bankAccHolder)
        {
            array_push($tableName,"bank_account_name");
            array_push($tableValue,$bankAccHolder);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=1');
            // echo "<script>alert('Update Profile success !');window.location='../editProfile.php'</script>";
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editProfile.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
