<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = rewrite($_POST["message_uid"]);
    $senderUID = rewrite($_POST["sender_uid"]);
    $message_details = rewrite($_POST["message_details"]);
    $adminStatus = "REPLY";
    $userStatus = "GET";
    $updateMessageStatus = "NO";

    // //for debugging
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $senderUID."<br>";
    // echo $message_details."<br>";

    if(isset($_POST['reply_sms']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($message_details)
        {
            array_push($tableName,"reply_message");
            array_push($tableValue,$message_details);
            $stringType .=  "s";
        }     
        if($adminStatus)
        {
            array_push($tableName,"admin_status");
            array_push($tableValue,$adminStatus);
            $stringType .=  "s";
        } 
        if($userStatus)
        {
            array_push($tableName,"user_status");
            array_push($tableValue,$userStatus);
            $stringType .=  "s";
        } 

        array_push($tableValue,$message_uid);
        $stringType .=  "s";
        $messageUpdated = updateDynamicData($conn,"message"," WHERE message_uid = ? ",$tableName,$tableValue,$stringType);
        
        if($messageUpdated)
        {
            if(isset($_POST['reply_sms']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database"; 
                if($adminStatus)
                {
                    array_push($tableName,"admin_status");
                    array_push($tableValue,$adminStatus);
                    $stringType .=  "s";
                } 
                if($userStatus)
                {
                    array_push($tableName,"user_status");
                    array_push($tableValue,$userStatus);
                    $stringType .=  "s";
                } 
                array_push($tableValue,$senderUID);
                $stringType .=  "s";
                $messageStatusInUser = updateDynamicData($conn,"message"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($messageStatusInUser)
                {
                    if(isset($_POST['reply_sms']))
                    {   
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database"; 
                        if($updateMessageStatus)
                        {
                        array_push($tableName,"message");
                        array_push($tableValue,$updateMessageStatus);
                        $stringType .=  "s";
                        } 
                        array_push($tableValue,$senderUID);
                        $stringType .=  "s";
                        $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($messageStatusInUser)
                        {
                            // header('Location: ../adminMessageAll.php?type=1');
                            if(isset($_SESSION['url'])) 
                            {
                                $url = $_SESSION['url']; 
                                header("location: $url");
                            }
                            else 
                            {
                                // header("location: $url");
                                header('Location: ../adminMessageAll.php');
                            }
                        }
                        else
                        {
                            header('Location: ../adminMessageAll.php?type=2');
                        }
                    }
                    else
                    {
                        header('Location: ../adminMessageAll.php?type=3');
                    }
                }
                else
                {
                    header('Location: ../adminMessageAll.php?type=4');
                }
            }
            else
            {
                header('Location: ../adminMessageAll.php?type=5');
            }
        }
        else
        {
            header('Location: ../adminMessageAll.php?type=6');
        }
    }
    else
    {
        header('Location: ../adminMessageAll.php?type=7');
    }
}
else 
{
    header('Location: ../index.php');
}
?>