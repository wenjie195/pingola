<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/BetStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $tradeUid = rewrite($_POST["trade_uid"]);

    $tradeRow = getBetstatus($conn," WHERE trade_uid = ? ",array("trade_uid"),array($tradeUid),"s");
    $userUid = $tradeRow[0]->getUid();
    $username = $tradeRow[0]->getUsername();
    $amount = $tradeRow[0]->getAmount();

    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
    $currentCredit = $userDetails[0]->getCredit();

    $refundCredit = $currentCredit + $amount;
    $updateGameResult = "REFUND";
    $updateGameStatus = "REFUND";

    // $depositDetails = getDeposit($conn," WHERE uid = ? ",array("uid"),array($depositID),"s");
    // $amount = $depositDetails[0]->getAmount();

    // $userUid = $depositDetails[0]->getUserUid();
    // $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
    // $currentCredit = $userDetails[0]->getCredit();
    // $newCredit = $currentCredit + $amount;
    // $status = "Approved";

    // $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    // $adminUsername = $adminDetails[0]->getUsername();

    // $tz = 'Asia/Kuala_Lumpur';
    // $timestamp = time();
    // $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    // $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    // $verifyTime = $dt->format('Y-m-d H:i:s');
 
    //for debugging
    // echo "<br>";
    // echo $depositID."<br>";
    // echo $depositUid."<br>";
    // echo $depositUsername."<br>";


    if(isset($_POST['trade_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($updateGameResult)
        {
            array_push($tableName,"result");
            array_push($tableValue,$updateGameResult);
            $stringType .=  "s";
        }   
        if($updateGameStatus)
        {
            array_push($tableName,"status");
            array_push($tableValue,$updateGameStatus);
            $stringType .=  "s";
        }   

        array_push($tableValue,$tradeUid);
        $stringType .=  "s";
        // $renewCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        $gameRefundStatus = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
        if($gameRefundStatus)
        {

            if(isset($_POST['trade_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($refundCredit)
                {
                    array_push($tableName,"credit");
                    array_push($tableValue,$refundCredit);
                    $stringType .=  "s";
                }               
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                // $orderUpdated = updateDynamicData($conn,"deposit"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($orderUpdated)
                {
                    header('Location: ../adminDashboard.php');
                }
                else
                {
                    echo "fail aa";
                }
            }
            else
            {
                echo "dunno aa";
            }
        }
        else
        {
            echo "fail";
        }
    }
    else
    {
        echo "dunno";
    }
}
else 
{
    header('Location: ../index.php');
}

?>