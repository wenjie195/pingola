<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Reviews.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["review_uid"]);
    
    $display = "Yes";

    $rate = rewrite($_POST["rate"]);
    $issueUid = rewrite($_POST["issue_uid"]);

    // $previousReview = getReview($conn," WHERE company_uid = ? AND display != 'Delete' OR display != 'Pending' ",array("company_uid"),array($issueUid),"s");
    $previousRating = getReviews($conn," WHERE company_uid = ? AND display = 'Yes' ",array("company_uid"),array($issueUid),"s");
    if($previousRating)
    {
        $totalRating = 0;
        for ($cnt=0; $cnt <count($previousRating) ; $cnt++)
        {
            $totalRating += $previousRating[$cnt]->getRating();
        }
    }
    else
    {  
        $totalRating = 0;
    }

    if($previousRating)
    {   
        $totalReview = count($previousRating);
        $newReview = "1";
        $sum = $totalReview + $newReview;
        // echo $sum = $newReview;
    }
    else
    {   
        $totalReview = 0;   
        $newReview = "1";
        $sum = $totalReview + $newReview;
        // echo $sum = $newReview;
    }

    // $newRating = round(($totalRating + $rate) / $sum );                 
    $newRate = (($totalRating + $rate) / $sum );    

    $newRating = round($newRate);   

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $display."<br>";
    // echo $rate."<br>";
    // echo $issueUid."<br>";
    // echo $totalRating."<br>";
    // echo $totalReview."<br>";
    // echo $sum."<br>";
    // echo $newRate."<br>";
    // echo $newRating."<br>";

    if(isset($_POST['review_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $approvedReview = updateDynamicData($conn,"reviews"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($approvedReview)
        {
            if(isset($_POST['review_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newRating)
                {
                    array_push($tableName,"rating");
                    array_push($tableValue,$newRating);
                    $stringType .=  "s";
                }    
        
                array_push($tableValue,$issueUid);
                $stringType .=  "s";
                $approvedReview = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($approvedReview)
                {
                    // echo "success";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminReviewApproved.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminReviewPending.php?type=3');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminReviewPending.php?type=4');
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminReviewPending.php?type=3');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminReviewPending.php?type=4');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>