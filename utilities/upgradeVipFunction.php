<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $updateVip = rewrite($_POST['update_vip']);
     $upgradeVipStatus = "Yes";

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $previousCredit = $userDetails[0]->getCredit();

     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $previousClick."<br>";
     // echo $newClick."<br>";
     // echo $totalClick."<br>";

     if($updateVip == 'Yes')
     {
          if($previousCredit > 200)
          {
               $upgradeVipAmount = "200";
               $newCredit = $previousCredit - $upgradeVipAmount;

               if(!$user)
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($upgradeVipStatus)
                    {
                         array_push($tableName,"vip_status");
                         array_push($tableValue,$upgradeVipStatus);
                         $stringType .=  "s";
                    }
     
                    if($newCredit)
                    {
                         array_push($tableName,"credit");
                         array_push($tableValue,$newCredit);
                         $stringType .=  "s";
                    }
     
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                         $_SESSION['messageType'] = 2;
                         header('Location: ../profile.php?type=1');  
                    }
                    else
                    {
                         $_SESSION['messageType'] = 2;
                         header('Location: ../profile.php?type=3');  
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../profile.php?type=5');  
               }

          }
          elseif($previousCredit <= 200)
          {
               $_SESSION['messageType'] = 2;
               header('Location: ../profile.php?type=4');  
          }
     }
     elseif($updateVip == 'No')
     {
          header('Location: ../profile.php');  
     } 
 
}
else 
{
     header('Location: ../index.php');
}
?>