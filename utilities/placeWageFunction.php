<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Games.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

// function recordBet($conn,$betUid,$uid,$username,$teamSelected,$wageValue,$valueOne,$valueTwo,$currentCash,$gameStatus)
// {
//      if(insertDynamicData($conn,"bet_status",array("trade_uid","uid","username","currency","amount","start_rate","end_rate","current_credit","status"),
//           array($betUid,$uid,$username,$teamSelected,$wageValue,$valueOne,$valueTwo,$currentCash,$gameStatus),"sssssssss") === null)
//      {
//           echo "gg";
//           // header('Location: ../addReferee.php?promptError=1');
//           //     promptError("error registering new account.The account already exist");
//           //     return false;
//      }
//      else{    }
//      return true;
// }
function recordBet($conn,$tradeUid,$gameId,$uid,$username,$matches,$teamSelected,$wageValue,$currentCash,$gameResult,$gameStatus)
{
     if(insertDynamicData($conn,"bet_status",array("trade_uid","game_id","uid","username","matches","team","amount","current_credit","result","status"),
          array($tradeUid,$gameId,$uid,$username,$matches,$teamSelected,$wageValue,$currentCash,$gameResult,$gameStatus),"ssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $betUid = md5(uniqid());

    $teamSelected = rewrite($_POST["team_selected"]);
    $wageValue = rewrite($_POST["wage_value"]);
    // $valueOne = rewrite($_POST["value_one"]);
    // $valueTwo = rewrite($_POST["value_two"]);
    $gametitle = rewrite($_POST["game_title"]);
    $gameId = rewrite($_POST['game_id']);
    $matches = rewrite($_POST['matches']);
    $tradeUid = md5(uniqid());
    // $gameUid = md5(uniqid());
    // $gameStat = rand(1,2);
    // if ($gameStat == 1) {
    //   $gameResult = "WIN";
    // }elseif($gameStat == 2){
    //   $gameResult = "LOSE";
    // }
    $gameResult = "PENDING";
    $gameStatus = "ORI";
    // $currentCash = rewrite($_POST["current_cash"]);

    $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
    $currentCash = $userDetails[0]->getCredit();
    $username = $_SESSION['username'];

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $betUid."<br>";
    // echo $teamSelected."<br>";
    // echo $wageValue."<br>";
    // echo $valueOne."<br>";
    // echo $valueTwo."<br>";
    // echo $gameUid."<br>";
    // echo $gameStatus."<br>";
    // echo $currentCash."<br>";

    if($currentCash < $wageValue)
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../profile.php?type=2');
    }
    elseif($currentCash >= $wageValue)
    {

        // if($gameResult == 'WIN')
        // {
        //     $updateCredit = $currentCash + $wageValue;
        //
        //     if(recordBet($conn,$uid,$username,$teamSelected,$wageValue,$currentCash,$gameResult,$gameStatus))
        //     {
        //         if(isset($_POST['submit']))
        //         {
        //             $tableName = array();
        //             $tableValue =  array();
        //             $stringType =  "";
        //             //echo "save to database";
        //             if($updateCredit)
        //             {
        //                 array_push($tableName,"credit");
        //                 array_push($tableValue,$updateCredit);
        //                 $stringType .=  "s";
        //             }
        //             array_push($tableValue,$uid);
        //             $stringType .=  "s";
        //             $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        //             if($newDownlineAmount)
        //             {
        //                 $_SESSION['messageType'] = 1;
        //                 header('Location: ../profile.php?type=8');
        //             }
        //             else
        //             {
        //                 $_SESSION['messageType'] = 1;
        //                 header('Location: ../profile.php?type=5');
        //             }
        //         }
        //         else
        //         {
        //             $_SESSION['messageType'] = 1;
        //             header('Location: ../profile.php?type=6');
        //         }
        //     }
        //     else
        //     {
        //         $_SESSION['messageType'] = 1;
        //         header('Location: ../profile.php?type=7');
        //     }
        //
        // }
        // elseif($gameResult == 'LOSE')
        // {
            $updateCredit = $currentCash - $wageValue;

            // if(recordBet($conn,$betUid,$uid,$username,$teamSelected,$wageValue,$valueOne,$valueTwo,$currentCash,$gameStatus))
            if(recordBet($conn,$tradeUid,$gameId,$uid,$username,$matches,$teamSelected,$wageValue,$currentCash,$gameResult,$gameStatus))
            {
                if(isset($_POST['submit']))
                {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($updateCredit)
                    {
                        array_push($tableName,"credit");
                        array_push($tableValue,$updateCredit);
                        $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($newDownlineAmount)
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../profile.php?type=4');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../profile.php?type=5');
                    }
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../profile.php?type=6');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../profile.php?type=7');
            }

        // }
        // else
        // {
        //     $_SESSION['messageType'] = 1;
        //     header('Location: ../profile.php?type=9');
        // }

    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../profile.php?type=3');
    }

}
else
{
    header('Location: ../index.php');
}
?>
