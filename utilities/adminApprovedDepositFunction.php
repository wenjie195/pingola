<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $depositID = rewrite($_POST["deposit_uid"]);
    $depositDetails = getDeposit($conn," WHERE uid = ? ",array("uid"),array($depositID),"s");
    $amount = $depositDetails[0]->getAmount();

    $userUid = $depositDetails[0]->getUserUid();
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
    $currentCredit = $userDetails[0]->getCredit();
    $newCredit = $currentCredit + $amount;
    $status = "Approved";

    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $verifyTime = $dt->format('Y-m-d H:i:s');
 
    //for debugging
    // echo "<br>";
    // echo $depositID."<br>";
    // echo $depositUid."<br>";
    // echo $depositUsername."<br>";


    if(isset($_POST['deposit_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($newCredit)
        {
            array_push($tableName,"credit");
            array_push($tableValue,$newCredit);
            $stringType .=  "s";
        }   

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $renewCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($renewCredit)
        {

            if(isset($_POST['deposit_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($status)
                {
                    array_push($tableName,"status");
                    array_push($tableValue,$status);
                    $stringType .=  "s";
                }    
                if($adminUsername)
                {
                    array_push($tableName,"verify_by");
                    array_push($tableValue,$adminUsername);
                    $stringType .=  "s";
                } 
                if($verifyTime)
                {
                    array_push($tableName,"verify_time");
                    array_push($tableValue,$verifyTime);
                    $stringType .=  "s";
                }            
                array_push($tableValue,$depositID);
                $stringType .=  "s";
                $orderUpdated = updateDynamicData($conn,"deposit"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($orderUpdated)
                {
                    header('Location: ../adminDashboard.php');
                }
                else
                {
                    echo "fail aa";
                }
            }
            else
            {
                echo "dunno aa";
            }
        }
        else
        {
            echo "fail";
        }
    }
    else
    {
        echo "dunno";
    }
}
else 
{
    header('Location: ../index.php');
}

?>