<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitTopUp($conn,$uid,$userUid,$username,$cashValue,$updateVip,$imageOne,$status)
{
     if(insertDynamicData($conn,"deposit",array("uid","user_uid","username","amount","upgrade","reference","status"),
     array($uid,$userUid,$username,$cashValue,$updateVip,$imageOne,$status),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $userUid = $_SESSION['uid'];

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     $username = $userDetails[0]->getUsername();

     $cashValue = rewrite($_POST['cash_value']);
     $updateVip = rewrite($_POST['update_vip']);

     $imageOne = $uid.$_FILES['image_one']['name'];
     $target_dir = "../uploadsRecipt/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $status = "Pending";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $loginType."<br>";

     if(submitTopUp($conn,$uid,$userUid,$username,$cashValue,$updateVip,$imageOne,$status))
     {
          // echo "success 1";
          echo "<script>alert('Deposit Submitted !');window.location='../profile.php'</script>";
     }
     else
     {
          // echo "fail 1";
          echo "<script>alert('Fail to submit deposit !');window.location='../profile.php'</script>";
     }

     // if($updateVip == 'Yes')
     // {
     //      if(submitTopUpVip($conn,$uid,$userUid,$username,$cashValue,$updateVip,$imageOne,$status))
     //      {
     //           // echo "success 1";
     //           echo "<script>alert('Deposit Submitted !');window.location='../profile.php'</script>";
     //      }
     //      else
     //      {
     //           // echo "fail 1";
     //           echo "<script>alert('Fail to submit deposit !');window.location='../profile.php'</script>";
     //      }
     // }
     // elseif($updateVip == 'No')
     // {
     //      if(submitTopUp($conn,$uid,$userUid,$username,$cashValue,$imageOne,$status))
     //      {
     //           // echo "success 2";
     //           echo "<script>alert('Deposit Submitted !');window.location='../profile.php'</script>";
     //      }
     //      else
     //      {
     //           // echo "fail 2";
     //           echo "<script>alert('Fail to submit deposit !');window.location='../profile.php'</script>";
     //      }
     // }

}
else
{
     header('Location: ../index.php');
}
?>
