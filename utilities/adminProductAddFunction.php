<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
// require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewProduct($conn,$productUid,$name,$description,$keywordOne,$link,$slug,$status,$descriptionTwo,$descriptionThree,$descriptionFour,$descriptionFive,$descriptionSix,$category,$brand)
{
     if(insertDynamicData($conn,"product",array("uid","name","description","keyword_one","link","slug","status","description_two","description_three","description_four","description_five","description_six","category","brand"),
          array($productUid,$name,$description,$keywordOne,$link,$slug,$status,$descriptionTwo,$descriptionThree,$descriptionFour,$descriptionFive,$descriptionSix,$category,$brand),"ssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $productUid = md5(uniqid());
     // $_SESSION['product_uid'] = $productUid;

     // NEW ADD PRODUCT FUNCTION
     $category= rewrite($_POST['register_category']);
     // $brand = rewrite($_POST['register_brand']);
     $brand = NULL;
     $name = rewrite($_POST['register_name']);
     $description = rewrite($_POST['register_description']);
     $descriptionTwo = rewrite($_POST['register_description_two']);
     $descriptionThree = rewrite($_POST['register_description_three']);
     $descriptionFour = rewrite($_POST['register_description_four']);
     $descriptionFive = rewrite($_POST['register_description_five']);
     $descriptionSix = rewrite($_POST['register_description_six']);
     $keywordOne = rewrite($_POST['register_keyword']);
     $link = rewrite($_POST['register_link']);
     $slug = rewrite($_POST['register_name']);
     $status = "Available";

     $productNameDetails = getProduct($conn," WHERE name = ? ",array("name"),array($name),"s");
     $registeredProductName = $productNameDetails[0];

     if(!$registeredProductName)
     {
          if(registerNewProduct($conn,$productUid,$name,$description,$keywordOne,$link,$slug,$status,$descriptionTwo,$descriptionThree,$descriptionFour,$descriptionFive,$descriptionSix,$category,$brand))
          {
               // $_SESSION['product_uid'] = $productUid;
               $_SESSION['product_uid'] = $productUid;
               header('Location: ../addMultipleImageProduct.php');
               // echo "success";
          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          // echo "product name already register !!";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminProductsAdd.php?type=3');
     }
}
else
{
     header('Location: ../index.php');
}
?>