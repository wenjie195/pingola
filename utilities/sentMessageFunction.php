<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
// require_once dirname(__FILE__) . '/../classes/Registration.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function sentMessage($conn,$uid,$username,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne)
{
     if(insertDynamicData($conn,"message",array("uid","username","message_uid","receive_message","user_status","admin_status","reply_one"),
     array($uid,$username,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

function sentMessageB($conn,$uid,$username,$message_uid,$receiveSMS,$productName,$userStatus,$adminStatus,$replyOne)
{
     if(insertDynamicData($conn,"message",array("uid","username","message_uid","receive_message","reply_three","user_status","admin_status","reply_one"),
     array($uid,$username,$message_uid,$receiveSMS,$productName,$userStatus,$adminStatus,$replyOne),"ssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $message_uid = md5(uniqid());

     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     // $username = $userRows[0]->getName();
     $username = $userRows[0]->getUsername();

     $receiveSMS = rewrite($_POST["message_details"]);
     $productName = rewrite($_POST["product_name"]);

     $userStatus = "SENT";
     $adminStatus = "GET";
     $updateMessageStatus = "YES";
     $replyOne = $message_uid;

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $message_uid."<br>";
     // echo $username."<br>";
     // echo $receiveSMS."<br>";

     if(isset($_POST['submit']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database"; 
          if($updateMessageStatus)
          {
               array_push($tableName,"message");
               array_push($tableValue,$updateMessageStatus);
               $stringType .=  "s";
          } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               if($productName != '')
               {
                    if(sentMessageB($conn,$uid,$username,$message_uid,$receiveSMS,$productName,$userStatus,$adminStatus,$replyOne))
                    {
                         header('Location: ../liveChat.php');
                    }
                    else
                    {
                         header('Location: ../liveChat.php?type=2');
                    }
               }
               else
               {
                    if(sentMessage($conn,$uid,$username,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
                    {
                         header('Location: ../liveChat.php');
                    }
                    else
                    {
                         header('Location: ../liveChat.php?type=2');
                    }
               }
          }
          else
          {
               header('Location: ../liveChat.php?type=3');
          }
     }
     else
     {
     header('Location: ../liveChat.php?type=4');
     }

     // if($productName != '')
     // {
     //      if(sentMessageB($conn,$uid,$username,$message_uid,$receiveSMS,$productName,$userStatus,$adminStatus,$replyOne))
     //      {
     //           header('Location: ../liveChat.php');
     //      }
     //      else
     //      {
     //           header('Location: ../liveChat.php?type=2');
     //      }
     // }
     // else
     // {
     //      if(sentMessage($conn,$uid,$username,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
     //      {
     //           header('Location: ../liveChat.php');
     //      }
     //      else
     //      {
     //           header('Location: ../liveChat.php?type=2');
     //      }
     // }
}
else
{
     header('Location: ../index.php');
}
?>