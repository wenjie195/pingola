<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $cashValue = rewrite($_POST['cash_value']);
     $updateVip = rewrite($_POST['update_vip']);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $previousCredit = $userDetails[0]->getCredit();

     $totalCredit = $previousCredit + $cashValue;

     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $previousClick."<br>";
     // echo $newClick."<br>";
     // echo $totalClick."<br>";

     if($updateVip == 'Yes')
     {

          $updateFees = "200";
          $updateVipCredit = $totalCredit - $updateFees;

          if(!$user)
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($updateVip)
               {
                    array_push($tableName,"vip_status");
                    array_push($tableValue,$updateVip);
                    $stringType .=  "s";
               }

               if($updateVipCredit)
               {
                    array_push($tableName,"credit");
                    array_push($tableValue,$updateVipCredit);
                    $stringType .=  "s";
               }

               array_push($tableValue,$uid);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../profile.php?type=1');  
               }
               else
               {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../profile.php?type=3');  
               }
          }
          else
          {
               $_SESSION['messageType'] = 2;
               header('Location: ../profile.php?type=5');  
          }

          // if($totalCredit < 200)
          // { 
          //      $_SESSION['messageType'] = 2;
          //      header('Location: ../profile.php?type=4');  
          // }
          // else
          // { 
          //      $updateFees = "200";
          //      $updateVipCredit = $totalCredit - $updateFees;

          //      if(!$user)
          //      {   
          //           $tableName = array();
          //           $tableValue =  array();
          //           $stringType =  "";
          //           //echo "save to database";
          //           if($updateVip)
          //           {
          //                array_push($tableName,"vip_status");
          //                array_push($tableValue,$updateVip);
          //                $stringType .=  "s";
          //           }

          //           if($updateVipCredit)
          //           {
          //                array_push($tableName,"credit");
          //                array_push($tableValue,$updateVipCredit);
          //                $stringType .=  "s";
          //           }

          //           array_push($tableValue,$uid);
          //           $stringType .=  "s";
          //           $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          //           if($passwordUpdated)
          //           {
          //                $_SESSION['messageType'] = 2;
          //                header('Location: ../profile.php?type=1');  
          //           }
          //           else
          //           {
          //                $_SESSION['messageType'] = 2;
          //                header('Location: ../profile.php?type=3');  
          //           }
          //      }
          //      else
          //      {
          //           $_SESSION['messageType'] = 2;
          //           header('Location: ../profile.php?type=5');  
          //      }

          // }
     }
     // elseif($updateVip == 'No')
     else
     {

          $_SESSION['messageType'] = 2;
          header('Location: ../profile.php?type=2');  

          // if(!$user)
          // {   
          //      $tableName = array();
          //      $tableValue =  array();
          //      $stringType =  "";
          //      //echo "save to database";
     
          //      if($totalCredit)
          //      {
          //           array_push($tableName,"credit");
          //           array_push($tableValue,$totalCredit);
          //           $stringType .=  "s";
          //      }
     
          //      array_push($tableValue,$uid);
          //      $stringType .=  "s";
          //      $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          //      if($passwordUpdated)
          //      {
          //           $_SESSION['messageType'] = 2;
          //           header('Location: ../profile.php?type=2');  
          //      }
          //      else
          //      {
          //           $_SESSION['messageType'] = 2;
          //           header('Location: ../profile.php?type=3');  
          //      }
          // }
          // else
          // {
          //      $_SESSION['messageType'] = 2;
          //      header('Location: ../profile.php?type=5');  
          // }
     }
     // else
     // {
     //      echo "dunno";
     // }

     // if(!$user)
     // {   
     //      $tableName = array();
     //      $tableValue =  array();
     //      $stringType =  "";
     //      //echo "save to database";

     //      if($totalClick)
     //      {
     //           array_push($tableName,"link_click");
     //           array_push($tableValue,$totalClick);
     //           $stringType .=  "s";
     //      }

     //      array_push($tableValue,$uid);
     //      $stringType .=  "s";
     //      $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     //      if($passwordUpdated)
     //      {
     //           echo "success";
     //      }
     //      else
     //      {
     //           echo "fail";
     //      }
     // }
     // else
     // {
     //      echo "gg";
     // }
 
 
}
else 
{
     header('Location: ../index.php');
}
?>