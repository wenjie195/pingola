<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// function registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt)
// {
//      if(insertDynamicData($conn,"user",array("uid","member_id","username","email","firstname","lastname","country","phone_no","password","salt"),
//           array($uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt),"ssssssssss") === null)
//      {
//           // echo "gg";
//      }
//      else
//      {    }
//      return true;
// }

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","credit"),
          array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit),"sssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $phoneNo = rewrite($_POST['register_phone']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);
     $credit = "5";
     $uplineCredit = "1";
     $sponsorID = rewrite($_POST['sponsor_id']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $phoneNo ."<br>";
     // echo $register_password ."<br>";
     // echo $register_retype_password ."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     if($sponsorID)
     {
          $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
          if($referrerUserRows)
          {
               $referrerUid = $referrerUserRows[0]->getUid();
               $referrerName = $referrerUserRows[0]->getUsername();
               $currentCredit = $referrerUserRows[0]->getCredit();
               $currentDownlineNo = $referrerUserRows[0]->getReferNo();

               $referralName = $username;
               $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
               $currentLevel = 1;
               // $getUplineCurrentLevel = 1;

               $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
               if($referralHistoryRows)
               {
                    $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                    $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
               }
               $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
               if($referralNewestRows)
               {
                    // $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                    $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                    $usernameDetails = $usernameRows[0];
               
                    $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
                    $userEmailDetails = $userEmailRows[0];
               
                    $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                    $userPhoneDetails = $userPhoneRows[0];
                    
                    if($register_password == $register_retype_password)
                    {
                         if($register_password_validation >= 6)
                         {

                              if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
                              {
          
                                   $newCredit = $currentCredit + 1;
                                   $newDownline = $currentDownlineNo + 1;
          
                                   // if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$credit))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
          
                                             if(isset($_POST['register']))
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($newCredit)
                                                  {
                                                       array_push($tableName,"credit");
                                                       array_push($tableValue,$newCredit);
                                                       $stringType .=  "s";
                                                  }
                                                  if($newDownline)
                                                  {
                                                       array_push($tableName,"refer_no");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                  }
                                                  array_push($tableValue,$referrerUid);
                                                  $stringType .=  "s";
                                                  $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($newDownlineAmount)
                                                  {
                                                       header('Location: ../thankYou.php');
                                                  }
                                                  else 
                                                  {
                                                       // echo "fail";
                                                       // echo "<script>alert('Fail to register !!');window.location='../register.php'</script>";  
                                                       $_SESSION['messageType'] = 1;
                                                       header('Location: ../register.php?type=2');     
                                                  }
                                             }
                                             else 
                                             {
                                                  $_SESSION['messageType'] = 1;
                                                  header('Location: ../register.php?type=3');    
                                             }
          
                                        }
                                        else
                                        { 
                                             $_SESSION['messageType'] = 1;
                                             header('Location: ../register.php?type=4');   
                                        }
                                   }
                                   else
                                   { 
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../register.php?type=5');   
                                   }
          
                              }
                              else
                              { 
                                   $_SESSION['messageType'] = 1;
                                   header('Location: ../register.php?type=6');   
                              }

                         }
                         else 
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../register.php?type=7');   
                         }
                    }
                    else 
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../register.php?type=8');   
                    } 

               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../register.php?type=9');  
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../register.php?type=10');  
          } 
     }  
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../register.php?type=11');  
     } 
}
else 
{
     header('Location: ../index.php');
}

?>