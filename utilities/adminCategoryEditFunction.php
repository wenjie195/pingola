<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Category.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $categoryName = rewrite($_POST["update_category_name"]);
    $id = rewrite($_POST["category_id"]);

    $categoryStatus = rewrite($_POST["category_status"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $allCategory = getCategory($conn," WHERE name = ? AND status = ? ",array("name","status"),array($categoryName,$categoryStatus),"ss");
    $existingCategory = $allCategory[0];

    // $category = getCategory($conn," id = ?   ",array("id"),array($id),"s");    
    $category = getCategory($conn," WHERE id = ? ",array("id"),array($id),"s");    

    if(!$existingCategory)
    {
        // if(!$category)
        if($category)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($categoryName)
            {
                array_push($tableName,"name");
                array_push($tableValue,$categoryName);
                $stringType .=  "s";
            }
            array_push($tableValue,$id);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"category"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 3;
                header('Location: ../category.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 3;
                header('Location: ../category.php?type=2');
            }
        }
        else
        {
            $_SESSION['messageType'] = 3;
            header('Location: ../category.php?type=3');
        }
    }
    else
    {
        $_SESSION['messageType'] = 3;
        header('Location: ../category.php?type=4');
    }
}
else 
{
    header('Location: ../index.php');
}
?>
