<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Games.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/BetStatus.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$curl = curl_init();

//******************************************************************************
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/fifa/matches/past?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
//******************************************************************************

$betDetails = getBetstatus($conn, "WHERE result = 'PENDING'");

if ($betDetails) {
  for ($i=0; $i <count($betDetails) ; $i++) {
    $gameResult = null;
    $tradeUid = $betDetails[$i]->getTradeUid();
    $gameId = $betDetails[$i]->getGameId();
    $matches = $betDetails[$i]->getMatches();
    $teamSelected = $betDetails[$i]->getTeam();
    $betAmount = $betDetails[$i]->getAmount();
    $uid = $betDetails[$i]->getUid();

    $userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");
    $currentCredit = $userDetails[0]->getCredit();

    if ($exchangeData) {
      $j=0;
      for ($j=0; $j <count($exchangeData) ; $j++) {
        if ($matches == $exchangeData[$j]['slug']) {
          $teamWin = $exchangeData[$j]['winner']['name'];
          if ($teamWin) {
            if ($teamSelected == $teamWin) {
              $gameResult = "WIN";
              $totalCredit = $currentCredit + ($betAmount * 2);
            }else {
              $gameResult = "LOSE";
              $totalCredit = $currentCredit;
            }
          }else {
            $gameResult = "REMATCH";
            $totalCredit = $currentCredit + $betAmount;
          }

        }
      }
    }else {
      echo "API Got Problem";
    }

    echo $gameResult."<br>";
    echo $teamSelected."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($gameResult)
    {
        array_push($tableName,"result");
        array_push($tableValue,$gameResult);
        $stringType .=  "s";
    }
    array_push($tableValue,$tradeUid);
    $stringType .=  "s";
    $updateResult = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
    if($updateResult){

      echo "sucess update."."<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($totalCredit)
      {
          array_push($tableName,"credit");
          array_push($tableValue,$totalCredit);
          $stringType .=  "s";
      }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $updateCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($updateCredit){
        echo "Success ALL"."<br>";
      }
    }else {
      echo "cant Update"."<br>";
    }
  }
}else {
  echo "<h1>No Pending Result<h1>"."<br>";
}


//******************************************************************************
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/past?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
//******************************************************************************

$betDetails = getBetstatus($conn, "WHERE result = 'PENDING'");

if ($betDetails) {
  for ($i=0; $i <count($betDetails) ; $i++) {
    $gameResult = null;
    $tradeUid = $betDetails[$i]->getTradeUid();
    $gameId = $betDetails[$i]->getGameId();
    $matches = $betDetails[$i]->getMatches();
    $teamSelected = $betDetails[$i]->getTeam();
    $betAmount = $betDetails[$i]->getAmount();
    $uid = $betDetails[$i]->getUid();

    $userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");
    $currentCredit = $userDetails[0]->getCredit();

    if ($exchangeData) {
      $j=0;
      for ($j=0; $j <count($exchangeData) ; $j++) {
        if ($matches == $exchangeData[$j]['slug']) {
          $teamWin = $exchangeData[$j]['winner']['name'];
          if ($teamWin) {
            if ($teamSelected == $teamWin) {
              $gameResult = "WIN";
              $totalCredit = $currentCredit + ($betAmount * 2);
            }else {
              $gameResult = "LOSE";
              $totalCredit = $currentCredit;
            }
          }else {
            $gameResult = "REMATCH";
            $totalCredit = $currentCredit + $betAmount;
          }

        }
      }
    }else {
      echo "API Got Problem";
    }

    echo $gameResult."<br>";
    echo $teamSelected."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($gameResult)
    {
        array_push($tableName,"result");
        array_push($tableValue,$gameResult);
        $stringType .=  "s";
    }
    array_push($tableValue,$tradeUid);
    $stringType .=  "s";
    $updateResult = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
    if($updateResult){

      echo "sucess update."."<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($totalCredit)
      {
          array_push($tableName,"credit");
          array_push($tableValue,$totalCredit);
          $stringType .=  "s";
      }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $updateCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($updateCredit){
        echo "Success ALL"."<br>";
      }
    }else {
      echo "cant Update"."<br>";
    }
  }
}else {
  echo "<h1>No Pending Result<h1>"."<br>";
}

//******************************************************************************
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/lol/matches/past?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
//******************************************************************************

$betDetails = getBetstatus($conn, "WHERE result = 'PENDING'");

if ($betDetails) {
  for ($i=0; $i <count($betDetails) ; $i++) {
    $gameResult = null;
    $tradeUid = $betDetails[$i]->getTradeUid();
    $gameId = $betDetails[$i]->getGameId();
    $matches = $betDetails[$i]->getMatches();
    $teamSelected = $betDetails[$i]->getTeam();
    $betAmount = $betDetails[$i]->getAmount();
    $uid = $betDetails[$i]->getUid();

    $userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");
    $currentCredit = $userDetails[0]->getCredit();

    if ($exchangeData) {
      $j=0;
      for ($j=0; $j <count($exchangeData) ; $j++) {
        if ($matches == $exchangeData[$j]['slug']) {
          $teamWin = $exchangeData[$j]['winner']['name'];
          if ($teamWin) {
            if ($teamSelected == $teamWin) {
              $gameResult = "WIN";
              $totalCredit = $currentCredit + ($betAmount * 2);
            }else {
              $gameResult = "LOSE";
              $totalCredit = $currentCredit;
            }
          }else {
            $gameResult = "REMATCH";
            $totalCredit = $currentCredit + $betAmount;
          }

        }
      }
    }else {
      echo "API Got Problem";
    }

    echo $gameResult."<br>";
    echo $teamSelected."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($gameResult)
    {
        array_push($tableName,"result");
        array_push($tableValue,$gameResult);
        $stringType .=  "s";
    }
    array_push($tableValue,$tradeUid);
    $stringType .=  "s";
    $updateResult = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
    if($updateResult){

      echo "sucess update."."<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($totalCredit)
      {
          array_push($tableName,"credit");
          array_push($tableValue,$totalCredit);
          $stringType .=  "s";
      }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $updateCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($updateCredit){
        echo "Success ALL"."<br>";
      }
    }else {
      echo "cant Update"."<br>";
    }
  }
}else {
  echo "<h1>No Pending Result<h1>"."<br>";
}

//******************************************************************************
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/csgo/matches/past?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
//******************************************************************************

$betDetails = getBetstatus($conn, "WHERE result = 'PENDING'");

if ($betDetails) {
  for ($i=0; $i <count($betDetails) ; $i++) {
    $gameResult = null;
    $tradeUid = $betDetails[$i]->getTradeUid();
    $gameId = $betDetails[$i]->getGameId();
    $matches = $betDetails[$i]->getMatches();
    $teamSelected = $betDetails[$i]->getTeam();
    $betAmount = $betDetails[$i]->getAmount();
    $uid = $betDetails[$i]->getUid();

    $userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");
    $currentCredit = $userDetails[0]->getCredit();

    if ($exchangeData) {
      $j=0;
      for ($j=0; $j <count($exchangeData) ; $j++) {
        if ($matches == $exchangeData[$j]['slug']) {
          $teamWin = $exchangeData[$j]['winner']['name'];
          if ($teamWin) {
            if ($teamSelected == $teamWin) {
              $gameResult = "WIN";
              $totalCredit = $currentCredit + ($betAmount * 2);
            }else {
              $gameResult = "LOSE";
              $totalCredit = $currentCredit;
            }
          }else {
            $gameResult = "REMATCH";
            $totalCredit = $currentCredit + $betAmount;
          }

        }
      }
    }else {
      echo "API Got Problem";
    }

    echo $gameResult."<br>";
    echo $teamSelected."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($gameResult)
    {
        array_push($tableName,"result");
        array_push($tableValue,$gameResult);
        $stringType .=  "s";
    }
    array_push($tableValue,$tradeUid);
    $stringType .=  "s";
    $updateResult = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
    if($updateResult){

      echo "sucess update."."<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($totalCredit)
      {
          array_push($tableName,"credit");
          array_push($tableValue,$totalCredit);
          $stringType .=  "s";
      }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $updateCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($updateCredit){
        echo "Success ALL"."<br>";
      }
    }else {
      echo "cant Update"."<br>";
    }
  }
}else {
  echo "<h1>No Pending Result<h1>"."<br>";
}

//******************************************************************************
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/ow/matches/past?token=VXlL1KxIq_cvajHYI5ZW4YL3uPVZ3hVRPdog-Bj714jDYeAFIJQ&fbclid=IwAR35DLQX1U-Bcg77ZDRlm7hRxmSS-VMV60F2khuMsXieId5Tf-rNL8myzTQ");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
//******************************************************************************

$betDetails = getBetstatus($conn, "WHERE result = 'PENDING'");

if ($betDetails) {
  for ($i=0; $i <count($betDetails) ; $i++) {
    $gameResult = null;
    $tradeUid = $betDetails[$i]->getTradeUid();
    $gameId = $betDetails[$i]->getGameId();
    $matches = $betDetails[$i]->getMatches();
    $teamSelected = $betDetails[$i]->getTeam();
    $betAmount = $betDetails[$i]->getAmount();
    $uid = $betDetails[$i]->getUid();

    $userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");
    $currentCredit = $userDetails[0]->getCredit();

    if ($exchangeData) {
      $j=0;
      for ($j=0; $j <count($exchangeData) ; $j++) {
        if ($matches == $exchangeData[$j]['slug']) {
          $teamWin = $exchangeData[$j]['winner']['name'];
          if ($teamWin) {
            if ($teamSelected == $teamWin) {
              $gameResult = "WIN";
              $totalCredit = $currentCredit + ($betAmount * 2);
            }else {
              $gameResult = "LOSE";
              $totalCredit = $currentCredit;
            }
          }else {
            $gameResult = "REMATCH";
            $totalCredit = $currentCredit + $betAmount;
          }

        }
      }
    }else {
      echo "API Got Problem";
    }

    echo $gameResult."<br>";
    echo $teamSelected."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($gameResult)
    {
        array_push($tableName,"result");
        array_push($tableValue,$gameResult);
        $stringType .=  "s";
    }
    array_push($tableValue,$tradeUid);
    $stringType .=  "s";
    $updateResult = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
    if($updateResult){

      echo "sucess update."."<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($totalCredit)
      {
          array_push($tableName,"credit");
          array_push($tableValue,$totalCredit);
          $stringType .=  "s";
      }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $updateCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($updateCredit){
        echo "Success ALL"."<br>";
      }
    }else {
      echo "cant Update"."<br>";
    }
  }
}else {
  echo "<h1>No Pending Result<h1>"."<br>";
}
 ?>
