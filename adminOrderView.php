<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
// require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminOrderView.php" />
<meta property="og:title" content="Order Details | Pingola" />
<title>Order Details | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminOrderView.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
<h1 class="line-header margin-bottom50">Order Details</h1>

    <div class="border-separation">
        <div class="clear"></div>
        <!-- <form method="POST" action="utilities/editSellerFunction.php" enctype="multipart/form-data"> -->
        <!-- <form method="POST" action="#"> -->
        <form method="POST" id="paymentVerifiedForm" onsubmit="doPreview(this.submited); return false;">
        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                // echo $_POST['order_id'];
                // $ordersDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                $ordersDetails = getOrders($conn,"WHERE order_id = ? ORDER BY date_created DESC LIMIT 1 ", array("order_id") ,array($_POST['order_id']),"s");
                $orderId = $ordersDetails[0]->getId();

                // $productOrder = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($orderId),"s");
                $productOrder = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
            ?>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Order ID: <b>#<?php echo $orderId;?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Purchaser Name: <b><?php echo $ordersDetails[0]->getName();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Contact: <b><?php echo $ordersDetails[0]->getContactNo();?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Address: <b><?php echo $ordersDetails[0]->getAddressLine1();?></b></p>
                </div> 

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Payment References: <b><?php echo $ordersDetails[0]->getPaymentBankReference();?></b></p>
                </div> 
                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Order Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                </div> 
                <div class="clear"></div>

                <!-- <div class="width100 scroll-div border-separation"> -->
                <div class="overflow-scroll-div margin-top30 same-padding-tdh margin-bottom20">
                    <table class="table-css">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($productOrder)
                                {
                                    
                                    for($cnt = 0;$cnt < count($productOrder) ;$cnt++)
                                    {
                                    ?>
                                        
                                        <tr class="link-to-details">
                                            <td><?php echo ($cnt+1)?></td>
                                            <td><?php echo $productOrder[$cnt]->getProductName();?></td>
                                            <td><?php echo $productOrder[$cnt]->getQuantity();?></td>
                                            <td>RM<?php echo $productOrder[$cnt]->getTotalPrice();?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                            ?>                                 
                        </tbody>
                    </table>
                </div>

            <?php
            }
            ?>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Shipping Method</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Shipping Method"  name="shipping_method" id="shipping_method">
        </div>        
        
 
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Tracking Number</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Tracking Number" name="tracking_number" id="tracking_number" >      
        </div>

        <div class="clear"></div>

<!--        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Issue Date*</p>
            <input class="input-name clean input-textarea admin-input" type="date" name="delivered_on" id="delivered_on" required>
        </div>        
        
        <div class="clear"></div>
-->

        <input class="input-name clean input-textarea admin-input date-input" type="hidden" id="order_id" name="order_id" value="<?php echo $_POST['order_id'];?>">  
        
        <div class="width100 overflow text-center margin-top30"> 
                     
            <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="ACCEPTED" class=" green-button white-text clean2 edit-1-btn second-button-margin" >
            <div class="clear"></div> 
			<input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="red-btn white-text clean2 edit-1-btn margin-auto bottom-reject">
        </div>

        </form>
        
	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'ACCEPTED':
                form=document.getElementById('paymentVerifiedForm');
                // form.action='shippingRefund.php';
                form.action='utilities/adminOrdersAcceptedFunction.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/adminOrdersRejectedFunction.php';
                form.submit();
            break;
        }

    }
</script>

</body>
</html>