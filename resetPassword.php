<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = null;
$userRows = null;

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/resetPassword.php" />
<meta property="og:title" content="Reset Password | Pingola" />
<title>Reset Password | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/resetPassword.php" />
<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    

    <h1 class="line-header margin-bottom50">Reset Password</h1>

        <form method="POST" action="utilities/resetPasswordFunction.php">

            <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">

            <div class="width100">
                <p class="input-top-p">Verify Code</p>
                <input class="input-name clean" type="text" placeholder="Verify Code" id="verify_code" name="verify_code" required>	
            </div>

            <div class="clear"></div>
            
            <div class="width100">
                <p class="input-top-p">New Password</p>
                <input class="input-name clean" type="password" placeholder="New Password" id="new_password" name="new_password" required>	
            </div>
            
            <div class="clear"></div>

            <div class="width100">
                <p class="input-top-p">Retype New Password</p>
                <input class="input-name clean" type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>	
            </div>
            
            <div class="clear"></div>
            
            <button class="blue-button white-text width100 clean register-button"  name="submit">Submit</button>

        </form>

    </div> 

</div>

<?php include 'js.php'; ?>

</body>
</html>