<?php

require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userSMSDetails = getMessage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['message_uid']),"s");

if($userSMSDetails)
{   
    for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
    {
    ?>
            <div class="user-chat-bubble">

            <?php 
                $productQnA = $userSMSDetails[$cnt]->getReplyThree();
                if($productQnA != '')
                {
                ?>
                    <p class="username-date"><b>About : <?php echo $userSMSDetails[$cnt]->getReplyThree();?></b></p>
                <?php
                }
            ?>

                <p class="chat-bubble">
                    <?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>
                </p>

                <p class="chat-bubble">
                    <?php echo $userSMSDetails[$cnt]->getReplySMS();?>
                </p>
    
            </div>
    <?php
    }
    ?>

<?php
}
?>