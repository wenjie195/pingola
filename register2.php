<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/register.php" />
<meta property="og:title" content="Register | Pingola" />
<title>Register | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/register.php" />
<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="menu-distance width100 same-padding div1 grey-bg min-height register-bg">

    

    <h1 class="line-header margin-bottom50">Register Games</h1>

        <form method="POST" action="utilities/registerGameFunction.php">
            
            <div class="dual-input">
                <p class="input-top-p">Title</p>
                <input class="input-name clean" type="text" placeholder="Title" id="register_username" name="register_username" required>
            </div>
            
            <!-- <div class="dual-input second-dual-input">
                <p class="input-top-p">Nickname</p>
                <input class="input-name clean" type="text" placeholder="Nickname" id="register_username" name="register_username" required>	
            </div> -->
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Team One</p>
                <input class="input-name clean" type="text" placeholder="Team One" id="register_email" name="register_email" required>	
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Team Two</p>
                <input class="input-name clean" type="text" placeholder="Team Two" id="register_phone" name="register_phone" required>	
            </div>   
                     
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Value One</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="text" placeholder="Value One" id="register_password" name="register_password" required>        
                </div>
            </div>
            
            <div class="dual-input second-dual-input">

            <p class="input-top-p">Value Two</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="text" placeholder="Value One" id="register_retype_password" name="register_retype_password" required>         
                </div>
            </div>
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Result</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="text" placeholder="Result" id="register_result" name="register_result" required>        
                </div>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Date</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="datetime-local" id="register_date" name="register_date" required>        
                    <!-- <input class="input-name clean password-input" type="datetime" id="register_date" name="register_date" required>     -->
                </div>
            </div>

            <!-- <div class="dual-input second-dual-input">
                <p class="input-top-p">Time</p>
                <div class="password-input-div">
                    <input class="input-name clean password-input" type="time" id="register_time" name="register_time" required>         
                </div>
            </div> -->

            <div class="clear"></div>

            <button class="blue-button white-text width100 clean register-button"  name="register">Register</button>

            <div class="clear"></div>

        </form>

    </div> 

</div>

<?php include 'js.php'; ?>

</body>
</html>