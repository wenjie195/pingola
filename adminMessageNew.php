<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $messageDetails = getMessage($conn, " WHERE admin_status = 'GET' ORDER BY date_created DESC  ");
// $messageDetails = getUser($conn);
// $messageDetails = getUser($conn, " WHERE message != ' ' ");
$messageDetails = getUser($conn, " WHERE message = 'YES' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminMessageNew.php" />
<meta property="og:title" content="Chat | Pingola" />
<title>Chat | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminMessageNew.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding  black-bg ping-menu-distance ping-min-height" id="myTable">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="green-text h1-title2">New Messages | <a href="adminMessageAll.php" class="green-a opacity-hover2">All Message</a> </h1>
           
        </div>
    </div>

    <div class="clear"></div>

	<div class="overflow-scroll-div margin-top30 same-padding-tdh">
    	<table class="table-css">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($messageDetails)
                    {
                        
                        for($cnt = 0;$cnt < count($messageDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $messageDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $messageDetails[$cnt]->getDateCreated();?></td>
                                <td>
                                    <!-- <form method="POST" action="adminMessageView.php" class="hover1"> -->
                                    <!-- <a href="adminMessageView.php"> -->
                                    <a href='adminMessageView.php?id=<?php echo $messageDetails[$cnt]->getUid();?>'>
                                        <button class="clean transparent-button pointer" type="submit" name="message_uid" value="<?php echo $messageDetails[$cnt]->getUid();?>">
                                             <img src="img/edit.png" class="edit-icon1" alt="Reply" title="Reply">
                                        
                                        </button>
                                    </a>
                                    <!-- </form> -->
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['url']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete user !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>