<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminProductVariation.php" />
<meta property="og:title" content="Product Variation | Pingola" />
<title>Product Variation | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminProductVariation.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height" id="myTable">

    <?php
    if(isset($_POST['product_uid']))
    {
        $conn = connDB();
        // echo $_POST['product_uid'];
        $variationDetails = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_POST['product_uid']),"s");
    ?>

        <div class="width100">
            <div class="left-h1-div">
                <h1 class="green-text h1-title">All Variation</h1>
                <div class="green-border"></div>
            </div>
            <div class="mid-search-div">
            <form>
                <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                    <button class="search-btn mid-search-btn clean">
                        <img src="img/search.png" class="visible-img  mid-search-img opacity-hover" alt="Search" title="Search">
                        
                    </button>
                </form>
            </div>
            <div class="right-add-div">
                <form action="addProductVariationAddOn.php" method="POST" class="green-button white-text puppy-button">
                    <button class="clean hover1 img-btn transparent-button pointer" type="submit" name="variation_uid" value="<?php echo $_POST['product_uid'];?>">
                        Add Variation
                    </button>
                </form>
            </div>      
        </div>

        
        <div class="clear"></div>
        
        <div class="overflow-scroll-div margin-top30  same-padding-tdh">
            <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Price (RM)</th>    
                        <th>Edit</th>        
                        <th>Action</th>   
                    </tr>
                </thead>
                <tbody>
                <?php
                if($variationDetails)
                    {
                        for($cnt = 0;$cnt < count($variationDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $variationDetails[$cnt]->getName();?></td>
                                <td><?php echo $variationDetails[$cnt]->getPrice();?></td>

                                <td>
                                    <form action="adminProductVariationEdit.php" method="POST" class="hover1" target="_blank">
                                        <button class="clean img-btn transparent-button pointer" type="submit" name="variation_uid" value="<?php echo $variationDetails[$cnt]->getUid();?>">
                                            <img src="img/edit.png" class="edit-icon1 " alt="Edit" title="Edit">
                                           
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="utilities/adminVariationDeleteFunction.php" method="POST" class="hover1">
                                        <button class="clean img-btn transparent-button pointer" type="submit" name="variation_uid" value="<?php echo $variationDetails[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1" alt="Delete" title="Delete">
                                            
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
            </table>
        </div>

    <?php
    }
    ?>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['product_uid']);unset($_SESSION['image']); ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>