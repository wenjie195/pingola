<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$todayDate = date('Y-m-d');
$yesterdayDate = date('Y-m-d',strtotime($todayDate."- 1 days"));
// echo $yesterdayDate;

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/pastMatches.php" />
<meta property="og:title" content="Past Matches | Pingola" />
<title>Past Matches | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/pastMatches.php" />
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">
  <h1 class="line-header margin-bottom50">Match Results</h1>

<?php

$No = 0;

 ?>
 <div id="matches" class="overflow-scroll-div">
     <?php
     // create & initialize a curl session
     $curl = curl_init();

     // set our url with curl_setopt()
     curl_setopt($curl, CURLOPT_URL, "https://api.pandascore.co/dota2/matches/past?token=F7U9j7gsyiDz8BzpTePBShxkAJ2hGrEeZVrP24Dk0OwZlq7s40U");

     // return the transfer as a string, also with setopt()
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

     // curl_exec() executes the started curl session
     // $output contains the output string
     $output = curl_exec($curl);

     // close curl resource to free up system resources
     // (deletes the variable made by curl_init)
     // curl_close($curl);

     $exchangeData = json_decode($output, true);
     ?>
         <table class="table-css">
             <thead>
                 <tr>
                     <th class="th" style="text-align: center;">NO.</th>
                     <th class="th" style="text-align: center;">MATCH TYPE</th>
                     <th class="th" style="text-align: center;">TOURNAMENT LOGO</th>
                     <th class="th" style="text-align: center;">TOURNAMENT NAME</th>
                     <th class="th" style="text-align: center;">STATUS</th>
                     <th class="th" style="text-align: center;">MATCH</th>
                     <th class="th" style="text-align: center;">RESULT</th>
                     <th class="th" style="text-align: center;">SCHEDULED DATE</th>
                     <!-- <th>INVOICE</th> -->
                 </tr>
             </thead>
             <tbody>
                 <?php
                 // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                 // {

                 if ($exchangeData) {
                   for ($i=0; $i <count($exchangeData) ; $i++) {
                     $scoreTotalTeam0 = 0;
                     $scoreTotalTeam1 = 0;
                     if (date('Y-m-d',strtotime($exchangeData[$i]['scheduled_at'])) == $yesterdayDate) {
                       $No++;
                       ?>
                       <tr>
                         <!-- <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php //echo uniqid() ?></td> -->
                        <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $No ?></td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['match_type'] == 'best_of') {
                         echo "Best of ".$exchangeData[$i]['number_of_games'];
                       } ?></td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px">
                         <?php if ($exchangeData[$i]['league']['image_url']) {
                           ?><img style="width: 120px;height: 100px" src="<?php echo $exchangeData[$i]['league']['image_url'] ?>" alt=""><?php
                         }else {
                           ?><img style="width: 120px;height: 100px" src="img/dota_logo.png" alt=""><?php
                         } ?>
                         </td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo $exchangeData[$i]['league']['name'] ?></td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php if ($exchangeData[$i]['status'] == 'not_started') {
                         echo "Not Started Yet";
                       }elseif ($exchangeData[$i]['status'] == 'running') {
                         echo "Running";
                       } ?></td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo str_replace(":",":<br>",$exchangeData[$i]['name']) ?></td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php
                        $scoreTotalTeam0 = $exchangeData[$i]['results'][0]['score'];
                        $scoreTotalTeam1 = $exchangeData[$i]['results'][1]['score'];
                        echo $scoreTotalTeam0." - ".$scoreTotalTeam1;
                        ?>
                      </td>
                       <td class="td" style="text-align: center;vertical-align: middle;height: 100px"><?php echo date('d/m/Y h:i a',strtotime($exchangeData[$i]['scheduled_at'])) ?></td>
                     </tr>
                       <?php
                     }
                   }
                 }

                 //}
                 ?>
             </tbody>
         </table>
</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>
<script>
  setInterval(function(){
    $("#matches").load(location.href + " #matches");
  },60000);
</script>
