<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$betRows = getBetstatus($conn, "WHERE uid =? ORDER BY date_created DESC",array("uid"),array($uid),"s");
$userBetDetails = $betRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/betHistory.php" />
<meta property="og:title" content="Bet History | Pingola" />
<title>Bet History | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/betHistory.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

	<div class="width100 overflow text-center">

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto vip">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>

                </a>
            <?php
            }
            else
            {
            ?>
            	<a href="editProfilePic.php">
                <div class="left-profile-div margin-auto">
                    <?php
                        $image = $userDetails->getProfileImg();
                        if($image != "")
                        {
                        ?>
                            <img src="profilePicture/<?php echo $userDetails->getProfileImg();?>" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
                        <?php
                        }
                    ?>
                    <img src="img/update.png" class="update-profile-png" alt="Update Profile Picture" title="Update Profile Picture">
        		</div>
                </a>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>
		<p class="username-p">My PingCash: <?php echo $userDetails->getCredit();?></p>
        <a href="editProfile.php"><div class="blue-button goback-btn dual-button">Edit Profile</div></a>
        <!-- <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-topup">Top Up</div> -->
        <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-topup">Recharge</div>
		<div class="clear"></div>

        <?php
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {}
            else
            {
            ?>
            	<div class="width100 overflow text-center">
                	<div class="blue-button vip-upgrade open-upgrade">Upgrade to <img src="img/vip.png" class="vip-img" alt="Upgrade to VIP" title="Upgrade to VIP"></div>
                </div>
            <?php
            }
        ?>

        <!-- <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-upgrade">VIP Upgrade</div> -->
    </div>

    <div class="clear"></div>

    <div class="width100 overflow margin-top30" id="gametab">
        <h1 class="line-header margin-bottom50">Bet History</h1>
    </div>

    <div class="overflow-scroll-div margin-top30">
        <table class="glory-board">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Match Details</th>
                    <th>Bet Amount</th>
                    <th>Result</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($betRows)
                {
                    for($cnt = 0;$cnt < count($betRows) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $betRows[$cnt]->getMatches();?></td>
                            <td><?php echo $betRows[$cnt]->getAmount();?></td>
                            <td><?php echo $betRows[$cnt]->getResult();?></td>
                            <td><?php echo date("d-m-Y",strtotime($betRows[$cnt]->getDateCreated()));?></td>
                        </tr>
                    <?php
                    }
                }
                ?> 

            </tbody>
        </table>
        </div>

    <!-- Upgrade VIP Modal Old-->
    <div id="upgrade-modal" class="modal-css">
        <div class="modal-content-css register-modal-content">
            <span class="close-css close-upgrade">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Upgrade To VIP</h2>
            <!-- <form method="POST" action="utilities/topUpFunction.php"> -->
            <form method="POST" action="utilities/upgradeVipFunction.php">
                <div class="width100">
                <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash?</p>
                    <select class="clean input-name admin-input" type="text" name="update_vip" id="update_vip" required>
                        <option value="No" name="No" selected>No</option>
                        <option value="Yes" name="Yes">Yes</option>
                    </select>
                </div>

                <button class="blue-button white-text width100 clean register-button"  name="submit">Confirm</button>
            </form>
        </div>
    </div>

    <!-- Top Up Modal New-->
    <div id="topup-modal" class="modal-css">
        <div class="modal-content-css register-modal-content topup-modal-content">
            <span class="close-css close-topup">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Recharge</h2>
            <form method="POST" action="#" enctype="multipart/form-data">

                <div class="qr-div">
                    <img src="img/blockchain-qr.png" class="qr-png" alt="Recharge Details" title="Recharge Details">
                    <p class="qr-p">Bitcoin Wallet Address (Scan to Pay)</p>
                    <div class="invitation-link-container shadow-white-div black-text copy-btc">
                        <input type="hidden" id="copyBitcoin" value="1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM">
                        <a id="btcaddress" href="1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM" class="btcaddress">
                            1D57KiXJU1ztEJppUAfjrGxkReK6a48XhM
                        </a>
                    </div>
                    <div class="clean blue-btn-hover copy-btn" id="copy-bitcoin-link">COPY</div>
                </div>

                <div class="clear"></div>

            </form>
        </div>
    </div>

</div>


<?php include 'js.php'; ?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
    //   $("#copy-address").click(function(){
    //       var textArea = document.createElement("textarea");
    //       textArea.value = $('#btcaddress').val();
    //       document.body.appendChild(textArea);
    //       textArea.select();
    //       document.execCommand("Copy");
    //       textArea.remove();
    //       putNoticeJavascript("Copied!! ","");
    //   });
</script>

<script>
    $("#copy-bitcoin-link").click(function()
    {
        var textArea = document.createElement("textarea");
        textArea.value = $('#copyBitcoin').val();
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        putNoticeJavascript("Link Copied!! ","");
    });
</script>
<script>
$(document).ready(function(){
  $("#dota2Tab").on('click',function(){
    $.ajax({
      url: 'profile_dota.php',
      success:function(data){
        $("#gameTab").html(data);
      }
    });
  });
    $("#lolTab").on('click',function(){
      $.ajax({
        url: 'profile_lol.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $("#csgoTab").on('click',function(){
      $.ajax({
        url: 'profile_csgo.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $("#owTab").on('click',function(){
      $.ajax({
        url: 'profile_ow.php',
        success:function(data){
          $("#gameTab").html(data);
        }
      });
    });
    $(document).ajaxStart(function(){
      $("#gameTab").html("Loading...");
    });
});
</script>

</body>
</html>