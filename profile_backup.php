<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Games.php';
require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$visitorDetails = getPageview($conn, "WHERE refer_uid =?",array("refer_uid"),array($uid),"s");
// $visitorDetails = $visitorRows[0];

$gamesDetails = getGames($conn," Order By date_created ASC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/profile.php" />
<meta property="og:title" content="Profile | Pingola" />
<title>Profile | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/profile.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

	<div class="width100 overflow text-center">

        <?php 
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <div class="left-profile-div margin-auto vip">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
            else
            {
            ?>
                <div class="left-profile-div margin-auto">
        			<img src="img/profile.png" class="profile-png" alt="<?php echo $userDetails->getUsername();?>" title="<?php echo $userDetails->getUsername();?>">
        		</div>
            <?php
            }
        ?>

        <div class="clear"></div>

        <?php 
            $vip = $userDetails->getVipStatus();
            if($vip == 'Yes')
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?> <img src="img/vip.png" alt="VIP" title="VIP" class="vip-badge"></p>
            <?php
            }
            else
            {
            ?>
                <p class="username-p"><?php echo $userDetails->getUsername();?></p>
            <?php
            }
        ?>

        <a href="editProfile.php"><div class="blue-button goback-btn dual-button">Edit Profile</div></a>
        <div class="blue-button goback-btn dual-button second-dual-button blue-btn-hover open-topup">Top Up</div>
    </div>

    <div class="clear"></div>
    
    <div class="width100 overflow margin-top30">
    	<h1 class="line-header margin-bottom50">Invitation Link</h1>
        <div class="link-container">

            <form method="POST" action="utilities/linkClickFunction.php">
                <?php $actual_link = "https://$_SERVER[HTTP_HOST]"; ?>
                <!-- <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                        <?php echo "https://".$_SERVER['HTTP_HOST']."/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div> -->

                <!-- <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/testing/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/testing/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                    <?php echo "https://".$_SERVER['HTTP_HOST']."/testing/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div> -->

                <input type="hidden" id="linkCopy" value="<?php echo "https://".$_SERVER['HTTP_HOST']."/pingola/register.php?referrerUID=".$_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="<?php echo "/pingola/register.php?referrerUID=".$_SESSION['uid']?>" class="invitation-link-a opacity-hover text-overflow">
                    <?php echo "https://".$_SERVER['HTTP_HOST']."/pingola/register.php?referrerUID=".$_SESSION['uid']?>
                    </a>
                </div>

                <button class="clean blue-btn-hover copy-btn" id="copy-referral-link">COPY</button>   
            </form>

        </div> 	
    	<div class="clear"></div>
    </div>

    <div class="width100 overflow margin-top30">
    	<div class="three-div-width1 box-css">
        	<img src="img/link-clicked.png" alt="Referral Link Clicked" title="Referral Link Clicked" class="box-img">
			<p class="box-p">Referral Link Clicked</p> 
            <p class="box-value">
                <?php 
                   if($visitorDetails)
                   {   
                       echo $totalVisitor = count($visitorDetails);
                   }
                   else
                   {   echo $totalVisitor = 0;   }
                ?>
            </p>       
        </div>
    	<div class="three-div-width1 box-css mid-three-width1">
        	<img src="img/referral.png" alt="Successful Referrals" title="Successful Referrals" class="box-img">
			<p class="box-p">Successful Referrals</p> 
            <p class="box-value">
                <?php 
                    $referNo = $userDetails->getReferNo();
                    if($referNo == '')
                    {
                        echo "0";
                    }
                    else
                    {
                        echo $referNo;
                    }
                ?>
            </p>           
        </div> 
    	<div class="three-div-width1 box-css">
        	<img src="img/pingcash.png" alt="PingCash" title="PingCash" class="box-img">
			<p class="box-p">PingCash</p> 
            <p class="box-value"><?php echo $userDetails->getCredit();?></p>              
        </div>           
    </div>

    <div class="clear"></div>

    <div class="width100 overflow margin-top30">
        <h1 class="line-header margin-bottom50">Games Available</h1>

        <div id="divCounter"></div>

            <?php
            if($gamesDetails)
            {
                for($cnt = 0;$cnt < count($gamesDetails) ;$cnt++)
                {
                ?>
                <form method="POST" action="placeWage.php" class="hover1">
                    <div class="bluered-gradient-bg result-color-div width100">

                        <p class="tournament-p">
                            <?php echo $gamesDetails[$cnt]->getTitle();?> (<?php echo $time ;?>)
                        </p>
                        <div class="div1a">
                            <img src="img/scabbard.png" alt="SCABBARD" title="SCABBARD" class="team-logo">
                            <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamOne();?></p>
                        </div>
                        <div class="div2a">
                            <div class="emerald-bg rate-div rate-div1"><?php echo $gamesDetails[$cnt]->getValueOne();?></div>
                        </div>

                        <div class="div4a">
                            <img src="img/red-arrow.png" alt="arrow" class="red-arrow arrow-img">
                        </div>
                        <div class="div5a">
                            <img src="img/dota2.png" alt="Dota 2" title="Dota 2" class="bet-game-logo team-logo">
                            <p class="bet-team-name">Dota 2</p>
                        </div>  
                        <div class="div6a">
                            <img src="img/blue-arrow.png" alt="arrow" class="blue-arrow arrow-img">
                        </div>   

                        <div class="div8a">
                            <div class="red-bg rate-div rate-div2"><?php echo $gamesDetails[$cnt]->getValueTwo();?></div>
                        </div> 
                        <div class="div9a">
                            <img src="img/silent-night.png" alt="SILENT NIGHT" title="SILENT NIGHT" class="team-logo">
                            <p class="bet-team-name"><?php echo $gamesDetails[$cnt]->getTeamTwo();?></p>
                        </div>  

                    </div>

					<div class="width100 text-center margin-bottom50">
                        <button class="blue-button clean login-to-bet place-bet-button" type="submit" name="games_uid" value="<?php echo $gamesDetails[$cnt]->getUid();?>">
                            Place Your Wage
                        </button>
                    <div class="width100 text-center margin-top10 overflow">
                        <img src="img/timer-white.png" class="timer-png"><p class="timer-p white-timer"  id="timerP"></p>
                    </div>         
                    
					</div>
                </form>
                <?php
                }
            }
            ?>  
    </div>



    <!-- Top Up Modal -->
    <div id="topup-modal" class="modal-css">
        <div class="modal-content-css register-modal-content">
            <span class="close-css close-topup">&times;</span>
            <div class="clear"></div>
            <h2 class="black-text h2-title text-center reg-title ow-margin-bottom10">Top Up</h2>
            <form method="POST" action="utilities/topUpFunction.php">
                <div class="dual-input">
                    <p class="input-top-p big-top-p long-p">How Many PingCash You Would Like to Top Up?</p>
                    <!-- <input class="input-name clean" type="number" placeholder="0 PingCash" id="cash_value" name="cash_value" required> -->
                    <input class="input-name clean" type="number" placeholder="0 PingCash" id="cash_value" name="cash_value">
                </div>

                    <?php 
                    $vip = $userDetails->getVipStatus();
                    if($vip == 'Yes')
                    {
                    ?>

                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="dual-input second-dual-input">
                        <p class="input-top-p big-top-p long-p">Purchase VIP with 200 PingCash?</p>
                            <!-- <select  class="input-name clean" type="text" name="update_vip" id="update_vip" requried> -->
                            <select class="clean input-name admin-input" type="text" name="update_vip" id="update_vip" required>
                                <option value="No" name="No" selected>No</option>
                                <option value="Yes" name="Yes">Yes</option>
                                
                            </select>
                        </div>
                    <?php
                    }
                    ?>

                <button class="blue-button white-text width100 clean register-button"  name="submit">Confirm</button>
            </form>
        </div>
    </div>
    
</div>


<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Refer Link Copied !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "NOT enough PingCash <br> Please top up !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "unable to place wage !!"; 
        }
        else if($_GET['type'] == 4)
        {
            // $messageType = "The wages is placed !!"; 
            $messageType = "Too BAD, you lose <br> Please try again later !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "unable to place wage !!"; 
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR !!"; 
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "unable to record wage !!"; 
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Congratulations, You WIN !!";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Unknown Result !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Top Up Successfully <br> Congrtulation, You are a VIP now!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Top Up Sumbitted !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "unable to top up !!"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Not enough PingCash to upgrade as VIP !! <br> Please Top Up"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "ERROR !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#divCounter").load("countDown.php");
setInterval(function()
{
    $("#divCounter").load("countDown.php");
}, 1000);
});
</script>

</body>
</html>