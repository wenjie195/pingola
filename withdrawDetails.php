<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/withdrawDetails.php" />
<meta property="og:title" content="Withdraw Details | Pingola" />
<title>Withdraw Details | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/withdrawDetails.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">



<div class="width100 same-padding div1 white-container">

    <h1 class="line-header margin-bottom50">Withdraw Details</h1>

        <form method="POST" action="utilities/editProfileFunction.php">

			<div class="dual-input">
                <p class="input-top-p">Username</p>
                <input class="input-name clean no-input" type="text" placeholder="Username" value="<?php echo $userDetails->getUsername();?>" name="edit_username"  id="edit_username" required>
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Withdraw (RM)</p>
                <input class="input-name clean no-input" type="text" placeholder="100" value="" name="" id="" required>	
            </div>
            
            <div class="clear"></div>
			<div class="dual-input">
                <p class="input-top-p">Email</p>
                <input class="input-name clean no-input" type="email" placeholder="Type Your Email" value="<?php echo $userDetails->getEmail();?>" name="edit_email" id="edit_email" required>	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Phone</p>
                <input class="input-name clean no-input" type="text" placeholder="Phone Number" value="<?php echo $userDetails->getPhoneNo();?>" name="edit_phone" id="edit_phone" required>	
            </div>            
            
            <div class="clear"></div>
			<div class="dual-input">
                <p class="input-top-p">Country</p>
                <input class="input-name clean no-input" type="text" placeholder="Malaysia" value="" name="" id="" required>	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Bank</p>
                <input class="input-name clean no-input" type="text" placeholder="Maybank" value="" name="" id="" required>	
            </div>            
            
            <div class="clear"></div>  
			<div class="dual-input">
                <p class="input-top-p">Bank Acc No.</p>
                <input class="input-name clean no-input" type="text" placeholder="15088882766" value="" name="" id="" required>	
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Bank Holder Name</p>
                <input class="input-name clean no-input" type="text" placeholder="Ng Jia Jun" value="" name="" id="" required>	
            </div>         
                        
            <div class="clear"></div>
			<div class="width100">
                <p class="input-top-p">Reference/Reason</p>
                <input class="input-name clean no-input" type="text" placeholder="Reference/Reason" value="" name="" id="" required>	
			</div>     
            <div class="clear"></div>            
			<div class="width100">
                <p class="input-top-p">Receipt</p>
                <img src="img/pakages1.jpg" class="receipt-img hover-bigger" alt="Receipt" title="Receipt">	
			</div>     

            <div class="clear"></div>

            <!-- <div class="width100 text-center margin-top10">
                <a class="open-login bottom-a">Login</a>
            </div> -->

        </form>

</div>




</body>
</html>