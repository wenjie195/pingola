<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminProductVariationEdit.php" />
<meta property="og:title" content="Edit Product Variation | Pingola" />
<title>Edit Product Variation | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminProductVariationEdit.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<h1 class="line-header margin-bottom50">Edit Product</h1>


    <div class="clear"></div>

    <div class="border-separation">
        <?php
        if(isset($_POST['variation_uid']))
        {
            $conn = connDB();
            $variationDetails = getVariation($conn,"WHERE uid = ? ", array("uid") ,array($_POST['variation_uid']),"s");
        ?>
            <form method="POST" action="utilities/adminVariationEditFunction.php" enctype="multipart/form-data">
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getName();?>" name="update_name" id="update_name" required>      
                </div>

                 <div class="clear"></div>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Price (RM)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getPrice();?>" name="update_price" id="update_price" required>      
                </div>
               
                <div class="clear"></div>

                <input type="hidden" id="variation_uid" name="variation_uid" value="<?php echo $variationDetails[0]->getUid();?>">

                <div class="clear"></div>  
                
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" name ="submit">Submit</button>
                </div>

                <div class="clear"></div>
            </form>

            <div class="width100 margin-top50">
            	<h1 class="line-header margin-bottom50">Edit Variation Photo</h1>
                
            </div>

            <div class="border-separation"></div>
            <div class="clear"></div>

            <div class="width100 overflow">
                <div class="six-box3">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $variationDetails[0]->getImage();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <!-- <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data"> -->
                    <form action="utilities/adminUpdateVariationImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 1</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <!-- <input type="hidden" id="image_value" name="image_value" value="1"> -->
                            <input type="hidden" name="variatio_uid" id="variatio_uid" value="<?php echo $variationDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            </div>

        <?php
        }
        ?>
	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>