<?php
class User {
    /* Member variables */
    var $id,$uid,$username,$email,$password,$salt,$phoneNo,$fullName,$address,$area,$postcode,$state,$nationality,$deposit,$credit,$profileImg,$subscribeStatus,
        $bankName,$bankAccountNo,$bankAccountName,$messageStatus,$vipStatus,$linkClick,$referNo,$loginType,$userType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

        /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

        /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * @param mixed $deposit
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return mixed
     */
    public function getProfileImg()
    {
        return $this->profileImg;
    }

    /**
     * @param mixed $profileImg
     */
    public function setProfileImg($profileImg)
    {
        $this->profileImg = $profileImg;
    }

    /**
     * @return mixed
     */
    public function getSubscribeStatus()
    {
        return $this->subscribeStatus;
    }

    /**
     * @param mixed $subscribeStatus
     */
    public function setSubscribeStatus($subscribeStatus)
    {
        $this->subscribeStatus = $subscribeStatus;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param mixed $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getBankAccountName()
    {
        return $this->bankAccountName;
    }

    /**
     * @param mixed $bankAccountName
     */
    public function setBankAccountName($bankAccountName)
    {
        $this->bankAccountName = $bankAccountName;
    }

    /**
     * @return mixed
     */
    public function getMessageStatus()
    {
        return $this->messageStatus;
    }

    /**
     * @param mixed $messageStatus
     */
    public function setMessageStatus($messageStatus)
    {
        $this->messageStatus = $messageStatus;
    }

    /**
     * @return mixed
     */
    public function getVipStatus()
    {
        return $this->vipStatus;
    }

    /**
     * @param mixed $vipStatus
     */
    public function setVipStatus($vipStatus)
    {
        $this->vipStatus = $vipStatus;
    }

    /**
     * @return mixed
     */
    public function getLinkClick()
    {
        return $this->linkClick;
    }

    /**
     * @param mixed $linkClick
     */
    public function setLinkClick($linkClick)
    {
        $this->linkClick = $linkClick;
    }

    /**
     * @return mixed
     */
    public function getReferNo()
    {
        return $this->referNo;
    }

    /**
     * @param mixed $referNo
     */
    public function setReferNo($referNo)
    {
        $this->referNo = $referNo;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","email","password","salt","phone_no","full_name","address","area","postcode","state","nationality","deposit","credit",
                            "profile_img","subscribe_status","bank_name","bank_account_no","bank_account_name","message","vip_status","link_click","refer_no",
                            "login_type","user_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$email,$password,$salt,$phoneNo,$fullName,$address,$area,$postcode,$state,$nationality,$deposit,$credit,$profileImg,
                            $subscribeStatus,$bankName,$bankAccountNo,$bankAccountName,$messageStatus,$vipStatus,$linkClick,$referNo,$loginType,$userType,
                            $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setPhoneNo($phoneNo);
            $user->setFullName($fullName);
            $user->setAddress($address);

            $user->setArea($area);
            $user->setPostcode($postcode);
            $user->setState($state);

            $user->setNationality($nationality);
            $user->setDeposit($deposit);
            $user->setProfileImg($profileImg);
            $user->setSubscribeStatus($subscribeStatus);
            $user->setCredit($credit);
            $user->setBankName($bankName);
            $user->setBankAccountNo($bankAccountNo);
            $user->setBankAccountName($bankAccountName);
            $user->setMessageStatus($messageStatus);
            $user->setVipStatus($vipStatus);
            $user->setLinkClick($linkClick);
            $user->setReferNo($referNo);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
