<?php
class Games {
    /* Member variables */
    var $id, $uid, $title, $teamOne, $valueOne, $teamTwo, $valueTwo, $imgOne, $imgTwo, $comImg, $comName, $winner, $status, $matchday, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTeamOne()
    {
        return $this->teamOne;
    }

    /**
     * @param mixed $teamOne
     */
    public function setTeamOne($teamOne)
    {
        $this->teamOne = $teamOne;
    }

    /**
     * @return mixed
     */
    public function getValueOne()
    {
        return $this->valueOne;
    }

    /**
     * @param mixed $valueOne
     */
    public function setValueOne($valueOne)
    {
        $this->valueOne = $valueOne;
    }

    /**
     * @return mixed
     */
    public function getTeamTwo()
    {
        return $this->teamTwo;
    }

    /**
     * @param mixed $teamTwo
     */
    public function setTeamTwo($teamTwo)
    {
        $this->teamTwo = $teamTwo;
    }

    /**
     * @return mixed
     */
    public function getValueTwo()
    {
        return $this->valueTwo;
    }

    /**
     * @param mixed $valueTwo
     */
    public function setValueTwo($valueTwo)
    {
        $this->valueTwo = $valueTwo;
    }

    /**
     * @return mixed
     */
    public function getImgOne()
    {
        return $this->imgOne;
    }

    /**
     * @param mixed $imgOne
     */
    public function setImgOne($imgOne)
    {
        $this->imgOne = $imgOne;
    }

    /**
     * @return mixed
     */
    public function getImgTwo()
    {
        return $this->imgTwo;
    }

    /**
     * @param mixed $imgTwo
     */
    public function setImgTwo($imgTwo)
    {
        $this->imgTwo = $imgTwo;
    }

    /**
     * @return mixed
     */
    public function getComImg()
    {
        return $this->comImg;
    }

    /**
     * @param mixed $comImg
     */
    public function setComImg($comImg)
    {
        $this->comImg = $comImg;
    }

    /**
     * @return mixed
     */
    public function getComName()
    {
        return $this->comName;
    }

    /**
     * @param mixed $comName
     */
    public function setComName($comName)
    {
        $this->comName = $comName;
    }

    /**
     * @return mixed
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param mixed $winner
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMatchday()
    {
        return $this->matchday;
    }

    /**
     * @param mixed $matchday
     */
    public function setMatchday($matchday)
    {
        $this->matchday = $matchday;
    }
            
    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getGames($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","title","team_one","value_one","team_two","value_two","img_one","img_two","com_img","com_name","winner","status","matchday","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"games");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id, $uid, $title, $teamOne, $valueOne, $teamTwo, $valueTwo, $imgOne, $imgTwo, $comImg, $comName, $winner, $status, $matchday, 
                            $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Games;
            $class->setId($id);
            $class->setUid($uid);
            $class->setTitle($title);
            $class->setTeamOne($teamOne);
            $class->setValueOne($valueOne);
            $class->setTeamTwo($teamTwo);
            $class->setValueTwo($valueTwo);

            $class->setImgOne($imgOne);
            $class->setImgTwo($imgTwo);
            $class->setComImg($comImg);
            $class->setComName($comName);
            $class->setWinner($winner);

            $class->setStatus($status);

            $class->setMatchday($matchday);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
