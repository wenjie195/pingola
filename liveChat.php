<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $messageDetails = getMessage($conn," ORDER BY date_created DESC LIMIT 50 ");
$messageDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC LIMIT 50 ",array("uid"),array($uid),"s");

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/liveChat.php" />
<meta property="og:title" content="Chat | Pingola" />
<title>Chat | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/liveChat.php" />
<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header.php'; ?>
<div class="width100 black-bg ping-menu-distance ping-min-height same-padding">
	<h1 class="line-header margin-bottom50">Chat With Pingola</h1>
    
	
    <div class="clear"></div>

	<div class="chat-section">
    	<div id="divLiveMessage"></div>
	</div>

    <?php
    if(isset($_GET['id']))
    {
    $conn = connDB();
    // echo $_GET['id'];
    // $productDetails = getProduct($conn,"WHERE uid = ? AND status = 'Available' ", array("uid") ,array($_GET['id']),"s");
    // $productVariation = getVariation($conn,"WHERE product_uid = ? AND status = 'Available' ", array("product_uid") ,array($_GET['id']),"s");
    ?>

            <form action="utilities/sentMessageFunction.php" method="POST">
                <div class="width100 bottom-send-div">
                    <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
                    <input class="send-message clean"  type="hidden" value="<?php echo $_GET['id'];?>" id="product_name" name="product_name" readonly>  
                    <button class="clean send-button" type="submit" name="submit">
                        SENT
                    </button>
                </div>
            </form>

    <?php
    }
    else
    {
    ?>
    
    <form action="utilities/sentMessageFunction.php" method="POST">
        <div class="width100 bottom-send-div">
            <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        
            <button class="clean send-button" type="submit" name="submit">
                SENT
            </button>
        </div>
    </form>
    
    <?php
    }
    ?>

    <!-- <form action="utilities/sentMessageFunction.php" method="POST">
        <div class="width100 bottom-send-div">
            <input class="send-message clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        
            <button class="clean send-button" type="submit" name="submit">
                SENT
            </button>
        </div>
    </form> -->

</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    setInterval(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    }, 5000);
    });
</script>

</body>
</html>