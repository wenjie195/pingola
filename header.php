<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype_level'] == 0)
    //admin
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="#" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="Pingola" title="Pingola"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div login-header">
                    <a href="adminDashboard.php" class="white-text menu-margin-right menu-item opacity-hover">Dashboard</a>
                   <div class="dropdown v-top">
                                    <a  class="white-text menu-margin-right menu-item opacity-hover">
                                                Product <img src="img/dropdown.png" class="dropdown-png">
                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                        <!-- <p class="dropdown-p"><a href="allProducts.php"  class="menu-padding dropdown-a">All Products</a></p>
                                        <p class="dropdown-p"><a href="addProduct.php"  class="menu-padding dropdown-a">Add Product</a></p>
                                        <p class="dropdown-p"><a href="category.php"  class="menu-padding dropdown-a">Category</a></p>
                                        <p class="dropdown-p"><a href="shopping.php"  class="menu-padding dropdown-a">View Shop</a></p> -->
                                        <p class="dropdown-p"><a href="adminProductsAll.php"  class="menu-padding dropdown-a">All Products</a></p>
                                        <p class="dropdown-p"><a href="adminProductsAdd.php"  class="menu-padding dropdown-a">Add Product</a></p>
                                        <p class="dropdown-p"><a href="adminCategoryAll.php"  class="menu-padding dropdown-a">Category</a></p>
                                        <p class="dropdown-p"><a href="shopping.php"  class="menu-padding dropdown-a">View Shop</a></p>
                                    </div>
                    </div> 
                   <div class="dropdown v-top">
                                    <a  class="white-text menu-margin-right menu-item opacity-hover">
                                                Pending <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                                <p class="dropdown-p"><a href="adminOrdersPending.php"  class="menu-padding dropdown-a">Orders</a></p> 
                                                <p class="dropdown-p"><a href="adminMessageNew.php"  class="menu-padding dropdown-a">Message</a></p>
                                               <p class="dropdown-p"><a href="adminReviewPending.php"  class="menu-padding dropdown-a">Review</a></p>
                                              
                                    </div>
                    </div>                    
                        <a href="logout.php" class="white-text menu-item opacity-hover">Logout</a>
                        
                                    <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="adminDashboard.php">Dashboard</a></li>
                                        <!-- <li><a href="allProducts.php">All Products</a></li>
                                        <li><a href="addProduct.php">Add Product</a></li>
                                        <li><a href="category.php">Category</a></li> -->
                                        <li><a href="adminProductsAll.php">All Products</a></li>
                                        <li><a href="adminProductsAdd.php">Add Product</a></li>
                                        <li><a href="adminCategoryAll.php">Category</a></li>
                                        <li><a href="shopping.php">View Shop</a></li>
                                        <li><a href="adminOrdersPending.php">Orders</a></li>
                                        <li><a href="adminMessageNew.php">Message</a></li>
                                        <li><a href="adminReviewPending.php">Review</a></li>
                                        <li><a href="logout.php">Logout</a></li>
                                        </ul>
                                    </div><!-- /dl-menuwrapper -->                   
                                                
                    </div>
                </div>
        </header>

    <?php
    }
    elseif($_SESSION['usertype_level'] == 1)
    //user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="Pingola" title="Pingola"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div login-header">
                    <div class="dropdown">
                                    <a  class="white-text menu-margin-right menu-item opacity-hover">
                                                Profile <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a">Profile</a></p>
                                                <!-- <p class="dropdown-p"><a   class="menu-padding dropdown-a open-topup">Top Up</a></p> -->
                                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a">Edit Password</a></p>
                                                <!-- <p class="dropdown-p"><a href="betHistory.php"  class="menu-padding dropdown-a">Bet History</a></p> -->
                                                <!--<p class="dropdown-p"><a href="withdrawFund.php"  class="menu-padding dropdown-a">Withdraw Fund</a></p>-->
                                                <!--<p class="dropdown-p"><a href="transactionHistory.php"  class="menu-padding dropdown-a">Transaction History</a></p>-->
                                    </div>
                    </div> 
                    <div class="dropdown">
                                    <a  class="white-text menu-margin-right menu-item opacity-hover">
                                                Games <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                                <p class="dropdown-p"><a href="betHistory.php"  class="menu-padding dropdown-a">Bet History</a></p>
                                                <p class="dropdown-p"><a href="games.php"  class="menu-padding dropdown-a">Games</a></p>
                                                <!-- <p class="dropdown-p"><a href="gameResult.php"  class="menu-padding dropdown-a">Result</a></p> -->
                                                <p class="dropdown-p"><a href="leaderboard.php"  class="menu-padding dropdown-a">Leaderboard</a></p>
                                                
                                    </div>
                    </div>  
                   <div class="dropdown">
                                    <a  class="white-text menu-margin-right menu-item opacity-hover">
                                               Shop <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                                <p class="dropdown-p"><a href="shopping.php"  class="menu-padding dropdown-a">Shopping</a></p>
                                                <p class="dropdown-p"><a href="viewShoppingCart.php"  class="menu-padding dropdown-a">Cart</a></p>
                                                <p class="dropdown-p"><a href="orderHistory.php"  class="menu-padding dropdown-a">Order History</a></p>
                                              <p class="dropdown-p"><a href="liveChat.php"  class="menu-padding dropdown-a">Chat</a></p>
                                                <p class="dropdown-p"><a href="favouriteProducts.php"  class="menu-padding dropdown-a">Favourite</a></p>
                                    </div>
                    </div>              
                        
                        <a href="logout.php" class="white-text menu-item opacity-hover">Logout</a>
                        
                                    <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="profile.php">Profile</a></li>
                                        <li><a href="editPassword.php">Edit Password</a></li>
                                        <li><a href="betHistory.php">Bet History</a></li>
                                        <!--<li><a href="withdrawFund.php" >Withdraw Fund</a></li>-->
                                        <!--<li><a href="transactionHistory.php">Transaction History</a></li>-->
                                        <li><a href="games.php">Games</a></li>                                
                                        <!-- <li><a href="gameResult.php">Result</a></li> -->
                                        <li><a href="leaderboard.php">Leaderboard</a></li>
                                        <li><a href="shopping.php">Shop</a></li>
                                        <li><a href="viewShoppingCart.php">Cart</a></li>                                        
                                        <li><a href="orderHistory.php">Order History</a></li>
                                        <li><a href="liveChat.php">Chat</a></li> 
                                        <li><a href="favouriteProducts.php">Favourite</a></li>                                       
                                        <li><a href="logout.php">Logout</a></li>
                                        </ul>
                                    </div><!-- /dl-menuwrapper -->                   
                                                
                    </div>
                </div>
        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="Pingola" title="Pingola"></a>
            </div>
			<div class="right-menu-div float-right before-header-div">
                <a href="shopping.php" class="white-text menu-margin-right menu-item opacity-hover">
                  Product
                </a>  
                <a class="white-text menu-margin-right menu-item opacity-hover open-register">
                   Register
                </a>     
                 <a class="white-text menu-item opacity-hover open-login">
                   Login
                </a>                        
            </div>
        </div>
</header>

<?php
}
?>