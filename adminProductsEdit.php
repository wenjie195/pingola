<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$category = getCategory($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/editProduct.php" />
<meta property="og:title" content="Edit Product | Pingola" />
<title>Edit Product | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/editProduct.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
	<h1 class="line-header margin-bottom50">Edit Product</h1>
    <div class="clear"></div>
   <div class="border-separation">
        <div class="clear"></div>
        <form method="POST" action="utilities/adminProductEditFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_POST['product_uid']))
            {
                $conn = connDB();
                $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
            ?>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getName();?>" name="update_name" id="update_name" required>      
                </div>

                 <div class="clear"></div>

                <!-- <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Price (RM)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php //echo $productDetails[0]->getPrice();?>" name="update_price" id="update_price" required>      
                </div> -->

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Category</p>
                    <!-- <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getPrice();?>" name="update_price" id="update_price" required>       -->
                    <select class="input-name clean admin-input" type="text" name="update_category" id="update_category" required>
                        <?php
                        if($productDetails[0]->getCategory() == '')
                        {
                        ?>
                            <option selected>Select Category</option>
                            <?php
                            for ($cnt=0; $cnt <count($category) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $category[$cnt]->getName(); ?>"> 
                                    <?php echo $category[$cnt]->getName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($category) ; $cnt++)
                            {
                                if ($productDetails[0]->getCategory() == $category[$cnt]->getName())
                                {
                                ?>
                                    <option selected value="<?php echo $category[$cnt]->getName(); ?>"> 
                                        <?php echo $category[$cnt]->getName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $category[$cnt]->getName(); ?>"> 
                                        <?php echo $category[$cnt]->getName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
                
				<div class="clear"></div>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Product Description* (Avoid "'')</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescription();?>" name="update_description" id="update_description" required>              
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescriptionTwo();?>" name="update_description_two" id="update_description_two">
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescriptionThree();?>" name="update_description_three" id="update_description_three">
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescriptionFour();?>" name="update_description_four" id="update_description_four">
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescriptionFive();?>" name="update_description_five" id="update_description_five">
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getDescriptionSix();?>" name="update_description_six" id="update_description_six">
                </div>

                <div class="clear"></div>

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="product name, brand, for sale, Penang," value="<?php echo $productDetails[0]->getKeywordOne();?>" name="update_keyword_one" id="update_keyword_one">
                </div>         

                <div class="clear"></div>

                <div class="width100 overflow">
					<img src="img/youtube-tutorial.jpg" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
    				<p class="input-top-p admin-top-p">Youtube Video Link Only (Optional) (Copy the Highlighted Part Only)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[0]->getLink();?>" placeholder="Video Link"  name="update_link" id="update_link">           
                </div>      

                <div class="clear"></div>

                <input type="hidden" id="product_uid" name="product_uid" value="<?php echo $productDetails[0]->getUid();?>">

                <div class="clear"></div>  
                
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" name ="submit">Submit</button>
                </div>
                <div class="clear"></div>
        </form>

        <?php
        }
        ?>
	</div>

    <div class="width100 margin-top50">
        <h1 class="line-header margin-bottom50">Edit Product Photo</h1>
    </div>

    <div class="border-separation"></div>
    <div class="clear"></div>

    <div class="width100 overflow">
        <div class="six-box2">
            <div class="square">
                <div class="width100 white-bg content">
                    <img src="productImage/<?php echo $productDetails[0]->getImageOne();?>" class="pet-photo-preview">
                </div>
            </div>
            <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                 <!-- <p class="review-product-name text-center ow-margin-top20">Update Product Image 1</p> -->
                 <p class="input-top-p admin-top-p">Update Image 1</p>
                <div class="width100 overflow text-center">
                    <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                    <input type="hidden" id="image_value" name="image_value" value="1">
                    <!-- <input type="hidden" id="image_number" name="image_number" value="image_one"> -->
                    <!-- <input type="hidden" name="image_uid" id="image_uid" value="<?php //echo $productDetails[0]->getImageOne();?>" required> -->
                    <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                </div>
                <div class="width100 overflow text-center">  
                <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                </div>
                <div class="clear"></div> 
            </form>
        </div>

        <?php 
            $imageTwo = $productDetails[0]->getImageTwo();
            if($imageTwo != '')
            {
            ?>
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $productDetails[0]->getImageTwo();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                        <!-- <p class="review-product-name text-center ow-margin-top20">Update Product Image 1</p> -->
                        <p class="input-top-p admin-top-p">Update Image 2</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <input type="hidden" id="image_value" name="image_value" value="2">
                            <!-- <input type="hidden" id="image_number" name="image_number" value="image_two"> -->
                            <!-- <input type="hidden" name="image_uid" id="image_uid" value="<?php //echo $productDetails[0]->getImageOne();?>" required> -->
                            <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            <?php
            }
        ?>

        <?php 
            $imageThree = $productDetails[0]->getImageThree();
            if($imageThree != '')
            {
            ?>
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $productDetails[0]->getImageThree();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 3</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <input type="hidden" id="image_value" name="image_value" value="3">
                            <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            <?php
            }
        ?>

        <?php 
            $imageFour = $productDetails[0]->getImageFour();
            if($imageFour != '')
            {
            ?>
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $productDetails[0]->getImageFour();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 4</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <input type="hidden" id="image_value" name="image_value" value="4">
                            <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            <?php
            }
        ?>

        <?php 
            $imageFive = $productDetails[0]->getImageFive();
            if($imageFive != '')
            {
            ?>
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $productDetails[0]->getImageFive();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 5</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <input type="hidden" id="image_value" name="image_value" value="5">
                            <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            <?php
            }
        ?>

        <?php 
            $imageSix = $productDetails[0]->getImageSix();
            if($imageSix != '')
            {
            ?>
                <div class="six-box2">
                    <div class="square">
                        <div class="width100 white-bg content">
                            <img src="productImage/<?php echo $productDetails[0]->getImageSix();?>" class="pet-photo-preview">
                        </div>
                    </div>
                    <form action="utilities/updateProductImageFunction.php" method="POST" enctype="multipart/form-data">
                        <p class="input-top-p admin-top-p">Update Image 6</p>
                        <div class="width100 overflow text-center">
                            <input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
                            <input type="hidden" id="image_value" name="image_value" value="6">
                            <input type="hidden" name="product_uid" id="product_uid" value="<?php echo $productDetails[0]->getUid();?>" required>
                        </div>
                        <div class="width100 overflow text-center">  
                        <button type="submit" class="clean green-button pointer width100 update-photo-btn" name="submit" id="submit">Submit</button>
                        </div>
                        <div class="clear"></div> 
                    </form>
                </div>
            <?php
            }
        ?>

    </div>


</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>