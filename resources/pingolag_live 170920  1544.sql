-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 17, 2020 at 03:43 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pingolag_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `game_id` varchar(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `matches` varchar(255) NOT NULL,
  `team` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'ORI',
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `game_id`, `uid`, `username`, `current_credit`, `amount`, `matches`, `team`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, 'e540158df76503edd36b5b0c0b643526', '563557', '4666bfa37d650679eade008113e7fdb3', 'user1', '1000', 100, 'business-associates-2020-06-27', 'business associates', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-06-27 01:26:06', '2020-06-28 02:00:07'),
(2, '4b8853b698d81da144a58e280f7b1bdf', '564962', '4666bfa37d650679eade008113e7fdb3', 'user1', '900', 150, 'neon-esports-2020-06-28', 'Neon Esports', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-06-27 01:26:15', '2020-06-28 02:00:07'),
(3, 'e44d90a9c71fa1eef4e370c2d65420d7', '564843', '4666bfa37d650679eade008113e7fdb3', 'user1', '750', 200, 'fnatic-vs-reality-rift-2020-06-28', 'Fnatic', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-06-27 01:26:29', '2020-06-28 02:00:07'),
(4, '0630920e45dd0570950ce6e6293b5175', '564844', '4666bfa37d650679eade008113e7fdb3', 'user1', '550', 200, 'tnc-predator-vs-neon-esports-2020-06-28', 'TNC Predator', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-06-27 01:26:43', '2020-06-28 02:00:07'),
(5, '5adc8cecf15dab31a9f14ef360262428', '563557', 'a199400df7c2bedfc2cf11e226970ba5', 'user2', '1000', 100, 'business-associates-2020-06-27', 'CR4ZY', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-06-27 01:27:25', '2020-06-28 02:00:07'),
(6, 'df954f45050c55f082ffb0199e370759', '564962', 'a199400df7c2bedfc2cf11e226970ba5', 'user2', '900', 200, 'neon-esports-2020-06-28', '496 Gaming', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-06-27 01:27:33', '2020-06-28 02:00:07'),
(7, 'c559c84b1158559c0a6bee1eee13cfb3', '564843', 'a199400df7c2bedfc2cf11e226970ba5', 'user2', '700', 150, 'fnatic-vs-reality-rift-2020-06-28', 'Fnatic', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-06-27 01:27:43', '2020-06-28 02:00:07'),
(8, 'eedf0f812f41f28f9fcbe5472111e837', '564844', 'a199400df7c2bedfc2cf11e226970ba5', 'user2', '550', 250, 'tnc-predator-vs-neon-esports-2020-06-28', 'TNC Predator', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-06-27 01:27:52', '2020-06-28 02:00:07'),
(9, '754ff8b86b1bdae6a5aa4598b0a694c8', '565124', '4666bfa37d650679eade008113e7fdb3', 'user1', '350', 5, '4-zoomers-vs-infamous-2020-07-02', '4 Zoomers', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-07-01 04:12:43', '2020-07-02 01:30:04'),
(10, '514c974c4e372b296a90ad37528286c7', '565122', '2cf4e46d0d8a66766bdf647b5c3943fa', 'kevin', '50', 8, 'cr4zy-vs-beastcoast-2020-07-01', 'beastcoast', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-07-01 09:35:33', '2020-07-01 23:30:05'),
(11, 'f959a097fcb3d19bc5c6910235d369c1', '', 'c2957d9d3f32770871b934613528e0e2', 'irukorx', '5', 1, '', 'Execration.安博电竞', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-07-07 08:38:16', '2020-07-13 02:30:03'),
(12, 'd1bbb0c7b865732e3bf21aa3d0db058c', '565143', '815b44413f88f15aed997ef60b196779', 'testing', '6', 1, 'evil-geniuses-vs-quincy-crew-2020-07-15', 'Quincy Crew', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-07-15 05:19:06', '2020-07-15 20:00:04'),
(13, 'b93887603b745e533780f0285737ae7a', '', '815b44413f88f15aed997ef60b196779', 'testing', '5', 2, '', 'Sparking Arrow Gaming', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-07-15 05:19:43', '2020-07-15 20:00:04'),
(14, 'ed77bea116e3116711645deac4f919bb', '', '815b44413f88f15aed997ef60b196779', 'testing', '3', 1, '', 'Fnatic', NULL, NULL, NULL, NULL, 'WIN', NULL, 'ORI', NULL, NULL, '2020-07-15 05:21:35', '2020-07-15 20:00:04'),
(15, '7bb372ba4a6f03527ede23cdfade4128', '565978', 'c2957d9d3f32770871b934613528e0e2', 'irukorx', '4', 1, 'infamous-vs-business-associates-2020-07-16', 'Infamous', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-07-16 06:44:14', '2020-07-16 19:30:04'),
(16, 'b39d7b04338eaf6ec7a053e3e18f5806', '', 'c2957d9d3f32770871b934613528e0e2', 'irukorx', '3', 3, '', 'Team Trust', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-07-18 05:35:35', '2020-07-26 06:30:05'),
(17, '595478a1bc8810d1d7d13f848087c86b', '570943', '4666bfa37d650679eade008113e7fdb3', 'user1', '355', 50, 'ez1d-vs-catalonia-fc-esports-2020-09-15', 'Ez1d', NULL, NULL, NULL, NULL, 'REMATCH', NULL, 'ORI', NULL, NULL, '2020-09-15 06:43:34', '2020-09-17 07:33:36'),
(18, 'e528e6acfc05b7c7c4cb033e6d288451', '570943', '4666bfa37d650679eade008113e7fdb3', 'user1', '305', 20, 'ez1d-vs-catalonia-fc-esports-2020-09-15', 'Catalonia FC Esports', NULL, NULL, NULL, NULL, 'REMATCH', NULL, 'ORI', NULL, NULL, '2020-09-15 06:45:28', '2020-09-17 07:33:38'),
(19, '060272a18d96ee14ac8dcc2d98b525bf', '570266', '4666bfa37d650679eade008113e7fdb3', 'user1', '285', 20, '2020-09-15', 'Virtus.pro', NULL, NULL, NULL, NULL, 'LOSE', NULL, 'ORI', NULL, NULL, '2020-09-15 06:45:39', '2020-09-17 06:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ch_name`, `en_name`, `date_created`, `date_updated`) VALUES
(1, '阿布哈茲', 'Abkhazia', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(2, '阿富汗', 'Afghanistan', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(3, '奥兰', 'Åland', '2020-02-28 02:51:32', '2020-02-28 02:53:16'),
(4, '阿尔巴尼亚', 'Albania', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(5, '阿尔及利亚', 'Algeria', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(6, '美属萨摩亚', 'American Samoa', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(7, '安道尔', 'Andorra', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(8, '安哥拉', 'Angola', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(9, '安圭拉', 'Anguilla', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(10, '安地卡及巴布達', 'Antigua and Barbuda', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(11, '阿根廷', 'Argentina', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(12, '亞美尼亞', 'Armenia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(13, '阿尔扎赫', 'Artsakh', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(14, '阿鲁巴', 'Aruba', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(15, '澳大利亚', 'Australia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(16, '奥地利', 'Austria', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(17, '阿塞拜疆', 'Azerbaijan', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(18, '巴哈马', 'Bahamas', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(19, '巴林', 'Bahrain', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(20, '孟加拉国 孟加拉国', 'Bangladesh', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(21, '巴巴多斯', 'Barbados', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(22, '白俄羅斯', 'Belarus', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(23, '比利時', 'Belgium', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(24, '伯利兹', 'Belize', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(25, '贝宁', 'Benin', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(26, '百慕大', 'Bermuda', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(27, '不丹', 'Bhutan', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(28, '玻利维亚', 'Bolivia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(29, '波斯尼亚和黑塞哥维那', 'Bosnia and Herzegovina', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(30, '博茨瓦纳', 'Botswana', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(31, '巴西', 'Brazil', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(32, '文莱', 'Brunei', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(33, '保加利亚', 'Bulgaria', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(34, '布吉納法索', 'Burkina Faso', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(35, '布隆迪', 'Burundi', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(36, '柬埔寨', 'Cambodia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(37, '喀麦隆', 'Cameroon', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(38, '加拿大', 'Canada', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(39, '佛得角', 'Cape Verde', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(40, '开曼群岛', 'Cayman Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(41, '中非共和国', 'Central African Republic', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(42, '乍得', 'Chad', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(43, '智利', 'Chile', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(44, '中国', 'China', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(45, '圣诞岛', 'Christmas Island', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(46, '科科斯（基林）', 'Cocos (Keeling) Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(47, '哥伦比亚', 'Colombia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(48, '科摩罗', 'Comoros', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(49, '刚果（布）', 'Congo (Brazzaville)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(50, '刚果（金）', 'Congo (Kinshasa)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(51, '庫克群島', 'Cook Islands', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(52, '哥斯达黎加', 'Costa Rica', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(53, '科特迪瓦', 'Côte d\'Ivoire\r\n', '2020-02-28 02:51:37', '2020-02-28 03:38:22'),
(54, '克罗地亚', 'Croatia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(55, '古巴', 'Cuba', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(56, '库拉索', 'Curaçao\r\n', '2020-02-28 02:51:37', '2020-02-28 03:37:19'),
(57, '賽普勒斯', 'Cyprus', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(58, '捷克', 'Czech Republic', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(59, '丹麥', 'Denmark', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(60, '吉布提', 'Djibouti', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(61, '多米尼克', 'Dominica', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(62, '多米尼加', 'Dominican Republic', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(63, '顿涅茨克', 'Donetsk', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(64, '厄瓜多尔', 'Ecuador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(65, '埃及', 'Egypt', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(66, '薩爾瓦多', 'El Salvador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(67, '赤道几内亚', 'Equatorial Guinea', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(68, '厄立特里亚', 'Eritrea', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(69, '爱沙尼亚', 'Estonia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(70, '斯威士兰', 'Eswatini', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(71, '衣索比亞', 'Ethiopia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(72, '福克蘭群島', 'Falkland Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(73, '法罗群岛', 'Faroe Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(74, '斐济', 'Fiji', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(75, '芬兰', 'Finland', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(76, '法國', 'France', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(77, '法屬玻里尼西亞', 'French Polynesia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(78, '加彭', 'Gabon', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(79, '冈比亚', 'Gambia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(80, '格鲁吉亚', 'Georgia', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(81, '德國', 'Germany', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(82, '加纳', 'Ghana', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(83, '直布罗陀', 'Gibraltar', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(84, '希臘', 'Greece', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(85, '格陵兰', 'Greenland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(86, '格瑞那達', 'Grenada', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(87, '關島', 'Guam', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(88, '危地马拉', 'Guatemala', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(89, '根西', 'Guernsey', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(90, '几内亚', 'Guinea', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(91, '几内亚比绍', 'Guinea-Bissau', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(92, '圭亚那', 'Guyana', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(93, '海地', 'Haiti', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(94, '洪都拉斯', 'Honduras', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(95, '香港', 'Hong Kong', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(96, '匈牙利', 'Hungary', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(97, '冰島', 'Iceland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(98, '印度', 'India', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(99, '印尼', 'Indonesia', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(100, '伊朗', 'Iran', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(101, '伊拉克', 'Iraq', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(102, '爱尔兰', 'Ireland', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(103, '马恩岛', 'Isle of Man', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(104, '以色列', 'Israel', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(105, '意大利', 'Italy', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(106, '牙买加', 'Jamaica', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(107, '日本', 'Japan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(108, '澤西', 'Jersey', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(109, '约旦', 'Jordan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(110, '哈萨克斯坦', 'Kazakhstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(111, '肯尼亚', 'Kenya', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(112, '基里巴斯', 'Kiribati', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(113, '科索沃', 'Kosovo', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(114, '科威特', 'Kuwait', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(115, '吉尔吉斯斯坦', 'Kyrgyzstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(116, '老挝', 'Laos', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(117, '拉脫維亞', 'Latvia', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(118, '黎巴嫩', 'Lebanon', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(119, '賴索托', 'Lesotho', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(120, '利比里亚', 'Liberia', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(121, '利比亞', 'Libya', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(122, '列支敦斯登', 'Liechtenstein', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(123, '立陶宛', 'Lithuania', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(124, '卢甘斯克', 'Luhansk', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(125, '卢森堡', 'Luxembourg', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(126, '澳門', 'Macau', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(127, '马达加斯加', 'Madagascar', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(128, '马拉维', 'Malawi', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(129, '马来西亚', 'Malaysia', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(130, '馬爾地夫', 'Maldives', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(131, '马里', 'Mali', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(132, '馬爾他', 'Malta', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(133, '马绍尔群岛', 'Marshall Islands', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(134, '毛里塔尼亚', 'Mauritania', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(135, '模里西斯', 'Mauritius', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(136, '墨西哥', 'Mexico', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(137, '密克羅尼西亞聯邦', 'Micronesia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(138, '摩尔多瓦', 'Moldova', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(139, '摩納哥', 'Monaco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(140, '蒙古國', 'Mongolia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(141, '蒙特內哥羅', 'Montenegro', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(142, '蒙特塞拉特', 'Montserrat', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(143, '摩洛哥', 'Morocco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(144, '莫桑比克', 'Mozambique', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(145, '緬甸', 'Myanmar', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(146, '纳米比亚', 'Namibia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(147, '瑙鲁', 'Nauru', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(148, '尼泊尔', 'Nepal', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(149, '荷蘭', 'Netherlands', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(150, '新喀里多尼亞', 'New Caledonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(151, '新西蘭', 'New Zealand', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(152, '尼加拉瓜', 'Nicaragua', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(153, '尼日尔', 'Niger', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(154, '奈及利亞', 'Nigeria', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(155, '纽埃', 'Niue', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(156, '朝鲜', 'North Korea', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(157, '北馬其頓', 'North Macedonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(158, '北塞浦路斯', 'Northern Cyprus', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(159, '北馬里亞納群島', 'Northern Mariana Islands', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(160, '挪威', 'Norway', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(161, '阿曼', 'Oman', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(162, '巴基斯坦', 'Pakistan', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(163, '帛琉', 'Palau', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(164, '巴勒斯坦', 'Palestine', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(165, '巴拿马', 'Panama', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(166, '巴布亚新几内亚', 'Papua New Guinea', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(167, '巴拉圭', 'Paraguay', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(168, '秘魯', 'Peru', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(169, '菲律賓', 'Philippines', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(170, '皮特凯恩群岛', 'Pitcairn Islands', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(171, '波蘭', 'Poland', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(172, '葡萄牙', 'Portugal', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(173, '德涅斯特河沿岸', 'Pridnestrovie', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(174, '波多黎各', 'Puerto Rico', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(175, '卡塔尔', 'Qatar', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(176, '羅馬尼亞', 'Romania', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(177, '俄羅斯', 'Russia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(178, '卢旺达', 'Rwanda', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(179, '圣巴泰勒米', 'Saint Barthelemy', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(180, '圣基茨和尼维斯', 'Saint Christopher and Nevis', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(181, '圣赫勒拿、阿森松和特里斯坦-达库尼亚', 'Saint Helena, Ascension and Tristan da Cunha', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(182, '圣卢西亚', 'Saint Lucia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(183, '圣皮埃尔和密克隆', 'Saint Pierre and Miquelon', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(184, '圣文森特和格林纳丁斯', 'Saint Vincent and the Grenadines', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(185, '萨摩亚', 'Samoa', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(186, '圣马力诺', 'San Marino', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(187, '聖多美和普林西比', 'São Tomé and Príncipe\r\n', '2020-02-28 02:51:52', '2020-02-28 03:39:40'),
(188, '沙烏地阿拉伯', 'Saudi Arabia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(189, '塞内加尔', 'Senegal', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(190, '塞爾維亞', 'Serbia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(191, '塞舌尔', 'Seychelles', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(192, '塞拉利昂', 'Sierra Leone', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(193, '新加坡', 'Singapore', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(194, '荷屬聖馬丁', 'Sint Maarten', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(195, '斯洛伐克', 'Slovakia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(196, '斯洛維尼亞', 'Slovenia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(197, '所罗门群岛', 'Solomon Islands', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(198, '索馬利亞', 'Somalia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(199, '索馬利蘭', 'Somaliland', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(200, '南非', 'South Africa', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(201, '韩国', 'South Korea', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(202, '南奥塞梯', 'South Ossetia', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(203, '南蘇丹', 'South Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(204, '西班牙', 'Spain', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(205, '斯里蘭卡', 'Sri Lanka', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(206, '苏丹', 'Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(207, '苏里南', 'Suriname', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(208, '斯瓦尔巴', 'Svalbard', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(209, '瑞典', 'Sweden', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(210, '瑞士', 'Switzerland', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(211, '叙利亚', 'Syria', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(212, '中華民國', 'Taiwan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(213, '塔吉克斯坦', 'Tajikistan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(214, '坦桑尼亚', 'Tanzania', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(215, '泰國', 'Thailand', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(216, '梵蒂冈', 'The Holy See（Vatican City）', '2020-02-28 02:51:55', '2020-02-28 03:40:13'),
(217, '东帝汶', 'Timor-Leste', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(218, '多哥', 'Togo', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(219, '托克勞', 'Tokelau', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(220, '汤加', 'Tonga', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(221, '千里達及托巴哥', 'Trinidad and Tobago', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(222, '突尼西亞', 'Tunisia', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(223, '土耳其', 'Turkey', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(224, '土库曼斯坦', 'Turkmenistan', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(225, '特克斯和凯科斯群岛', 'Turks and Caicos Islands', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(226, '图瓦卢', 'Tuvalu', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(227, '乌干达', 'Uganda', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(228, '烏克蘭', 'Ukraine', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(229, '阿联酋', 'United Arab Emirates', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(230, '英國', 'United Kingdom', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(231, '美國', 'United States', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(232, '乌拉圭', 'Uruguay', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(233, '乌兹别克斯坦', 'Uzbekistan', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(234, '瓦努阿圖', 'Vanuatu', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(235, '委內瑞拉', 'Venezuela', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(236, '越南', 'Vietnam', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(237, '英屬維爾京群島', 'Virgin Islands, British', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(238, '瓦利斯和富圖納', 'Wallis and Futuna', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(239, '西撒哈拉', 'Western Sahara', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(240, '葉門', 'Yemen', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(241, '尚比亞', 'Zambia', '2020-02-28 02:51:58', '2020-02-28 02:51:58'),
(242, '辛巴威', 'Zimbabwe', '2020-02-28 02:51:58', '2020-02-28 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `upgrade` varchar(255) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submit_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reference` text,
  `verify_by` varchar(255) DEFAULT NULL,
  `verify_time` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `team_one` varchar(255) DEFAULT NULL,
  `value_one` varchar(255) DEFAULT NULL,
  `team_two` varchar(255) DEFAULT NULL,
  `value_two` varchar(255) DEFAULT NULL,
  `img_one` varchar(255) DEFAULT NULL,
  `img_two` varchar(255) DEFAULT NULL,
  `com_img` varchar(255) DEFAULT NULL,
  `com_name` varchar(255) DEFAULT NULL,
  `winner` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `matchday` datetime DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `uid`, `title`, `team_one`, `value_one`, `team_two`, `value_two`, `img_one`, `img_two`, `com_img`, `com_name`, `winner`, `status`, `matchday`, `date_created`, `date_updated`) VALUES
(1, '6402d3396c8fd580f8aa3a0fce59b01c', 'StarLadder ImbaTV Dota 2 Minor Season 3', 'SCABBARD', '1.89', 'SILENT NIGHT', '1.92', 'scabbard.png', 'silent-night.png', 'dota2.png', 'Dota 2', NULL, 'Win', '2020-05-21 22:00:00', '2020-05-21 03:17:04', '2020-05-21 04:28:36'),
(2, '1427b3fdbb29d20ade7e05152d512d92', 'UKLC 2020 Summer', 'SCABBARD', '1.89', 'SILENT NIGHT', '1.92', 'scabbard.png', 'silent-night.png', 'league-of-legends.png', 'League of Legends', NULL, 'Win', '2020-05-22 22:00:00', '2020-05-21 03:17:16', '2020-05-21 04:28:38'),
(3, '2a8892f568eda499c9bf7585eeac65bc', 'Overwatch League', 'SCABBARD', '1.89', 'SILENT NIGHT', '1.92', 'scabbard.png', 'silent-night.png', 'overwatch.png', 'Overwatch', NULL, 'Win', '2020-05-23 22:00:00', '2020-05-21 03:17:28', '2020-05-21 04:28:40'),
(4, 'c65c2d69688bea26280257cede045329', 'StarLadder ImbaTV Dota 2 Minor Season 3 SF', 'SCABBARD', '1.89', 'SILENT NIGHT', '1.92', 'scabbard.png', 'silent-night.png', 'dota2.png', 'Dota 2', 'SCABBARD', 'Win', '2020-05-20 20:00:00', '2020-05-21 04:09:11', '2020-05-21 04:28:41'),
(5, '9cea95f5982e20586f44c67f9ac3fda0', 'UKLC 2020 Summer SF', 'SCABBARD', '1.89', 'SILENT NIGHT', '1.92', 'scabbard.png', 'silent-night.png', 'league-of-legends.png', 'League of Legends', 'SILENT NIGHT', 'Win', '2020-05-19 20:00:00', '2020-05-21 04:09:37', '2020-05-21 04:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `pageview`
--

CREATE TABLE `pageview` (
  `id` int(255) NOT NULL,
  `page` text,
  `userip` text,
  `refer_uid` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pageview`
--

INSERT INTO `pageview` (`id`, `page`, `userip`, `refer_uid`, `date_created`, `date_updated`) VALUES
(1, NULL, '202.190.53.45', '4666bfa37d650679eade008113e7fdb3', '2020-07-01 04:59:58', '2020-07-01 04:59:58'),
(2, NULL, '202.190.53.115', '815b44413f88f15aed997ef60b196779', '2020-07-15 05:17:32', '2020-07-15 05:17:32');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, '9444083d4987ee78c99346fbb7154f81', '9444083d4987ee78c99346fbb7154f81', 'admin', 0, '9444083d4987ee78c99346fbb7154f81', '2020-05-19 04:01:06', '2020-05-19 04:01:06'),
(2, '4666bfa37d650679eade008113e7fdb3', '4666bfa37d650679eade008113e7fdb3', 'user1', 0, '4666bfa37d650679eade008113e7fdb3', '2020-05-19 04:01:53', '2020-05-19 04:01:53'),
(3, 'a199400df7c2bedfc2cf11e226970ba5', 'a199400df7c2bedfc2cf11e226970ba5', 'user2', 0, 'a199400df7c2bedfc2cf11e226970ba5', '2020-05-19 04:02:09', '2020-05-19 04:02:09'),
(4, '2cf4e46d0d8a66766bdf647b5c3943fa', '2cf4e46d0d8a66766bdf647b5c3943fa', 'kevin', 0, '2cf4e46d0d8a66766bdf647b5c3943fa', '2020-06-25 13:06:19', '2020-06-25 13:06:19'),
(5, 'c2957d9d3f32770871b934613528e0e2', 'c2957d9d3f32770871b934613528e0e2', 'irukorx', 0, 'c2957d9d3f32770871b934613528e0e2', '2020-06-30 06:53:16', '2020-06-30 06:53:16'),
(6, '815b44413f88f15aed997ef60b196779', '815b44413f88f15aed997ef60b196779', 'testing', 0, '815b44413f88f15aed997ef60b196779', '2020-07-15 05:16:17', '2020-07-15 05:16:17'),
(7, '815b44413f88f15aed997ef60b196779', '85a5a52902d4f8fb743b6a46b07e5cd1', 'testing2', 1, '815b44413f88f15aed997ef60b196779', '2020-07-15 05:18:17', '2020-07-15 05:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `sharing`
--

CREATE TABLE `sharing` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `deposit` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL COMMENT 'win or lose',
  `profile_img` varchar(255) DEFAULT NULL,
  `subscribe_status` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `vip_status` varchar(255) DEFAULT NULL,
  `link_click` varchar(255) DEFAULT NULL,
  `refer_no` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `deposit`, `credit`, `profile_img`, `subscribe_status`, `bank_name`, `bank_account_no`, `bank_account_name`, `message`, `vip_status`, `link_click`, `refer_no`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9444083d4987ee78c99346fbb7154f81', 'admin', 'admin@gmail.com', '35801bdeed18fd73957d706d0c30dcd51b50019a4c1bf60b06fd83998d7ba44e', '0cef6c5b428399faa906926296ab8edc325d5c6b', '12332100', NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-05-19 04:01:06', '2020-05-19 04:01:19'),
(2, '4666bfa37d650679eade008113e7fdb3', 'user1', 'wenjie195.vidatech@gmail.com', 'a836cff05deef9b1bfcc75a97b801f053d8ef0e97661315f74e465ba7011d428', '5420394c8be0f45444257ab16c8967c3fe12e1f2', '12332111', NULL, NULL, NULL, NULL, '265', '4666bfa37d650679eade008113e7fdb315935795862.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, 1, 1, '2020-05-19 04:01:53', '2020-09-15 06:45:39'),
(3, 'a199400df7c2bedfc2cf11e226970ba5', 'user2', 'sherry2.vidatech@gmail.com', '8a5507ce62a3602d70475adfb6dbf6ac889fdb90ac528f378a621770077138c9', 'b57512e5ff03b778d91a7858dcb934227377ad66', '12332122', NULL, NULL, NULL, NULL, '500', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-05-19 04:02:09', '2020-07-01 14:30:37'),
(4, '2cf4e46d0d8a66766bdf647b5c3943fa', 'kevin', 'kevinyam168@gmail.com', '74fb78903dd4b0c420fb250540c98d8f532c6cf7b736c370789cebf5ac665aa3', 'c8710f55ff0e6b0eb0523dae50008973dc309d31', '0165324691', NULL, NULL, NULL, NULL, '42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, 1, 1, '2020-06-25 13:06:19', '2020-07-01 09:35:33'),
(5, 'c2957d9d3f32770871b934613528e0e2', 'irukorx', 'mr_awg@hotmail.com', '72319146ffa09a570569c8d164f35e734537207a63dd57215e4ca45669dd8902', 'bee241e72e035bbbc788011f8baa4b045bea5bfa', '98382323', NULL, NULL, NULL, NULL, '3', 'c2957d9d3f32770871b934613528e0e21594110653edited pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-06-30 06:53:16', '2020-07-16 06:44:14'),
(6, '815b44413f88f15aed997ef60b196779', 'testing', 'testing@gmail.com', '991a7887295e1d2e1a430c13d90feb538037a174f354942e12d1608ad09bd642', 'cb3f9abf605b49be7c639d684b3d8e5ca287d32f', '01258741254', NULL, NULL, NULL, NULL, '4', '815b44413f88f15aed997ef60b1967791594790228IMG_2741.PNG', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', 1, 1, '2020-07-15 05:16:17', '2020-07-15 20:00:04'),
(7, '85a5a52902d4f8fb743b6a46b07e5cd1', 'testing2', 'testing2@gmail.com', '26aa5d6d272028f3ac7c288b02a67fd721c7c2f34feda1dca35c1ade00104de8', '65f3d51710032772afef4df679b8519fbb30a9e5', '01474748585', NULL, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-07-15 05:18:17', '2020-07-15 05:18:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageview`
--
ALTER TABLE `pageview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sharing`
--
ALTER TABLE `sharing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pageview`
--
ALTER TABLE `pageview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sharing`
--
ALTER TABLE `sharing`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
