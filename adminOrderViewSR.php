<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
// require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminOrderViewSR.php" />
<meta property="og:title" content="Order Details | Pingola" />
<title>Order Details | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminOrderViewSR.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
<h1 class="line-header margin-bottom50">Order Details</h1>

    <div class="border-separation">

        <div class="clear"></div>

        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                // $ordersDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                $ordersDetails = getOrders($conn,"WHERE order_id = ? ORDER BY date_created DESC LIMIT 1  ", array("order_id") ,array($_POST['order_id']),"s");
                $orderId = $ordersDetails[0]->getId();

                // $productOrder = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($orderId),"s");
                $productOrder = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
            ?>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Order ID: <b>#<?php echo $orderId;?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Purchaser Name: <b><?php echo $ordersDetails[0]->getName();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Contact: <b><?php echo $ordersDetails[0]->getContactNo();?></b></p>
                </div>

                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Address: <b><?php echo $ordersDetails[0]->getAddressLine1();?></b></p>
                </div> 

                <div class="clear"></div>

                <div class="dual-input data-text">
                    <p class="input-top-p admin-top-p">Payment References: <b><?php echo $ordersDetails[0]->getPaymentBankReference();?></b></p>
                </div> 
                <div class="dual-input second-dual-input data-text">
                    <p class="input-top-p admin-top-p">Order Date: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
                </div> 

                <div class="clear"></div>

                <?php 
                    // $shippingStatus = $ordersDetails[0]->getShippingStatus();
                    $paymentStatus = $ordersDetails[0]->getPaymentStatus();
                    // if($shippingStatus = 'DELIVERED')
                    if($paymentStatus == 'ACCEPTED')
                    {
                    ?>
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Status: <b><?php echo $paymentStatus;?></b></p>
                        </div>

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Shipping Method: <b><?php echo $ordersDetails[0]->getShippingMethod();?></b></p>
                        </div>        
                        <div class="clear"></div>
                
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Tracking Number: <b><?php echo $ordersDetails[0]->getTrackingNumber();?></b></p>   
                        </div>

                        

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Shipped Date: <b><?php echo $ordersDetails[0]->getShippingDate();?></b></p>
                        </div> 
                    <?php
                    }
                    // elseif($shippingStatus = 'REJECTED')
                    elseif($paymentStatus == 'REJECTED')
                    {
                    ?>
                    	<div class="clear"></div>
                        <div class="dual-input data-text">
                            <p class="input-top-p admin-top-p">Status: <b><?php echo $paymentStatus;?></b></p>
                        </div>

                        <div class="dual-input second-dual-input data-text">
                            <p class="input-top-p admin-top-p">Rejected Date: <b><?php echo $ordersDetails[0]->getShippingDate();?></b></p>
                        </div> 
                        <div class="clear"></div>
                    <?php
                    }
                    else
                    {}
                ?>

            <?php
            }
            ?>
        
                
                <div class="overflow-scroll-div margin-top30 same-padding-tdh margin-bottom20">
                    <table class="table-css">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($productOrder)
                                {
                                    
                                    for($cnt = 0;$cnt < count($productOrder) ;$cnt++)
                                    {?>
                                        
                                        <tr class="link-to-details">
                                            <td><?php echo ($cnt+1)?></td>
                                            <td><?php echo $productOrder[$cnt]->getProductName();?></td>
                                            <td><?php echo $productOrder[$cnt]->getQuantity();?></td>
                                            <td>RM<?php echo $productOrder[$cnt]->getTotalPrice();?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>                                 
                        </tbody>
                    </table>
                </div>

	</div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>