<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

//$userRows = getSeller($conn,"WHERE id = ?",array("id"),array($id),"s");
// $userDetails = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"s");
// $userDetails = getUser($conn," WHERE user_type = ? OR user_type = ? ",array("user_type","user_type"),array(1,5),"ss");

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$orderRequest = getOrders($conn, " WHERE payment_status = 'ACCEPTED' AND shipping_status = 'DELIVERED' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/adminOrdersShipped.php" />
<meta property="og:title" content="Shipped Order | Pingola" />
<title>Shipped Order | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/adminOrdersShipped.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding black-bg ping-menu-distance ping-min-height" id="myTable">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="h1-title2"><a href="adminOrdersPending.php" class="green-a opacity-hover2">Pending</a> | <a href="adminOrdersFail.php" class="green-a opacity-hover2">Fail</a> | Shipped | <a href="adminOrdersRejected.php" class="green-a opacity-hover2">Rejected</a></h1>
            
        </div>

    </div>

    <div class="clear"></div>

	<div class="overflow-scroll-div margin-top30 same-padding-tdh">
    	<table class="table-css">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Shipped<br>Date</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Amount</th>
                    <th>Transaction<br>References</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($orderRequest)
                    {
                        
                        for($cnt = 0;$cnt < count($orderRequest) ;$cnt++)
                        {?>
                            
                            <tr class="link-to-details">
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $orderRequest[$cnt]->getShippingDate();?></td>
                                <td><?php echo $orderRequest[$cnt]->getName();?></td>
                                <td><?php echo $orderRequest[$cnt]->getContactNo();?></td>
                                <td>RM<?php echo $orderRequest[$cnt]->getSubtotal();?></td>
                                <td><?php echo $orderRequest[$cnt]->getPaymentBankReference();?></td>

                                <td>
                                    <form method="POST" action="adminOrderViewSR.php" class="hover1">
                                        <button class="clean  transparent-button pointer" type="submit" name="order_id" value="<?php echo $orderRequest[$cnt]->getOrderId();?>">
                                            <img src="img/edit.png" class="edit-icon1" alt="Edit" title="Edit">
                                           
                                        </button>
                                    </form>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete user !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Account Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update user account !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</body>
</html>