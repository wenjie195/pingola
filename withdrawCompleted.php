<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
$time = $dt->format('d M Y');

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/withdrawCompleted.php" />
<meta property="og:title" content="Completed Withdrawal | Pingola" />
<title>Completed Withdrawal | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/withdrawCompleted.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="menu-distance width100 same-padding div1 grey-bg min-height">

  
     <div class="width100 overflow">
    	<h1 class="line-header">Completed Withdrawal | <a href="withdrawRequest.php" class="pop-out-a">Request</a></h1>
        <div class="clear"></div>
        <div class="overflow search-input-div opacity-hover">
        	<img src="img/search.png" class="search-png">
        	<input class="search-input clean" type="text" placeholder="Search">
        </div>
        <div class="clear"></div>
        <div class="overflow-scroll-div">
        <table class="table-css">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Requested Date</th>
                    <th>Process Date</th>
                    <th>User</th>
                    <th>Withdraw (PingCash)</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td>1.</td>
                    <td>5/5/2020</td>
                    <td>5/5/2020</td>
                    <td>Angela</td>
                    <td>100</th>
                    <td class="win-color">Approved</th>
                    <td><a class='iframe pop-out-a' href="withdrawDetails.php">View</a></td> 
                </tr>
            	<tr>
                	<td>2.</td>
                    <td>5/5/2020</td>
                    <td>5/5/2020</td>
                    <td>James</td>
                    <td>100</th>
                    <td class="win-color">Approved</th>
                    <td><a class='iframe pop-out-a' href="withdrawDetails.php">View</a></td> 
                </tr>                
            	<tr>
                	<td>3.</td>
                    <td>5/5/2020</td>
                    <td>5/5/2020</td>
                    <td>Jimmy</td>
                    <td>100</th>
                    <td class="lose-color">Rejected</th>
                    <td><a class='iframe pop-out-a' href="withdrawDetails.php">View</a></td> 
                </tr>                 
            </tbody>
        </table>
        </div>
     </div>
</div>


<?php include 'js.php'; ?>


</body>
</html>