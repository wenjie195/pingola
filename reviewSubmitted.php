<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reviews.php';
require_once dirname(__FILE__) . '/classes/ReviewsRespond.php';
// require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://pingola.games/reviewSubmitted.php" />
<meta property="og:title" content="Review Submitted | Pingola" />
<title>Review Submitted | Pingola</title>
<meta property="og:description" content="Pingola" />
<meta name="description" content="Pingola" />
<meta name="keywords" content="Pingola, game, dota, dota 2, counter strike, king of glory, honor of kings, 王者荣耀, gaming, esport, waging, win, loss, lose, team, earn, money, etc">
<link rel="canonical" href="https://pingola.games/reviewSubmitted.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding black-bg ping-menu-distance ping-min-height">
<div class="width100 overflow text-center">
	<img src="img/check.png" class="tick-css">
</div>
<p class="text-center submit-p" style="margin-top:20px;">Your review is submitted and waiting for approval.</p>
               <!-- <div class="width100 text-center margin-top-bottom">
                    <div class="green-button mid-btn-width" onclick="goBack()">Go Back</div>
                </div>-->	
</div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>